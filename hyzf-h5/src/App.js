import RouteView from './router/index';
import routes from './router/config'
function App() {
  return (
    <div className="App">
      <RouteView routes={routes}></RouteView>
    </div>
  );
}

export default App;
