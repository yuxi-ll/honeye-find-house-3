/*
 * @Author: your name
 * @Date: 2021-05-31 09:52:06
 * @LastEditTime: 2021-05-31 22:46:58
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\honeye-find-house-3\hyzf-h5\src\index.js
 */
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import { BrowserRouter } from 'react-router-dom'
import 'lib-flexible';
import 'zarm/dist/zarm.css'
import './mock/zzh/index'
import './mock/ssh/index'
import "./icon/iconfont.css"
import 'antd-mobile/dist/antd-mobile.css'
import 'antd/dist/antd.css';
import './index.css'
import "./mock/yjr/index"
import './mock/hpx/index'
import "./mock/yky/index"
ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById('root')
);
