import Mock from 'mockjs'

const plotData=Mock.mock({
    "plotData|10":[{
        name:"@ctitle(2)小区"
    }]
})

Mock.mock("/api/plotlist",()=>{
    return plotData
})