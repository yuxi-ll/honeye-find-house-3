import Mock from "mockjs"

const brokerData = Mock.mock({

    "list|20": [
        {
            // id
            'id|+1': 1,
            // 头像
            'img': '@image(50x50,@color)',
            // 名字
            'name': '@cname',
            // 电话
            'phone|222222222222-99999999999': 1,
            // 地址
            'area': '长江路105号东50米'
        }
    ]
})
// 经纪人
Mock.mock('http://localhost:3000/api/broker', () => {
    return brokerData
})
const brokerdetailData = Mock.mock({

    "list|20": [
        {
            'houseImg': 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fi0.hdslb.com%2Fbfs%2Farticle%2Ffdf8ae9d3a66fb464c0316cd62c73b97ea7d603f.jpg&refer=http%3A%2F%2Fi0.hdslb.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624962973&t=2cbc910e31e445c39f60767d4eb060df',
            'title': '@ctitle(4,7)',
            'desc': '@ctitle',
            'houseCode|1000000-99999999': 1
        }
    ]
})
// 经纪人详情
Mock.mock('http://localhost:3000/api/brokerDetail', () => {
    return brokerdetailData
})

const groupPurchaseData = Mock.mock({
    'list': [
        {
            'img': 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fwww.91tianxia.cn%2Fwp-content%2Fuploads%2F2020%2F05%2F20200519_5ec3ca933ebee.jpg&refer=http%3A%2F%2Fwww.91tianxia.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1625042614&t=5d9246d0bf6094ca63c9ca6d8d8dd788',
            'house': '野原新之助之家',
            'area': '埼玉县春日部市双叶町904'
        },
        {
            'img': 'https://img2.baidu.com/it/u=2595368534,2820748561&fm=26&fmt=auto&gp=0.jpg',
            'house': '哆啦A梦and野比大雄之家',
            'area': '东京都杉并区内的某一町'
        },
        {
            'img': 'https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fn.sinaimg.cn%2Fsinakd202042s%2F181%2Fw640h341%2F20200402%2Fa076-irpunai5548964.jpg&refer=http%3A%2F%2Fn.sinaimg.cn&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1625043245&t=0b9e3a3ad9fb0a63aa126a0007cab2a7',
            'house': '派大星的家',
            'area': '比奇堡贝壳街126号'
        }
    ]
})

// 团购看房groupPurchaseData
Mock.mock('http://localhost:3000/api/groupPurchase', () => {
    return groupPurchaseData
})