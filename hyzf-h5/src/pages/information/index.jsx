import React, { useEffect, useState } from 'react'
import axios from '../../utils/request'
import './common.css';
import './index.css'
const Information = (props) => {
    const [news,setNews]=useState([])
    useEffect(()=>{
        getNews()
    },[])
    const getNews=async ()=>{
        const newsList=await axios.get('/home/news?area=AREA%7C88cff55c-aaa4-e2e0');
        console.log(newsList,'news');
        setNews(newsList.data.body)
    }
    const handleBackHome=()=>{
        props.history.go(-1)
    }
    const handlePushNewsDetail=(val)=>{
        props.history.push({
            pathname:'/infoDetail',
            state:{val}
        })
    }
    return (
        <div className='information flex-col'>
            <div className="wrap2 flex-col">
                <div className="section4 flex-row">
                    <img
                    className="label2"
                    src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng1180f4d786d64dbeb38b8c9061ea64271c9c6c95bf874dada053daff01a46b52'
                    }
                    alt=''
                    onClick={handleBackHome}
                    />
                    <span className="txt10">资讯</span>
                </div>
            </div>
            <div className="newslist">
                {
                    news.map(item=>{
                        return <dl key={item.id} onClick={()=>handlePushNewsDetail(item)}>
                            <dt>
                                <img width='120' height='90' src={`https://api-haoke-web.itheima.net`+item.imgSrc} alt="" />
                            </dt>
                            <dd>
                                <h3>{item.title}</h3>
                                <div className='msg'>
                                    <span>{item.from}</span>
                                    <span>{item.date}</span>
                                </div>
                            </dd>
                        </dl>
                    })
                }
            </div>
        </div>
    )
}

export default Information
