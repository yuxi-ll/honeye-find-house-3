import React from 'react';
import './common.css';
import './index.css';
const NewsDetail = (props) => {
    console.log(props,'ppppp');
    const {val}=props.location.state
    const handleBackInfo=()=>{
        props.history.go(-1)
    }
    return (
        <div className='infoDetail flex-col'>
           <div className="wrap2 flex-col">
                <div className="section4 flex-row">
                    <img
                    className="label2"
                    src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng1180f4d786d64dbeb38b8c9061ea64271c9c6c95bf874dada053daff01a46b52'
                    }
                    alt=''
                    onClick={handleBackInfo}
                    />
                    <span className="txt10">详情</span>
                </div>
            </div>
           <div className="infoMessage">
               <h2>{val.title}</h2>
               <div className='infoDate'>
                   <span>来源：{val.from}</span>
                   <span>时间：{val.date}</span>
               </div>
               <div className='floorState'>
                   <span>楼盘动态</span>
                   <p>2021年4月29日，新华网:新城士里金樾位于开发区，目前在售15#高层，仅剩一楼二楼房源，建面约110-127m2,备案均价8379元/平;今日加推14#洋房，建面约127m2,洋房44套，备案均价9255元/平。6#高层加推时间待定，以上信息仅供参考，详情请致电售楼处。</p>
               </div>
               <div>
                   <img width='100%' src={`https://api-haoke-web.itheima.net`+val.imgSrc} alt="" />
               </div>
            </div> 
        </div>
    )
}

export default NewsDetail
