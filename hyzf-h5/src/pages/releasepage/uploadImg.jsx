import React from 'react'
import { useState } from 'react';
import { FilePicker, Icon, Toast, Badge } from 'zarm';
import {useHistory} from 'react-router-dom'
import './css/sell.css'

const MAX_FILES_COUNT = 5;

const onBeforeSelect = () => {
//   alert('执行 onBeforeSelect 方法');
};
const onBeforeSelect2 = () => {
//   alert('执行 onBeforeSelect 方法');
};
const onBeforeSelect3 = () => {
//   alert('执行 onBeforeSelect 方法');
};

const UploadImg=()=>{
    const history=useHistory()

    const [files, setFiles] = useState([]);
    const [files2, setFiles2] = useState([]);
    const [files3, setFiles3] = useState([]);

    const onSelect = (selFiles) => {
      const newFiles = files.concat(selFiles);
      if (newFiles.length > MAX_FILES_COUNT) {
        Toast.show('最多只能选择5张图片');
        return;
      }
      setFiles(newFiles);
    };
    const onSelect2 = (selFiles) => {
        const newFiles = files2.concat(selFiles);
        if (newFiles.length > MAX_FILES_COUNT) {
          Toast.show('最多只能选择5张图片');
          return;
        }
        setFiles2(newFiles);
      };
      const onSelect3 = (selFiles) => {
        const newFiles = files3.concat(selFiles);
        if (newFiles.length > MAX_FILES_COUNT) {
          Toast.show('最多只能选择5张图片');
          return;
        }
        setFiles3(newFiles);
      };
  
    const remove = (index) => {
      const newFiles = [].concat(files);
      newFiles.splice(index, 1);
      setFiles(newFiles);
      Toast.show('删除成功');
    };
    const remove2 = (index) => {
        const newFiles = [].concat(files);
        newFiles.splice(index, 1);
        setFiles2(newFiles);
        Toast.show('删除成功');
      };
  
      const remove3 = (index) => {
        const newFiles = [].concat(files);
        newFiles.splice(index, 1);
        setFiles3(newFiles);
        Toast.show('删除成功');
      };
    
      const finishClick=()=>{
        // console.log(files)
        if(files.length+files2.length+files3.length>=5){
          alert("图片不能超过5张")
        }else{
          history.push({
            pathname:"/sellHouse",
            state:{
              imgs:files
            }
          })
        }
      }
    const imgRender = () => {
      return files.map((item, index) => {
        return (
          <Badge
            key={+index}
            className="file-picker-item"
            shape="circle"
            text={
              <span className="file-picker-closebtn">
                <Icon type="wrong" />
              </span>
            }
            onClick={() => remove(index)}
          >
            <div className="file-picker-item-img">
              <a href={item.thumbnail} target="_blank" rel="noopener noreferrer">
                <img src={item.thumbnail} alt="" />
              </a>
            </div>
          </Badge>
        );
      });
    };
    const imgRender2 = () => {
        return files2.map((item, index) => {
          return (
            <Badge
              key={+index}
              className="file-picker-item"
              shape="circle"
              text={
                <span className="file-picker-closebtn">
                  <Icon type="wrong" />
                </span>
              }
              onClick={() => remove2(index)}
            >
              <div className="file-picker-item-img">
                <a href={item.thumbnail} target="_blank" rel="noopener noreferrer">
                  <img src={item.thumbnail} alt="" />
                </a>
              </div>
            </Badge>
          );
        });
      };
      const imgRender3 = () => {
        return files3.map((item, index) => {
          return (
            <Badge
              key={+index}
              className="file-picker-item"
              shape="circle"
              text={
                <span className="file-picker-closebtn">
                  <Icon type="wrong" />
                </span>
              }
              onClick={() => remove3(index)}
            >
              <div className="file-picker-item-img">
                <a href={item.thumbnail} target="_blank" rel="noopener noreferrer">
                  <img src={item.thumbnail} alt="" />
                </a>
              </div>
            </Badge>
          );
        });
      };

    const goBack=()=>{
        history.go(-1)
    }
  
    return <div className="uploadImgdiv">
        <header>
            <span onClick={goBack}>&lt;</span>
            <p>上传图片</p>
            <span className="uploadfinish" onClick={finishClick}>完成</span>
        </header>
          <p className="uploadimglength">{`${files.length+files2.length+files3.length}/5`}</p>
        <div className="upimgone">
            <h2>室内图/视频</h2>
            <div className="file-picker-wrapper">
                {imgRender()}
                {files.length < MAX_FILES_COUNT && (
                    <FilePicker
                    multiple
                    className="file-picker-btn"
                    accept="image/*"
                    onBeforeSelect={onBeforeSelect}
                    onChange={onSelect}
                    >
                    <Icon type="add" size="lg" />
                    </FilePicker>
                )}
            </div>
        </div>
        <div className="upimgone">
            <h2>户型图</h2>
            <div className="file-picker-wrapper">
                {imgRender2()}
                {files.length < MAX_FILES_COUNT && (
                    <FilePicker
                    multiple
                    className="file-picker-btn"
                    accept="image/*"
                    onBeforeSelect={onBeforeSelect2}
                    onChange={onSelect2}
                    >
                    <Icon type="add" size="lg" />
                    </FilePicker>
                )}
            </div>
        </div>
        <div className="upimgone">
            <h2>室外图</h2>
            <div className="file-picker-wrapper">
                {imgRender3()}
                {files.length < MAX_FILES_COUNT && (
                    <FilePicker
                    multiple
                    className="file-picker-btn"
                    accept="image/*"
                    onBeforeSelect={onBeforeSelect3}
                    onChange={onSelect3}
                    >
                    <Icon type="add" size="lg" />
                    </FilePicker>
                )}
            </div>
        </div>
    </div>
}

export default UploadImg