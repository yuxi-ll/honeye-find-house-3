import React,{useEffect,useState} from 'react'
import {useHistory} from 'react-router-dom'
import './css/myissue.css'

const MyIssue=()=>{
    const history=useHistory()
    const [issueList,setIssueList]=useState([])

    useEffect(()=>{
        getList()
        console.log(issueList)
    },[])

    const  getList=()=>{
        issueList.push(history.location.params?history.location.params.housemsg.houseAllMsg:[])
        setIssueList([...issueList])
    }

    //点击头部返回上一页
    const goBack=()=>{
        history.go(-1)
        console.log(issueList)
    }

    return <div className="myissuediv">
        <header>
            <span onClick={goBack}>&lt;</span>
            <p>我的发布</p>
        </header>
        <div className="mylist">
            <ul>
                {
                    issueList?issueList.map((item,index)=>{
                        return <li key={index}>
                                <h2>{item.title}</h2>
                                <p>{item.plot}</p>
                            </li>
                    }):[]
                }
            </ul>
        </div>
    </div>
}

export default MyIssue