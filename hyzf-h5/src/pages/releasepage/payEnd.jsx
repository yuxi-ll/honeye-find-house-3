import React,{useEffect} from 'react'
import {useHistory} from 'react-router-dom'
import './css/sell.css'

const PayEnd=()=>{
    const history=useHistory()

    useEffect(()=>{
        // console.log(history.location.params)
    },[])

    //点击头部返回上级
    const goBack=()=>{  
        history.push("/layout/release")
    }

    //继续发布
    const goon=()=>{
        history.push("/layout/release")
    }

    //查看列表
    const lookList=()=>{
        history.push({
            pathname:"/myissue",
            params:{
                housemsg:history.location.params
            }
        })
    }

    return <div className="payenddiv">
        <header>
            <span onClick={goBack}>&lt;</span>
            <p>发布成功</p>
        </header>
        <h1>发布成功</h1>
        <div className="history">
            <button onClick={goon}>继续发布</button>
            <button onClick={lookList}>发布列表</button>
        </div>
    </div>
}

export default PayEnd