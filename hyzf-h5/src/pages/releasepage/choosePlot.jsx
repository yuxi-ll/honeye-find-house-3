import React,{useState,useEffect} from 'react'
import {useHistory} from 'react-router-dom'
import './css/sell.css'
import axios from '../../utils/request'

const ChoosePlot=()=>{
    const history=useHistory()
    const [plotlist,setPlotList]=useState([])
    const [search,setSeatch]=useState('')
    const goBack=()=>{
        history.go(-1)
    }
    useEffect(()=>{
            axios.get(`/area/community?name=${search}&id=AREA|88cff55c-aaa4-e2e0`).then(res=>{
                console.log(res.data.body)
                setPlotList(res.data.body)
            })
      },[])
      const searchChange=(e)=>{
          setSeatch(e.target.value)
      }
      const searchPlotClick=()=>{
        axios.get(`/area/community?name=${search}&id=AREA|88cff55c-aaa4-e2e0`).then(res=>{
            console.log(res.data.body)
            setPlotList(res.data.body)
        })
      }
      const pickThisLi=(name)=>{
          history.push({
              pathname:"/sellHouse",
              params:{
                  name:name
              }
          })
      }
    return <div className="chooseplot">
        <header>
            <span onClick={goBack}>&lt;</span>
            <p>选择小区</p>
        </header>
        <div className="body">
            <div className="chooseplotsearch">
                <input type="text" value={search} onChange={searchChange}/>
                <button onClick={searchPlotClick}>搜索</button>
            </div>
            <ul className="chooseplotlist">
                {
                    plotlist.map((item,index)=>{
                        return <li key={index} onClick={()=>{pickThisLi(item.communityName)}}>
                            <p>{item.communityName}</p>
                            <span>&gt;</span>
                        </li>
                    })
                }
            </ul>
        </div>
    </div>
}

export default ChoosePlot