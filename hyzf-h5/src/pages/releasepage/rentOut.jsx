import React, { useState,useEffect, useRef, useReducer } from 'react'
import {  Picker } from 'zarm';
import './css/sell.css'
import {useHistory} from 'react-router-dom'
import axios from '../../utils/request'

  const TYPE_DATA = [  //选择类型选择器数据
    { value: '1', label: '高层住宅' },
    { value: '2', label: '小高层住宅' },
    { value: '3', label: '商铺/门面房' },
    { value: '4', label: '厂房' },
    { value: '5', label: '写字楼' },
  ];
  const AREA_DATA = [  //选择区域选择器数据
    { value: '1', label: '主城区' },
    { value: '2', label: '宿豫区' },
    { value: '3', label: '宿城新区' },
    { value: '4', label: '苏宿园区' },
    { value: '5', label: '开发区' },
  ];

const RentHouse=()=>{
    const myRef = useRef();//引入ref
    const history=useHistory() //引入路由 
    const [locationName,setLocationName]=useState('')  //接收选择小区的路由传参
    const [houseType,setHouseType]=useState('') //选择房型数据
    const [houseArea,setHouseArea]=useState('') //选择区域数据
    const [chooseFlag,setChooseFlag]=useState('type') //动态选择器控制字段
    const [visibleOne,setVisebleOne]=useState(false)  //选择房型和区域弹框
    const [visibleThree,setVisibleThree]=useState(false)  //三级选择器控制弹框
    const [MULTI_DATA,setMULTI_DATA]=useState([])  //三级选择器数据
    const [threeObj,setThreeObj]=useState({type:'',dire:'',finish:''})  //三级选择器选中数据
    const [checkFlag,setCheckFlag]=useState(false)
    const [houseAllMsg,setHouseAllMsg]=useState({})
    const alldata={...houseAllMsg}
    const initState = {
        single: {
            visible: visibleOne,
            value: '',
            dataSource: TYPE_DATA,
            dataSources: AREA_DATA,
        },
        multi: {
            visible: visibleThree,
            value: [],
            dataSource: MULTI_DATA,
        },
    }
    
    const goBack=()=>{  //点击头部返回上级
        history.push("/layout/release")
    }
    
    useEffect(()=>{
        setLocationName(history.location.params?history.location.params.name:'')  //存储小区参数
        getHouseMsg()
    },[])

    const getHouseMsg=()=>{
        axios.get("/houses/params").then(res=>{
            MULTI_DATA.push(res.data.body.floor,res.data.body.oriented,res.data.body.supporting)
        })
    }

    const choosePlotClick=()=>{ //点击选择小区跳转
        history.push("/choosePlot")
    }
    
    const setValue = (key, value) => {
        setVisibleThree(false)
      };
    
    //picker选择器确定事件
    const selectOk=(selected)=>{
        
        setVisebleOne(false)
        if(chooseFlag==="type"){  //选择房型确定
            setHouseType(selected[0].label)
            alldata.plot=locationName
            alldata.type=selected[0].label
            setHouseAllMsg(alldata)
        }else{  //选择区域确定
            setHouseArea(selected[0].label)
            alldata.area=selected[0].label
            setHouseAllMsg(alldata)
        }
        setChooseFlag('')
    }
    //选择房型
    const chooseHouseType=()=>{
        setChooseFlag('type')
        setVisebleOne(true)
        console.log(chooseFlag)
        console.log(initState.single.dataSource)
    }
    //选择区域
    const chooseHouseArea=()=>{
        setChooseFlag('')
        setVisebleOne(true)
        console.log(initState.single.dataSource)
        console.log(chooseFlag)
    }

    const selectThreeOk=(selected)=>{
        console.log('Multi Picker onOk: ', selected);
        setValue(
            'multi',
            selected.map((item) => item.value),
        );
        setVisibleThree(false);
        setThreeObj({type:selected[0].label,dire:selected[1].label,finish:selected[2].label})
    }
    //上传图片跳转
    const pushUploadImg=()=>{
        history.push("/uploadImg")
    }
    const readRule=(e)=>{
        console.log(e.target.checked)
        setCheckFlag(e.target.checked)
    }
    const submitAndPay=()=>{
        if(checkFlag){
            if(!houseAllMsg.area||!houseAllMsg.plot||!houseAllMsg.type||!houseAllMsg.three){ //判断租房信息是否没有完善
                alert("请完善信息")
            }else{
                history.push("/layout/release")
                console.log(houseAllMsg)
            }
        }else{
            alert("请先勾选协议")
        }
    }
    return <div className="sellhousediv">
         <Picker
            visible={initState.single.visible}
            dataSource={chooseFlag==='type'?initState.single.dataSource:initState.single.dataSources}
            onOk={selectOk}
            onCancel={() => setVisebleOne(false)}
        />
         <Picker
            visible={initState.multi.visible}
            value={initState.multi.value}
            dataSource={initState.multi.dataSource}
            onOk={selectThreeOk}
            onCancel={() => setVisibleThree(false)}
        />
     <div id="test-div" style={{ position: 'relative', zIndex: 1 }} ref={myRef} />
        <header>
            <span onClick={goBack}>&lt;</span>
            <p>有房出售</p>
        </header>
        <div className="body">
                <div className="sellfirstli">
                    <p> 发布本条信息将收费10.00元</p>
                </div>
                <div className="uploadli" onClick={pushUploadImg}>
                    <div className="l">
                        +
                    </div>
                    <div className="c">
                        <p>上传照片</p>
                        <p>只能上传房屋照片，不能含有文字，数字，网站，名片，水印等，所有类别图片总计20张</p>
                    </div>
                    <div className="r">&gt;</div>
                </div>
                <div className="choosemsg">
                    <ol>
                        <li onClick={choosePlotClick}>
                            <div className="c">
                                <p className="choosetitle">小区</p>
                                <p>{locationName?locationName:'请选择小区'}</p>
                            </div>
                            <div className="r">&gt;</div>
                        </li>
                        <li onClick={chooseHouseType}>
                            <div className="c" >
                                <p className="choosetitle">房产类型</p>
                                <p>{houseType?houseType:'请选择'}</p>
                            </div>
                            <div className="r">&gt;</div>
                        </li>
                        <li onClick={chooseHouseArea}>
                            <div className="c">
                                <p className="choosetitle">区域</p>
                                <p>{houseArea?houseArea:'请选择'}</p>
                            </div>
                            <div className="r">&gt;</div>
                        </li>
                        <li  onClick={() => setVisibleThree(true)}>
                            <div className="c">
                                <p className="choosetitle">户型|朝向|装修</p>
                                <p>{threeObj.type?`${threeObj.type},${threeObj.dire},${threeObj.finish}`:'请选择'}</p>
                            </div>
                            <div className="r">&gt;</div>
                        </li>
                        <li className="fillInLi">
                            <span>所在楼层</span><input type="text" placeholder="请填写"/>层
                            <span>共</span><input type="text" placeholder="请填写"/>层
                        </li>
                        <li  className="fillInr">
                            <div className="c">
                                <p className="choosetitle">面积</p>
                                <p><input type="text" placeholder="请输入"/></p>
                            </div>
                            <div className="r">m²</div>
                        </li>
                        <li className="fillInr">
                            <div className="c">
                                <p className="choosetitle">售价</p>
                                <p><input type="text" placeholder="请输入"/></p>
                            </div>
                            <div className="r">万元</div>
                        </li>
                    </ol>
                </div>
                <div className="choosediv2">
                    <ol>
                        <li>
                            <p className="title">标签</p>
                            <div className="spans">
                                <span>新上房源</span>
                                <span>小户型</span>
                                <span>有电梯</span>
                                <span>优质教育</span>
                                <span>低总价</span>
                                <span>满两年</span>
                                <span>满五年</span>
                                <span>随时看房</span>
                                <span>满五唯一</span>
                                <span>底首付</span>
                                <span>两室朝阳</span>
                                <span>精修</span>
                                <span>南北通透</span>
                                <span>品质小区</span>
                            </div>
                        </li>
                        <li  className="fillInr">
                            <div className="c">
                                <p className="choosetitle">标签</p>
                                <p><input type="text" placeholder="请输入"/></p>
                            </div>
                            <div className="r">&gt;</div>
                        </li>
                        <li  className="fillInr">
                            <div className="c">
                                <p className="choosetitle">描述</p>
                                <p><input type="text" placeholder="请输入"/></p>
                            </div>
                            <div className="r">&gt;</div>
                        </li>
                        <li  className="fillInr">
                            <div className="c">
                                <p className="choosetitle">联系人</p>
                                <p><input type="text" placeholder="请输入"/></p>
                            </div>
                            <div className="r">&gt;</div>
                        </li>
                        <li className="buyhousetime">
                            <p>有效时间</p>
                            <span>30天</span>
                        </li>
                    </ol>
                </div>
                <div className="foot">
                    <button onClick={submitAndPay}>发布并支付</button>
                    <p><input type="checkbox" checked={checkFlag} onChange={readRule}/><span>已阅读并接受《洪烨找房源信息发布规则》</span></p>
                </div>
        </div>
    </div>
}

export default RentHouse