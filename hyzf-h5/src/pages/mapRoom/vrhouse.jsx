import React, { useEffect, useRef } from 'react'
import * as THREE from 'three'  // 引入 Three.js 插件
import { useHistory } from 'react-router-dom'
import a from "../../assets/img/5.png"
import b from "../../assets/img/6.png"
import c from "../../assets/img/3.png"
import d from "../../assets/img/4.png"
import f from "../../assets/img/1.png"
import e from "../../assets/img/2.png"
let ww = window.innerWidth;
let wh = window.innerHeight;
const Vrhouse = () => {
    const history = useHistory()
    let scene, camera, renderer, mesh, startX, startY, startLon, startLat;

    let lon = 90; //把鼠标在屏幕上的横(X)偏移量作为旋转角度的基准
    let lat = 0;  //吧鼠标在屏幕上的纵(Y)偏移量作为旋转角度的基准
    let phi = 0;  //相机的横切面,到Y轴的偏移弧度
    let theta = 0;//相机的竖切面,到X轴的偏移弧度
    let target = new THREE.Vector3();//相机照向的那个方向
    function init() {
        //场景
        scene = new THREE.Scene();
        //相机
        camera = new THREE.PerspectiveCamera(75, ww / wh, 1, 10000)
        //渲染器
        renderer = new THREE.WebGLRenderer();
        renderer.setSize(ww, wh)//渲染器宽高比
        renderer.setPixelRatio(window.devicePixelRatio)
        looks.current.appendChild(renderer.domElement)
        createMesh()
        looks.current.addEventListener('touchstart', handleTouchStart)
        looks.current.addEventListener('touchmove', handleTouchMove)
        looks.current.addEventListener('touched', handleTouchEnd)
    }
    const createMesh = () => {
        let array = [
            loadTexture(a),
            loadTexture(b),
            loadTexture(c),
            loadTexture(d),
            loadTexture(e),
            loadTexture(f),
        ]
        //建立几何体
        let geometry = new THREE.BoxGeometry(300, 300, 300);
        //材料
        let material = new THREE.MultiMaterial(array)
        //创建网格
        mesh = new THREE.Mesh(geometry, material)
        //翻转盒子
        mesh.geometry.scale(1, 1, -1)
        //将网格放到场景中
        scene.add(mesh)
    }
    //将图片贴合做成相应材料
    function loadTexture(url) {
        let textureLoader = new THREE.TextureLoader();
        let texture = textureLoader.load(url)//将图片组成纹理
        texture.needUpdate = true//更新使用新纹理
        //材料
        let basicMaterial = new THREE.MeshBasicMaterial({
            map: texture
        })
        return basicMaterial
    }
    function update() {
        lon += 0.1
        lat = Math.max(-85, Math.min(85, lat))
        phi = THREE.Math.degToRad(90-lat)
        theta = THREE.Math.degToRad(lon)
        target.x = 500 * Math.sin(phi) * Math.cos(theta)
        target.y = 500 * Math.cos(phi)
        target.z = 500 * Math.sin(phi) * Math.sin(theta)
        camera.lookAt(target)
        renderer.render(scene, camera)
    }
    //动画
    function animate() {
        requestAnimationFrame(animate)
        update()
    }
    function handleTouchStart(e) {
        // e.preventDefault();
        startX = e.touches[0].pageX;
        startY = e.touches[0].pageY;
        startLon = lon;
        startLat = lat;
    };

    function handleTouchMove(e) {
        // e.preventDefault();
        lon = (startX - e.touches[0].pageX) * 0.2 + startLon;
        lat = (e.touches[0].pageY - startY) * 0.2 + startLat;
        update();
    };

    function handleTouchEnd(e) {
        e.preventDefault();
    };
    
    const looks = useRef(null);
    useEffect(() => {
        init()
        animate()

    }, [])
    return <div className="lookHouse" ref={looks}>
    <div className='go_1' style={{height: '26px', width: '100%', lineHeight: '26px', fontWeight: 'bold', padding: '0 12px', fontSize: '14px'}} onClick={() =>{ history.go(-1); console.log(1)}}>&lt;</div>
    </div>
}

export default Vrhouse
