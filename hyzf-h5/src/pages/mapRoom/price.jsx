import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { getSelectHou, getArea } from '../../api/index'
import { PickerView, Toast, Button, Loading } from 'zarm';
import HouseItem from '../../components/HouseItem'
import styles from './area.module.css'

const Price = () => {
    const history = useHistory()
    const [selectArea, setSelectArea] = useState([])
    const [isselect, setIsselect] = useState(true)
    const [cityValue, setCityValue] = useState([]);
    const [content, setContent] = useState([]);
    const [houseList, setHouseList] = useState([])

    useEffect(() => {
        getAreaList()
    }, [])
    //获取区域数据
    const getAreaList = async () => {
        let { label, value } = JSON.parse(localStorage.getItem('hkzf_city'))
        Loading.show()
        const { data: res } = await getArea({ id: value })
        selectArea[0] = res.body.price
        setSelectArea([...selectArea])
        Loading.hide()
    }
    // //点击城市获取市区列表
    const changeCity = async (selected) => {
        console.log(selected)
        setContent([...selected])
    }
    //获取筛选后的房屋列表
    const getSelectHouse = async () => {
        const { data: res } = await getSelectHou({
            cityId: localStorage.getItem('hkzf_city'),
            price: content[0].value,
        })
        setHouseList([...res.body.list])
        Toast.show('查询成功')
        setIsselect(false)
    }
    //console.log(selectArea)
    return <div className={styles.area}>
        <div className={styles.mapheader}>
            <span onClick={() => history.push('/mapRoom')}>&lt;</span>
            <span>租金筛选</span>
            <span>···</span>
        </div>
        <div className={styles.mapmain}>
            <div className={styles.search}>
                <span>上海<i className={styles.trangle}></i></span>
                <div className={styles.searchli}><i><img src="https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng453dc40899615ef956fb1319425f8b80ff7071d4627831942e4ce1ede2f8c2ba" alt="" /></i><span>请输入搜索区域/商圈</span></div>
                <span className={styles.searchBtn}>搜索</span>
            </div>
            <div className={styles.select}>
                <div className={styles.selectnav}>
                    <span onClick={() => history.push('/areamap')}>区域<i className={styles.trangle}></i></span>
                    <span className={styles.houseactive} onClick={() => history.push('/price')}>价格<i className={styles.trangle}></i></span>
                    <span onClick={() => history.push('/house')}>户型<i className={styles.trangle}></i></span>
                    <span onClick={() => history.push('/methods')}>方式<i className={styles.trangle}></i></span>
                </div>
                <div className={styles.selection} style={{ display: isselect ? 'block' : 'none' }}>
                    <div className={styles.cityselect}>
                        <PickerView
                            value={cityValue}
                            dataSource={selectArea}
                            onChange={(selected) => {
                                changeCity(selected)
                            }}
                        />
                    </div>
                    <div>
                        <Button block theme="primary" onClick={getSelectHouse}>
                            确认
                        </Button>
                    </div>
                </div>
                <div className={styles.houseList}>
                    {
                        houseList.map(item => <div key={item.houseCode} className={styles.houseItems}>
                            <HouseItem key={item.houseCode} {...item}></HouseItem>
                        </div>)
                    }
                </div>
            </div>
        </div>
    </div>
}

export default Price