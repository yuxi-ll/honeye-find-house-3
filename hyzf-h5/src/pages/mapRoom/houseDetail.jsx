import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { getHouseDet, getHouse, addFavorites, delFavorites } from '../../api/index'
import { Carousel, Toast, Loading } from 'zarm';
import HouseItem from '../../components/HouseItem'
import styles from './houseDetail.module.css'
let url = 'http://157.122.54.189:9060'
const BMap = window.BMap;
let map = null;
const HouseDetail = () => {
    const history = useHistory()
    const [house, setHouse] = useState([])
    const [houseList, setHouseList] = useState([])
    const [isFavorite, setIsFavorite] = useState(false)
    useEffect(() => {
        getHouseDetail()
        getHouseList()
    }, [])
    const getHouseDetail = async () => {
        Loading.show()
        const { data: res } = await getHouseDet({ params: history.location.state.houseCode })
        house.push(res.body)
        Loading.hide()
        setHouse([...house])
        initMap(res.body.coord)
    }
    //初始化地图
    const initMap = (coord) => {
        const { latitude, longitude } = coord
        const point = new BMap.Point(longitude, latitude)
        //实例化地图
        map = new BMap.Map('container')
        map.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
        map.centerAndZoom(point, 18);
        map.addControl(new BMap.NavigationControl())
        map.addControl(new BMap.ScaleControl());
    }
    //获取周边房源信息
    const getHouseList = async () => {
        console.log(history.location.state.cityId)
        const { data: res } = await getHouse({ cityId: history.location.state.cityId })
        console.log(res)
        Toast.show('加载成功')
        setHouseList([...res.body.list])
    }
    //点击收藏房屋信息
    const collect = async () => {
        if (isFavorite) {
            const { data: res } = await addFavorites({ id: history.location.state.houseCode })
            console.log(res)
            // setIsFavorite(!isFavorite)
        } else {
            const { data: res } = await delFavorites({ id: history.location.state.houseCode })
            console.log(res)
        }
        Toast.show(isFavorite ? '已收藏' : null)
        setIsFavorite(!isFavorite)
    }
    return <div className={styles.detail}>
        <div className={styles.header}>
            <div className={styles.back}><span onClick={() => history.go(-1)}>&lt;</span></div>
            {
                house.length ? <Carousel
                    autoPlay
                    loop
                    direction="left"
                    style={{ height: '200px', width: '100%', position: 'absolute', left: 0, top: 0, zIndex: -1 }}
                // onChangeEnd={(index) => {
                //     console.log(`onChangeEnd: ${index}`);
                // }}
                >
                    {house[0].houseImg.map((item, index) => <div key={index}>
                        <img src={url + item} alt="" style={{ height: '200px', width: '100%' }} />
                    </div>)}

                </Carousel> : null
            }
        </div>
        {house.length ? <div className={styles.main}>
            <h3 className={styles.h3}>{house[0].title}</h3>
            <p className={styles.vr} onClick={() => history.push({ pathname: '/vr', state: { url: house[0].houseImg } })}><span>vr</span></p>
            <p className={styles.tag}>
                {
                    house[0].tags.map((item, index) => <span key={index} className={styles.detailTags}>{item}</span>)
                }
            </p>
            <p className={styles.desc}>{house[0].description}</p>
            <span className={styles.month}>参考月贷</span>
            <div className={styles.monthBox}>
                <div className={styles.monthmains}>
                    <div className={styles.top}>
                        <span className={styles.deng}>等额本息</span>
                        <div className={styles.ben} />
                        <span className={styles.word19}>等额本金</span>
                    </div>
                    <div className={styles.txt9}><span>月供&nbsp;¥6512.06（30年）</span></div>
                    <div className={styles.mod3} />
                    <div className={styles.mod4}>
                        <span className={styles.word20}>总价</span>
                        <span className={styles.word20}>首付</span>
                        <span className={styles.word20}>贷款</span>
                        <span className={styles.word20}>利息</span>
                    </div>
                    <div className={styles.mod5}>
                        <span className={styles.txt10}>138.00万</span>
                        <span className={styles.txt10}>41万（3成）</span>
                        <span className={styles.txt10}>91.00万</span>
                        <span className={styles.txt10}>88.33万</span>
                    </div>
                </div>
            </div>
            <span className={styles.month}>位置及周边</span>
            <div id='container' style={{ width: '100%', height: '180px' }}></div>
            <span className={styles.month}>推荐房源</span>
            <div className={styles.houseList}>
                {
                    houseList.map(item => <div key={item.houseCode} className={styles.houseItems}>
                        <HouseItem key={item.houseCode} {...item}></HouseItem>
                    </div>)
                }
            </div>
        </div> : null}
        <div className={styles.footer}>
            <span></span>
            <span onClick={collect}><i className='iconfont icon-aixin' style={{ color: isFavorite ? 'lightcoral' : '' }}></i>收藏</span>
            <div>
                <span onClick={() => history.push('/chat')} className={styles.chat}>在线咨询</span>
                <span onClick={() => history.push('/call')} className={styles.call}>电话咨询</span>
            </div>
        </div>
    </div>
}

export default HouseDetail