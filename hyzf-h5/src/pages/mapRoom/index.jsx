import React, { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import Map from './map'
import styles from './mapRoom.module.css'

const MapRoom = () => {
    const history = useHistory()
    const [isbuy, setIsbuy] = useState(true)//是买房还是租房

    return (
        <div className={styles.map}>
            <div className={styles.mapheader}>
                <span onClick={() => history.push('/layout/home')}>&lt;</span>
                <span>地图找房</span>
                <span>···</span>
            </div>
            <div className={styles.mapmain}>
                <div className={styles.search}>
                    <span>上海<i className={styles.trangle}></i></span>
                    <div className={styles.searchli}><i><img src="https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng453dc40899615ef956fb1319425f8b80ff7071d4627831942e4ce1ede2f8c2ba" alt="" /></i><span>请输入搜索区域/商圈</span></div>
                    <span className={styles.searchBtn}>搜索</span>
                </div>
                <div className={styles.nav}>
                    <div className={styles.buyhouse} onClick={() => setIsbuy(true)}>
                        <img src="https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPnga8f95ff5bdea29255e4d33264c80289249f089f6cd6faf351497a98dc8121029" alt="" />
                        <span className={isbuy ? styles.houseactive : ''}>买房</span>
                    </div>
                    <div className={styles.renthouse} onClick={() => setIsbuy(false)}>
                        <img src="https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngedbc83e18d2200774165a95e2033e4169fab72732a3a87570c055f6c252a2931" alt="" />
                        <span className={!isbuy ? styles.houseactive : ''}>租房</span>
                    </div>
                </div>
                <div className={styles.select}>
                    <div className={styles.selectnav}>
                        <span onClick={() => history.push('/areamap')}>区域<i className={styles.trangle}></i></span>
                        <span onClick={() => history.push('/price') }>价格<i className={styles.trangle}></i></span>
                        <span onClick={() => history.push('/house') }>户型<i className={styles.trangle}></i></span>
                        <span onClick={() => history.push('/methods') }>距离<i className={styles.trangle}></i></span>
                    </div>
                </div>
                <Map></Map>
            </div>
        </div>
    )
}

export default MapRoom
