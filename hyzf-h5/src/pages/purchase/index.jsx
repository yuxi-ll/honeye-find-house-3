import Reactn, { useState, useEffect } from 'react'
import ajax from "axios"
import "./index.css"
import { Select, Cell } from 'zarm';

const Purchase = (props) => {
    let [list, setList] = useState([]);//总数据
    let [lists, setLists] = useState([]);//筛选完的数据
    //渲染数据
    useEffect(() => {
        handlewanted()//渲染房间数据
    }, [])
    const handlewanted = async () => {
        const homelist = await ajax.get("/api/hua")
        setList(list => homelist.data.hua)
        setLists(lists => homelist.data.hua)
        console.log(list, 'nhd')
    }
    //跳转到聊天
    const handleChat = () => {
        props.history.push("/chat")
    }
    //跳转到打电话
    const handleCall = () => {
        props.history.push("/call")
    }
    //返回上一级
    const handleGoback=()=>{
        props.history.go(-1)
    }
    // 级联数据
    const CASCADE_DATA = [
        {
            value: '1',
            label: '房产类型',
            children: [
                { value: '11', label: '多层住宅' },
                { value: '12', label: '高层住宅' },
                { value: '13', label: '小高层住宅' },
                { value: '14', label: '商铺/门面房' },
                { value: '15', label: '厂房' },
                { value: '16', label: '写字楼' },
                { value: '17', label: '独院/别墅' },
                { value: '18', label: '车库' },
            ],
        },
        {
            value: '2',
            label: '上海市',
            children: [
                { value: '21', label: '海淀区' },
                { value: '22', label: '朝阳区' },
                { value: '23', label: '丰台区' },
                { value: '24', label: '大兴区' },
                { value: '25', label: '丰台区' },
                { value: '26', label: '延庆区' },
                { value: '27', label: '昌平区' },
                { value: '28', label: '海淀区' },
            ],
        },
    ];

    const Demo = () => {
        const [value, setValue] = useState([]);
        const [dataSource, setDataSource] = useState([]);
        const [wheelDefaultValue, setWheelDefaultValue] = useState([]);
        const [select, setSelect] = useState([])//筛选
        useEffect(() => {
            setTimeout(() => {
                setDataSource(CASCADE_DATA);
                setWheelDefaultValue(['1', '12']);
            }, 0);
        }, []);

        return (
            <Cell title="城市">
                <Select
                    value={value}
                    wheelDefaultValue={wheelDefaultValue}
                    dataSource={dataSource}
                    onOk={(selected) => {
                        console.log('Select onOk: ', selected);
                        setSelect([...selected])
                        list = lists.filter(item => item.type.includes(selected[1].label))
                        setList(list);
                    }}
                />

            </Cell>
        );
    };

    return (
        <div className="purchase">
            <p className="header">
                <i className="iconfont icon-zuojiantou" onClick={handleGoback}></i>
                <span>房屋求购</span>
                <span></span>
            </p>

            <Demo />
            <div className="house">
                {
                    list.map((item, index) => <dl key={index}>
                        <dd>
                            <p>
                                <span>{item.name}</span>
                                <span>{item.time}天前</span>
                                <span>{item.type}</span>
                                <span>{item.region}</span>
                            </p>

                            <p>{item.title}</p>
                            <hr color="#eee" />
                            <p className="p3">
                                <span>意向区域:{item.area}</span>
                                <span>意向价格:  <span className="red">{item.price}</span>/月</span>


                                <span className="icons">
                                    <i className="iconfont icon-icon-test1" onClick={handleChat}></i>
                                    <i className="iconfont icon-dianhua1" onClick={handleCall}></i>
                                </span>

                            </p>

                        </dd>

                    </dl>
                    )
                }
            </div>
        </div>
    )
}
export default Purchase
