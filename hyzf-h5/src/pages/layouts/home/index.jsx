import React, { useEffect,useState,useReducer} from 'react'
import axios from '../../../utils/request';
import ajax from 'axios'
import { Carousel, Picker, Toast } from 'zarm';
import growimg from '../../../assets/homeImg/big.png'
import './index.css'
import './common.css'
const reducer = (state, action) => {
  const { type, value } = action;
  switch (type) {
    case 'visible':
      return {
        ...state,
        visible: !state.visible,
      };
    case 'value':
      return {
        ...state,
        value
      };
    default:
      return {
        ...state
      }
  }
};
const Home = (props) => {
    useEffect(()=>{
        getSwiperList()
        getCityList()
    },[])
    const getSwiperList= async ()=>{
        const swiper= await axios.get('/home/swiper');
        setItems(ITEMS=>swiper.data.body)
    }
    const getCityList=async ()=>{
      const cityList=await axios.get('/area/city?level=1');
      console.log(cityList,'city')
      setSINGLE_DATA(cityList.data.body.filter(v=>v.label&&v.value))
    }
    const [ITEMS, setItems] = useState([])
    const [list, setList] = useState([])
    const [houseList, setHouseList] = useState([])
    const [SINGLE_DATA,setSINGLE_DATA] = useState([]);
    const initState = {
      visible: false,
      value: '上海',
      //dataSource: SINGLE_DATA,
    };
    const [state, dispatch] = useReducer(reducer, initState);
    //代替生命周期
    useEffect(() => {
        handlehouse()//热门楼盘数据
        handleagent()//经纪人数据      
    }, [])
    const setVisible = (key) => {
        dispatch({ type: 'visible', key });
    };

    const setValue = (value) => {
        dispatch({ type: 'value', value });
    };
    //渲染热门楼盘数据
    const handlehouse = async () => {
        const homelist = await ajax.get("/api/house")
        setList(list => homelist.data.house)
        // console.log(list)
    }
    //
    const handleagent = async () => {
        const agentlist = await ajax.get("/api/agent")
        setHouseList(houseList => agentlist.data.agent)
    } 
      const contentRender = () => {
        return ITEMS.map((item) => {
          return (
            <div className="carousel__item__pic" key={item.id}>
              <img src={`https://api-haoke-web.itheima.net`+item.imgSrc} alt="" draggable={false} />
            </div>
          );
        });
      };
    const handlePushBuyHouse=()=>{
      props.history.push('/buyHouse')
    }
    const handlePushRentingHouse=()=>{
      props.history.push('/rentingHouse')
    }
    const handlePushWanted=()=>{
      props.history.push('/wanted')
    }
    const handlePushPurchase=()=>{
      props.history.push('/purchase')
    }
    const handlePushMapRoom=()=>{
      props.history.push('/mapRoom')
    }
    const handlePushInformation=()=>{
      props.history.push('/information')
    }
    const handlePushHousingRing=()=>{
      props.history.push('/housingRing')
    }
    const handlePushBroker=()=>{
      props.history.push('/broker')
    }
    const handlePushGroupPurchase=()=>{
      props.history.push('/groupPurchase')
    }
    const handlePushLiveSeeRoom=()=>{
      props.history.push('/liveSeeRoom')
    }
    return (
        <div className='home flex-col'>
            <Picker
              visible={state.visible}
              value={state.value}
              dataSource={SINGLE_DATA}
              onOk={(selected) => {
                Toast.show('切换成功');
                setValue(
                  selected.map((item) => item.label),
                );
                setVisible(false);
              }}
              onCancel={() => setVisible(false)}
            />
            <div className="wrap8 flex-col">
              <div className="block11 flex-col">                
                <div className="bd6 flex-row">
                  <span className="info26">{state.value}</span>
                  <img
                    className="icon12"
                    src={
                      'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngb760d2a5a486f99adc0929f9b34f4e7d1c6ee8c0496e3368deaa16ba2472be81'
                    }
                    alt=''
                    onClick={()=>setVisible(true)}
                  />
                  <div className="group17 flex-col">
                    <div className="box2 flex-row">
                      <img
                        className="label15"
                        src={
                          'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng802c3750a7f135ca6099dcc5e16715959f77bcdd5ff5258ca28b7c27779d24d9'
                        }
                        alt=''
                      />
                      <span className="txt22">请输入查询的房源</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <Carousel
                autoPlay
                loop
                direction="left"
                onChangeEnd={(index) => {
                console.log(`onChangeEnd: ${index}`);
                }}
            >
                {contentRender()}
            </Carousel>
            <div className="wrap1 flex-col">
              <div className="main2 flex-row">
                  <div className="mod1 flex-col" onClick={handlePushBuyHouse}>
                    <img
                      className="icon2"
                      src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngc1b4ef79487e086616c7a421a785028e06c955564fd883c0ffe31de93f98e3b4'
                      }
                      alt=''
                    />
                  </div>
                  <div className="mod2 flex-col" onClick={handlePushRentingHouse}>
                    <img
                      className="label3"
                      src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng87f28225cca643eb0dc361b64c7646c1cf74db2648e1a00ff9424eb242b407d0'
                      }
                      alt=''
                    />
                  </div>
                  <div className="mod3 flex-col" onClick={handlePushWanted}>
                    <img
                      className="label4"
                      src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng6f890951248980d286df0638c1e9b79b2729990111cae63c334f67749ca67f28'
                      }
                      alt=''
                    />
                  </div>
                  <div className="mod4 flex-col" onClick={handlePushPurchase}>
                    <img
                      className="label5"
                      src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng5cbb16ad02f3693d3c9476cdd138fe845becc0d1d79d3e878a0c5ad295d787e3'
                      }
                      alt=''
                    />
                  </div>
                  <div className="mod5 flex-col" onClick={handlePushMapRoom}>
                    <img
                      className="label6"
                      src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngef766b246c2e43870d014efbb5589973e6e4344ef191c4c3ef64b78e83723dcf'
                      }
                      alt=''
                    />
                  </div>
                </div>
              <div className="main3 flex-row">
                  <span className="txt1">买房</span>
                  <span className="txt2">租房</span>
                  <span className="word2">求租</span>
                  <span className="word3">求购</span>
                  <span className="word4">地图</span>
                </div>
              <div className="main4 flex-row">
                  <div className="block1 flex-col" onClick={handlePushInformation}>
                    <img
                      className="icon3"
                      src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng517b7df41033a0afd16cb661aece16bbc4591fae0962b7fa2f02807f98b1d920'
                      }
                      alt=''
                    />
                  </div>
                  <div className="block2 flex-col" onClick={handlePushHousingRing}>
                    <img
                      className="icon4"
                      src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng7747faa895340602db6cd5a9ebb2756c0c9da808ed11c3d1392ec555b5512fc8'
                      }
                      alt=''
                    />
                  </div>
                  <div className="block3 flex-col" onClick={handlePushBroker}>
                    <img
                      className="label7"
                      src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng7fc40b86db443bf1dd3199a150b57943d324cb6649e43d239980f37da526e1b3'
                      }
                      alt=''
                    />
                  </div>
                  <div className="block4 flex-col" onClick={handlePushGroupPurchase}>
                    <img
                      className="label8"
                      src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng0163e132b75e232a622ca420f36e221d7c73aa1134197f8587fc2816fbd2a87f'
                      }
                      alt=''
                    />
                  </div>
                  <div className="block5 flex-col" onClick={handlePushLiveSeeRoom}>
                    <img
                      className="icon5"
                      src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng437ed7517db7d3b486ceaabec2d29d911fd9bf3411ae945090b89951a9b45c7a'
                      }
                      alt=''
                    />
                  </div>
                </div>
              <div className="main5 flex-row">
                  <span className="txt3">资讯</span>
                  <span className="info1">楼市</span>
                  <span className="txt4">经纪</span>
                  <span className="info2">团购</span>
                  <span className="txt5">直播</span>
                </div>
            </div>
            {/* 楼盘 */}
            <div className="broker">
              <span>热门楼盘</span>
              <span onClick={handlePushBuyHouse}>更多</span>
            </div>
                <div className="house">
                    {
                        list.map((item, index) => <dl key={index}>
                            <dt>
                                <img src={item.img} alt="" />
                            </dt>
                            <dd>
                                <p className="cu">{item.headline}</p>
                                <p className="red">{item.money}元/平</p>
                                <p>{item.address}</p>
                                <p>{item.advantage} {item.advantage}  {item.advantage}</p>
                            </dd>
                            <dd className="dd2">
                                <p>住宅</p>
                                <i className="iconfont icon-aixin"></i>
                            </dd>

                        </dl>)
                    }
                </div>
                <div className="broker">
                    <span>经纪人</span>
                    <span onClick={handlePushBroker}>更多</span>
                </div>
                <div className="argent">
                    {
                        houseList.map((item, index) => <dl key={index}>
                            <dt>
                                <img src={item.tu} alt="" />
                            </dt>
                            <dd>
                                <p>{item.ming}</p>
                                <p>{item.biao}</p>
                            </dd>
                            <button>
                                <i className="iconfont icon-icon-test1"></i>
                                  资讯
                            </button>
                        </dl>)
                    }
                </div>
                <div className="grow">
                    <img src={growimg} alt="" />
                </div>
                <img src="e" alt="" />

                <div className="broker">
                  <span>二手房</span>
                  <span onClick={handlePushBuyHouse}>更多</span>
                </div>
                <div className="secondhouse">
                    {
                        list.map((item, index) => <dl key={index}>
                            <dt>
                                <img src={item.img} alt="" />
                            </dt>
                            <dd>
                                <p className="cu">{item.headline}</p>
                                <p className="red">{item.money}元/平</p>
                                <p>{item.address}</p>
                                <p>{item.advantage} {item.advantage}  {item.advantage}</p>
                            </dd>
                            <dd className="dd2">
                                <p>住宅</p>
                                <i className="iconfont icon-aixin"></i>
                            </dd>
                        </dl>)
                    }
                </div>
        </div>
    )
}

export default Home