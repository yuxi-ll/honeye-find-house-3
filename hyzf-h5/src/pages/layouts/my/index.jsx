import React from 'react'
import './common.css'
import './index.css'
import {Modal} from 'zarm'
const My = (props) => {
    console.log(props)
    const handleBack=()=>{
        // window.localStorage.clear('token')
        // props.history.push('/login')
        Modal.confirm({
            title: '',
            content: '确定要退出吗？',
            onCancel: () => {
              return new Promise((resolve, reject) => {
                  
                resolve();
                // setTimeout(Math.random() > 0.5 ? resolve : reject, 500);
              }).catch(() => {
                window.alert('出错啦，弹窗无法关闭，继续点击试试');
                return false; // 返回false，可使弹窗无法关闭
              });
            },
            onOk: () => {
                console.log('点击ok');
                window.localStorage.clear('token');
                props.history.push('/login')
            },
          });
    }
    const handlePushPaymentDetails=()=>{
        props.history.push('/paymentDetails')
    }
    return (
        <div className='my flex-col'>
            <div className="bd3 flex-col">
                <div className="main1 flex-row">
                    <span className="word4">我的</span>
                </div>
            </div>
            <div className="bd4 flex-col">
                <div className="group1 flex-col">
                    <div className="layer1 flex-row">
                    <img
                        className="img1"
                        src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng0ba4af9643e4c186c9d983077a24be855eb2c488ec3890844dd4c652032c1567'
                        }
                        alt=''
                    />
                    <div className="section14 flex-col">
                        <span className="word5">梦想¥#</span>
                        <span className="txt7">15364777837</span>
                    </div>
                    <div className="section15 flex-col" />
                    </div>
                    <div className="layer2 flex-col" />
                    <div className="layer3 flex-row">
                    <img
                        className="icon6"
                        src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngf7bfbd72c01dbf4c5fe7045c87de2373b9d2881bbebd41e956a5deb0204454ad'
                        }
                        alt=''
                    />
                    <img
                        className="icon7"
                        src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngf6d50a59e47ff44e0665230e39d8f5b3f90a02b0792bba849b358c8537eaaca7'
                        }
                        alt=''
                    />
                    <img
                        className="label6"
                        src={
                        'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngaaa3a9819e092e78c69230be380131850e9c0de37c386994be60f08bd619824b'
                        }
                        alt=''
                    />
                    </div>
                    <div className="layer4 flex-row">
                    <span className="txt8">我的发布</span>
                    <span className="txt9">收藏</span>
                    <span className="word6">浏览历史</span>
                    </div>
                </div>
                <div className="group2 flex-col" />
            </div>
            <div className="section2 flex-row">
                <span className="word1">支付明细</span>
                <img
                className="icon3"
                src={
                    'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngb53ec51ff12fadd7e0c28fb394861387d0e164ddf568c02c5796a802d41a67d7'
                }
                alt=''
                onClick={handlePushPaymentDetails}
                />
            </div>
            <div className="section3 flex-col" />
            <div className="section4 flex-row">
                <span className="txt2">问题反馈</span>
                <img
                className="label1"
                src={
                    'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngb53ec51ff12fadd7e0c28fb394861387d0e164ddf568c02c5796a802d41a67d7'
                }
                alt=''
                />
            </div>
            <div className="section3 flex-col" />
            <div className="section6 flex-row">
                <span className="word2">联系客服</span>
                <img
                className="label2"
                src={
                    'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngb53ec51ff12fadd7e0c28fb394861387d0e164ddf568c02c5796a802d41a67d7'
                }
                alt=''
                />
            </div>
            <div className="section3 flex-col" />
            <button
                className="section8 flex-col"
            >
                <span onClick={handleBack} className="txt3">退出登录</span>
            </button>
        </div>
    )
}

export default My
