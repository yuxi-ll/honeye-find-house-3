import React, { useEffect, useState } from 'react'
import './index.css'
import down from '../../../../assets/myImg/down.png'
import axios from 'axios'
const PaymentDetails = (props) => {
    const [paymentList,setPaymentList]=useState([]);
    const [isShow,setIsShow]=useState(false);
    const [tabIndex,setTabIndex]=useState(0);
    const [tabTitle,setTabTitle]=useState('2021年1月')
    useEffect(()=>{
        getPaymentList()
    },[])
    const getPaymentList=async ()=>{
        const payment=await axios.get('/api/bill')
        console.log(payment);
        setPaymentList(payment.data.bill)
    }
    const handleShow=()=>{
        setIsShow(!isShow)
    }
    const handleTab=(val,Item)=>{
        setTabIndex(val);
        setTabTitle(Item.dateTitle);
        setIsShow(false)
    }
    const handleBackMyPage=()=>{
        props.history.go(-1)
    }
    return (
        <div className='paymentDetails'>
            <div className="paymentHeader">
                <img
                className="label2"
                src={
                    'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng1180f4d786d64dbeb38b8c9061ea64271c9c6c95bf874dada053daff01a46b52'
                }
                alt=''
                onClick={handleBackMyPage}
                />
                <span>支付明细</span>
                <b></b>
            </div>
            <div className="paymentMain">
                <div className="dateFilter">
                    <span>{tabTitle}</span>
                    <img src={down} alt="" onClick={handleShow}/>
                </div>
                <ul className="visible" style={{display:isShow?'block':'none'}}>
                    {
                        paymentList.map((item,index)=>{
                            return <li key={item.id} onClick={()=>handleTab(index,item)} className={index===tabIndex?'active':''}>
                                {item.dateTitle}
                            </li>
                        })
                    }
                </ul>
                <p>
                    <span>支出 ￥1817.61</span>
                </p>
                <div className='bill-list'>
                    {
                        paymentList.length?paymentList[tabIndex].children.map(item=>{
                            return <dl key={item.id}>
                                <dt>
                                    <img src={item.img} alt="" />
                                </dt>
                                <dd>
                                    <span className='msg'>{item.msg}</span>
                                    <span className='money'>
                                        <b>{item.money}</b>
                                    </span>
                                </dd>
                            </dl>
                        }):''
                    }
                </div>
            </div>
        </div>
    )
}

export default PaymentDetails
