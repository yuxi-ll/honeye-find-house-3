import React, { useState } from 'react';
import { useHistory } from "react-router-dom"
import "../findRoom/index.css"
import {
    EditTwoTone,
    VerifiedOutlined,
    HddTwoTone,
    CompassTwoTone,
    ReconciliationTwoTone,
    LayoutTwoTone
} from '@ant-design/icons';
//import Item from 'antd-mobile/lib/popover/Item';

const FindRoom = () => {
    const history = useHistory()
    const [value, setvalue] = useState('')
    const [flag, setflag] = useState(Boolean)
    const handleuser = (e) => {
        console.log(e.target.value)
        setvalue(e.target.value)
        if (value != '') {
            setflag(true)
        } else {
            setflag(false)
        }
    }

    return (
        <div className="findrom">
            <div className="fanhui">
                找房
            </div>
            <div className="bwzf">
                <h3>帮我找房</h3>
                <div className="search1">
                    <EditTwoTone twoToneColor="turquoise" />
                    <input type="text" placeholder="请输入意向城市，区域，意向楼盘" />
                </div>
                <div className="search2">
                    <input type="text" placeholder="请输入姓名" className="kuang" onChange={handleuser} value={value} />
                    <input type="text" placeholder="请输入手机号" />
                </div>
                <p className={flag ? 'start' : 'secret'}><VerifiedOutlined />隐私保护已开启</p>
                <div className="demand" onClick={() => history.push('/layout/release')}>
                    发布需求
                </div>
                <div className="business">
                    当前有148位置业顾问为您服务
                </div>

            </div>
            <div className="plate">
                <div className="plate1" onClick={() => history.push('/buyHouse')}>
                    <div className="left">
                        <h2>新房</h2>
                        <a >热门新楼盘...</a>
                    </div>
                    <div className="right">
                        <HddTwoTone twoToneColor="#fa7276" />
                    </div>
                </div>
                <div className="plate2" onClick={() => history.push('/mapRoom')}>
                    <div className="left">
                        <h2 >地图找房</h2>
                        <a  >查看好房源...</a>
                    </div>
                    <div className="right">
                        <CompassTwoTone twoToneColor="#6cccf8" />
                    </div>
                </div>

            </div>
            <div className="plates" onClick={() => history.push('/broker')}>
                <div className="plate3">
                    <div className="left">
                        <h2>经纪人</h2>
                        <a >优先置业顾问...</a>
                    </div>
                    <div className="right">
                        <ReconciliationTwoTone twoToneColor="#25cdac" />

                    </div>
                </div>
                <div className="plate4" onClick={() => history.push('/housingRing')}>
                    <div className="left">
                        <h2>楼市圈</h2>
                        <a >楼市最新动态...</a>
                    </div>
                    <div className="right">
                        <LayoutTwoTone twoToneColor="#f5d05d" />

                    </div>
                </div>

            </div>
        </div>
    )
}

export default FindRoom
