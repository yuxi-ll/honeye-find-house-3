import React from 'react'
import '../../font/iconfont.css'
import './index.css'
import {NavLink} from 'react-router-dom'
const index = (props) => {
    const {routes}=props
    return (
        <div className='layouts'>
           <main>
               {
                   props.children
               }
           </main>
           <footer>
               {
                   routes.map((item,index)=>{
                    return <NavLink key={index} to={item.path}>
                      <i className={item.icon}></i>
                      <span>{item.title}</span>
                    </NavLink>
                  })
               }
           </footer> 
        </div>
    )
}

export default index
