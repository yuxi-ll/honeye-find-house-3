import React from 'react'
import './index.css'
import {useHistory} from 'react-router-dom'

const Index = () => {
    
    const history = useHistory();
    //有房出售
    const pushSellHouse=()=>{
        history.push('/sellHouse')
    }
    //有房出租
    const puhRentOut=()=>{
        history.push("/rentOut")
    }
    //我要买个房
    const pushBuyHouse=()=>{
        history.push("/buymyHouses")
    }
    //我要租个房
    const pushrentHouse=()=>{
        history.push("/rentHouse")
    }
    //顾问
    const pushCounselor=()=>{
        history.push("/broker")
    }
    return (
        <div className="releasediv">
            <header>
                发布
            </header>
            <div className="body">
                <ul>
                    <li onClick={pushSellHouse}>
                        <div className="left"></div>
                        <div className="center">
                            <p>有房出售</p>
                            <span>发布二手房出售信息</span>
                        </div>
                        <div className="right">&gt;</div>
                    </li>
                    <li onClick={puhRentOut}>
                        <div className="left"></div>
                        <div className="center">
                            <p>有房出租</p>
                            <span>发布房源出租信息</span>
                        </div>
                        <div className="right">&gt;</div>
                    </li>
                    <li onClick={pushBuyHouse}>
                        <div className="left"></div>
                        <div className="center">
                            <p>我想买个房子</p>
                            <span>发布求购意向信息</span>
                        </div>
                        <div className="right">&gt;</div>
                    </li>
                    <li onClick={pushrentHouse}>
                        <div className="left"></div>
                        <div className="center">
                            <p>我想租个房子</p>
                            <span>发布求租意向信息</span>
                        </div>
                        <div className="right">&gt;</div>
                    </li>
                    <li onClick={pushCounselor}>
                        <div className="left"></div>
                        <div className="center">
                            <p>买新房 职业顾问帮您忙</p>
                            <span>提交购房意向买房顾问全程带着</span>
                        </div>
                        <div className="right">&gt;</div>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default Index
