import React from 'react'
import { useHistory } from "react-router-dom"
import { useState, useEffect } from "react"
import http from "../../utils/request"
import style from "./index.module.css"
import { Toast, WhiteSpace, WingBlank, Button } from 'antd-mobile';
import { Modal } from 'antd';
import {

    LeftOutlined,
    HeartOutlined,
    ImportOutlined

} from '@ant-design/icons';



const BrokerDetail = () => {
    const history = useHistory()
    const [data, setdata] = useState([])
    const [id, setid] = useState(history.location.query.id)
    const [flag, setflag] = useState(Boolean)
    // 爱心
    const [xinflag, setxinflag] = useState(Boolean)
    const [activeIndex, setactiveIndex] = useState([])
    // 房屋列表
    const [housdata, sethousdata] = useState([])
    useEffect(() => {
        getlist();
        getlisthouse()
    }, [])
    const getlist = () => {
        http.get('http://localhost:3000/api/broker').then(res => {
            console.log(res.data.list)
            res.data.list.map(item => {
                if (item.id === id) {
                    setdata({ ...item })
                }
            })

        })
    }

    // 卖房
    const handlesell = () => {
        setflag(true)
        console.log(flag)
        getlisthouse();
    }
    // 租房
    const handlerent = () => {
        setflag(false)
        console.log(flag)
        getlisthouse();

    }

    const getlisthouse = () => {
        if (flag === true) {
            http.get('/houses').then(res => {
                console.log(res.data.body.list)
                sethousdata([...res.data.body.list])
                Toast.success('获取数据成功')

            })
        } else {
            http.get('http://localhost:3000/api/brokerDetail').then(res => {
                console.log(res.data.body)
                sethousdata([...res.data.list])
                Toast.success('获取数据成功')
            })
        }


    }
    // 电话咨询
    const handleCall = () => {
        history.push('/call')
    }
    // 点击收藏
    const handleCollect = (item, index) => {
        if (housdata[index].falg) {
            housdata[index].falg = false
            Toast.fail('取消收藏')
        } else {
            housdata[index].falg = true;
            Toast.success('收藏成功')
        }

        sethousdata([...housdata])
        console.log(item.houseCode)

    }

    // 分享弹框
    const [isModalVisible, setIsModalVisible] = useState(false);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };


    // console.log(data)
    console.log(housdata)

    return (
        <div className={style.brokerDetail}>
            {/*  */}
            <div className={style.header}>
                <div className={style.fanhui}>
                    <LeftOutlined onClick={() => history.go(-1)} />
                    <span>详情</span>
                    <span></span>
                </div>
                <div className={style.bag}>

                </div>
                <div className={style.card}>
                    <div className={style.box7}>
                        <div className={style.outer9}>
                            <span className={style.word22}>易居房产</span>
                            <span className={style.word23}>{data.name}</span>
                            <span className={style.word24}>电话：{data.phone}</span>
                            <span className={style.word25}>地址：{data.area}</span>
                        </div>
                    </div>
                </div>
                {/* tab切换 */}
                <div className={style.section2}>
                    <span className={style.info2} onClick={() => handlesell()}>卖房</span>
                    <span className={style.info2} onClick={() => handlerent()}>租房</span>
                </div>
                <div className={style.section3} />
            </div>
            {/*  */}
            <div className={style.main}>
                {
                    housdata.map((item, index) =>
                        <div className={style.section4} key={index}>
                            <div className={style.outer1}>
                                <div className={style.layer1}>
                                    <img
                                        className={style.img1}
                                        src={
                                            item.houseImg
                                        }
                                    />
                                </div>
                            </div>
                            <div className={style.outer2}>
                                <span className={style.txt2}>{item.desc}</span>
                                <span className={style.txt3}>121100元/平</span>
                                <span className={style.info3}>{item.title}</span>
                                <div className={style.box2}>
                                    <div className={style.block1}>
                                        <span className={style.info4}>低单价</span>
                                    </div>
                                    <div className={style.block2}>
                                        <span className={style.word1}>近地铁</span>
                                    </div>
                                </div>
                            </div>
                            <div className={style.outer3}>
                                < span className={style.word2}>环线房</span>
                            </div >
                            <div className={style.outer4}>
                                < div className={style.block3} />
                                < span className={
                                    style.txt4}>住宅</span>


                                <HeartOutlined className={item.falg ? style.active : style.activeA} onClick={() => { handleCollect(item, index) }} />
                            </div>
                        </div >

                    )
                }
                {/* 分享弹框 */}
                <Modal title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                    <p>Some contents...</p>
                    <p>Some contents...</p>
                    <p>Some contents...</p>
                </Modal>

            </div >
            {/*  */}
            < div className={style.footer} >

                <div className={style.outer8}>
                    <div className={style.group6} onClick={showModal}>
                        <ImportOutlined className={style.share} />
                        <span className={style.word17}>分享</span>
                    </div>
                    <div className={style.group7}>
                        <span className={style.word18}>在线咨询</span>
                    </div>
                    <div className={style.group8} onClick={() => handleCall()}>
                        <span className={style.word19}>电话咨询</span>

                    </div>
                </div>
            </div >
        </div >
    )
}

export default BrokerDetail