import React from 'react'
import axios from "../../utils/request"
import ajax from "axios"
import { Cascader } from 'antd';
import "../../mock/yjr/index"
import { NavBar, Icon } from 'antd-mobile';
import { useState, useEffect, useRef, useReducer } from "react"
import "../buyHouse/index.css"
import { Popup, Cell, Button, Picker, Toast } from 'zarm';
import img1 from "../../assets/homeImg/s.png"
import img2 from "../../assets/homeImg/z.png"
import img3 from "../../assets/homeImg/x.png"
import img4 from "../../assets/homeImg/d.png"
import "../../index.js"
const BuyHouse = (props) => {
    const [city, setCity] = useState([])
    const [list, setList] = useState([])
    const [vals, setVals] = useState('')//搜索
    //const [sort, setSort] = useState([])
    useEffect(() => {
        getCityList()
        getHomeList()
    }, [])
    const getHomeList = async () => {
        const list = await ajax.get('/api/list')
        console.log(list)
        setList(set => [...list.data.list])
    }
    //弹出框
    const SINGLE_DATA = [
        { value: '1', label: '选项一' },
        { value: '2', label: '选项二' },
    ];

    const initVisibleState = {
        popBottom: false,
        popTop: false,
        popLeft: false,
        popRight: false,
        picker: false,
        popSpec: false,
        popCenterSpec: false,
    };
    const popupRef = useRef();
    const [value, setValue] = useState('');
    const [visible, setVisible] = useReducer((state, action) => {
        const { type } = action;
        return {
            ...state,
            [type]: !state[type],
        };
    }, initVisibleState);

    const toggle = (type, e) => {
        setVisible({ type })
    }
    //降序
    const reduceSort = () => {
        // console.log(e.target)
       list.sort((a, b) => {
            return b - a
        })
        setList(res=>[...list])
    }
    //升序
    const addSort = (e) => {
        list.sort((a, b) => {
            return a - b
        });
        setList(res=>[...list])
    }

    const getCityList = async () => {
        const city = await axios.get("/area/city?level=1")
        setCity(set => [...city.data.body])
        //console.log(list)
    }
    //跳转地图找房页面
    const handleToMap = async () => {
        props.history.push("/mapRoom/index")
    }
    //模糊搜素
    const changeVals = (e) => {
        setVals(e.target.value, vals)
    }
    //搜索
    const Search = () => {
        const res = list.filter(item => item.title.includes(vals))
        setList(res)
        console.log(res)
    }
    //跳转首页
    const handleToPage = () => {
        props.history.push("/layout/home")
    }
    //跳转首页
    const handleTo = () => {
        props.history.push("/layout/home")
    }
    //跳转到区域的页面
    const handleToArea = () => {
        props.history.push('/areamap')
    }
    //跳转到价格页面
    const handleToPrice = () => {
        props.history.push("/price")
    }
    //跳转到户型页面
    const handleToHouse = () => {
        props.history.push("/house")
    }
    
    

    function onChange(value) {
        console.log(value);
    }
    return (
        <div className="house">
            <div className="header">
                <NavBar
                    mode="light"
                    icon={<Icon type="left" />}
                    onLeftClick={() => console.log('onLeftClick')}
                    rightContent={[
                        <Icon key="0" type="search" style={{ marginRight: '16px' }} />,
                        <Icon key="1" type="ellipsis" />,

                    ]}
                    onClick={() => { handleTo() }}
                >买房</NavBar>
            </div>
            <div className="select">
                <div className="position" style={{ width: 100, bordered: false }} defaultValue={['北京']}>
                    <Cascader options={city} onChange={onChange} />
                </div>
                <div className="search">
                    <input type="text" value={vals} onChange={changeVals} placeholder="请输入与搜索区域/商圈" style={{ height: 30 }} />

                </div>
                <div className="btn" >
                    <button onClick={Search}>搜索</button>
                </div>
            </div>
            <div className="operate">
                <dl>
                    <dt><img src={img1} alt="" /></dt>
                    <dd>商铺</dd>
                </dl>
                <dl>
                    <dt><img src={img2} alt="" /></dt>
                    <dd>写字楼</dd>
                </dl>
                <dl>
                    <dt><img src={img3} alt="" /></dt>
                    <dd>小区</dd>
                </dl>
                <dl>
                    <dt><img src={img4} alt="" /></dt>
                    <dd onClick={() => { handleToMap() }}>地图找房</dd>
                </dl>
            </div>
            <div className="list">
                <span onClick={() => { handleToArea() }}>区域</span>
                <span onClick={() => { handleToPrice() }}>价格</span>
                <span onClick={() => { handleToHouse() }}>户型</span>
                <span>
                    筛选
                </span>
                <span> <Cell
                    description={
                        <Button size="xs" onClick={(e) => {
                            toggle('popRight')
                            console.log(e)
                        }}>
                            排序
                        </Button>
                    }
                >
                </Cell></span>
            </div>
            <div className="cell">
                <Popup
                    visible={visible.popBottom}
                    direction="bottom"
                    onMaskClick={() => toggle('popBottom')}
                    afterOpen={() => console.log('打开')}
                    afterClose={() => console.log('关闭')}
                    destroy={false}
                    mountContainer={() => document.body}
                >
                    <div className="popup-box">
                        <Button size="xs" onClick={() => toggle('picker')}>
                            打开Picker
                    </Button>
                    </div>
                </Popup>
                <Popup
                    visible={visible.popRight}
                    onMaskClick={() => toggle('popRight')}
                    direction="right"
                    afterClose={() => console.log('关闭')}
                >
                    <div className="popup-box-right">
                        <Button size="xs" onClick={() => { addSort() }}>
                            升序
                        </Button>
                        <Button size="xs" onClick={() => { reduceSort() }}>
                            降序
                        </Button>
                        <Button size="xs" onClick={() => toggle('popRight')}>
                            关闭
                        </Button>
                    </div>
                </Popup>

                <Picker
                    visible={visible.picker}
                    value={value}
                    dataSource={SINGLE_DATA}
                    onOk={(selected) => {
                        console.log('Picker onOk: ', selected);
                        Toast.show(JSON.stringify(selected));
                        setValue(selected.map((item) => item.value));
                        toggle('picker');
                    }}
                    onCancel={() => toggle('picker')}
                />
            </div>
            <div className="homeList">
                {
                    list.map((item, index) => <dl key={index}>
                        <div className="image">
                            <dt><img src={item.img} alt="" /></dt>
                        </div>
                        <div className="content">
                            <dd>
                                <span><b>{item.title}</b></span>
                                <span>住宅</span>
                            </dd>
                            <dd>
                                <span><b>{item.price}/平</b></span>
                                <b style={{ marginRight: 3 }}>收藏</b>
                            </dd>
                            <dd>{item.title}</dd>
                            <dd>
                                <span>{item.total}</span><span>{item.total}</span><span>{item.total}</span>
                            </dd>
                        </div>
                    </dl>)
                }
            </div>
            <div className="page">
                <span onClick={() => { handleToPage() }}>首页</span>
            </div>
        </div>
    )
}

export default BuyHouse