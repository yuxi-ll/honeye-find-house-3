import React from 'react'
import style from "../groupPurchase/index.module.css"
import { useHistory } from "react-router-dom"
import { useState, useEffect } from "react"
import http from "../../utils/request"
import { Button, WhiteSpace, WingBlank } from 'antd-mobile';

import {

    LeftOutlined,

} from '@ant-design/icons';
const GroupPurchase = () => {

    const [data, setdata] = useState([])

    useEffect(() => {
        getlist()
    }, [])
    const getlist = () => {
        http.get('http://localhost:3000/api/groupPurchase').then(res => {
            console.log(res.data.list)
            setdata([...res.data.list])
        })
    }

    const history = useHistory()
    return (
        <div className={style.groupPurchase}>

            {/*  */}
            <div className={style.header}>
                <div className={style.fanhui}>
                    <LeftOutlined onClick={() => history.go(-1)} />
                    <span>团购看房</span>
                    <span></span>
                </div>
            </div>
            {/*  */}
            <div className={style.main}>
                {
                    data.map((item, index) => <dl key={index}>
                        <dt>
                            <img src={item.img} alt="" />
                        </dt>
                        <dd>
                            <h2>{item.house}</h2>
                            <p>{item.area}</p>
                            <p>活动时间：2021.4.17-2021.5.17<span>已报名:21人</span></p>

                        </dd>
                    </dl>)
                }
                <Button type="warning" inline size="small" style={{ marginRight: '4px' }} className={style.want} >要报名</Button><WhiteSpace />
                <Button type="warning" inline size="small" style={{ marginRight: '4px' }} className={style.ing}>正在抢</Button><WhiteSpace />
                <Button type="primary" inline size="small" style={{ marginRight: '4px' }} className={style.already}>已开团</Button>
            </div>

        </div>
    )
}

export default GroupPurchase
