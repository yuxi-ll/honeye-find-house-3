import loadable from '@loadable/component';
const layouts = loadable(() => import('../pages/layouts/index'))//首页布局容器
const login = loadable(() => import('../pages/login/index'))//登录
const Register = loadable(() => import('../pages/register/index'))//注册
const home = loadable(() => import('../pages/layouts/home/index'))//首页
const findRoom = loadable(() => import('../pages/layouts/findRoom/index'))//找房
const release = loadable(() => import('../pages/layouts/release/index'))//发布
const news = loadable(() => import('../pages/layouts/news/index'))//消息
const my = loadable(() => import('../pages/layouts/my/index'))//我的
const buyHouse = loadable(() => import('../pages/buyHouse/index'))//买房
const rentingHouse = loadable(() => import('../pages/rentingHouse/index'))//租房
const wanted = loadable(() => import('../pages/wanted/index'))//求租
const purchase = loadable(() => import('../pages/purchase/index'))//求购
const mapRoom = loadable(() => import('../pages/mapRoom/index'))//地图找房
const information = loadable(() => import('../pages/information/index'))//资讯
const housingRing = loadable(() => import('../pages/housingRing/index'))//楼市圈
const broker = loadable(() => import('../pages/broker/index'))//经纪人
const groupPurchase = loadable(() => import('../pages/groupPurchase/index'))//团购看房
const liveSeeRoom = loadable(() => import('../pages/liveSeeRoom/index'))//直播看房
const sellHouse = loadable(() => import('../pages/releasepage/sellHouse'))//有房出售
const rentOut = loadable(() => import('../pages/releasepage/rentOut'))//有房出租
const buymyHouses = loadable(() => import('../pages/releasepage/buyMyHouse'))//我要买个房
const rentHouse = loadable(() => import('../pages/releasepage/rentHouse'))//我要租个房
const payEnd = loadable(() => import('../pages/releasepage/payEnd'))//发布成功
const myIssue=loadable(()=>import("../pages/releasepage/myIssue"))//我的发布列表
const sellChoosePlot = loadable(() => import('../pages/releasepage/choosePlot'))//选择小区
const Areamap = loadable(() => import('../pages/mapRoom/area'))//地图找房区域筛选
const Price = loadable(() => import('../pages/mapRoom/price'))//地图找房价格筛选
const House = loadable(() => import('../pages/mapRoom/house'))//地图找房房型筛选
const Methods = loadable(() => import('../pages/mapRoom/methods'))//地图找房方式筛选
const HouseDetail = loadable(() => import('../pages/mapRoom/houseDetail'))//房屋详情列表
const Vr = loadable(() => import('../pages/mapRoom/vrhouse'))
const Chat = loadable(() => import('../pages/wanted/chat'))//求租聊天页面
const Call = loadable(() => import("../pages/wanted/call"))//求租打电话页面
const brokerDetail = loadable(() => import("../pages/brokerDetail/index"))
const sellUploadImg = loadable(() => import('../pages/releasepage/uploadImg'))//发布卖房上传照片
const infoDetail=loadable(()=>import('../pages/information/infoDetail/index'))//资讯详情
const paymentDetails=loadable(()=>import('../pages/layouts/my/paymentDetails/index'))//支付明细
const routes = [
    {
        path: '/layout',
        component: layouts,
        children: [
            {
                path: '/layout/home',
                title: '首页',
                icon: 'iconfont icon-76shouye',
                component: home
            },
            {
                path: '/layout/findRoom',
                title: '找房',
                icon: 'iconfont icon-xinfang',
                component: findRoom
            },
            {
                path: '/layout/release',
                title: '发布',
                icon: 'iconfont icon-ershoufang',
                component: release
            },
            {
                path: '/layout/news',
                title: '消息',
                icon: 'iconfont icon-zufang',
                component: news
            },
            {
                path:'/layout/my',
                title:'我的',
                icon:'iconfont icon-wode',
                component:my,
                isLogin:true
            },
        ]
    },
    {
        path: '/login',//登录
        component: login
    }, {
        path: '/sellHouse', //发布 二手房出售
        component: sellHouse
    }, {
        path: '/rentOut', //发布 有房出租
        component: rentOut
    }, {
        path: '/buymyHouses', //发布 我想买个房
        component: buymyHouses
    }, {
        path: '/rentHouse',  //发布 我想租个房
        component: rentHouse
    }, {
        path: '/payend', //发布成功
        component: payEnd
    }, {
        path: '/myissue', //发布成功myIssue
        component: myIssue
    }, {
        path: '/choosePlot', //发布 卖房 选择小区
        component: sellChoosePlot
    }, {
        path: '/uploadImg', //发布 卖房 上传照片
        component: sellUploadImg
    },
    {
        path: '/areamap',
        component: Areamap,//地图找房区域筛选
    },
    {
        path: '/house',
        component: House,//地图找房房型筛选
    },
    {
        path: '/price',
        component: Price,//地图找房价格筛选
    },
    {
        path: '/methods',
        component: Methods,//地图找房方式筛选
    },
    {
        path: '/vr',
        component: Vr,//vr
    },
    {
        path: '/HouseDetail',
        component: HouseDetail,//房屋详情
    },
    {
        path: '/register',//注册
        component: Register
    },
    {
        path: '/buyHouse',//买房
        component: buyHouse
    },
    {
        path: '/rentingHouse',//租房
        component: rentingHouse
    },
    {
        path: '/wanted',//求租
        component: wanted,
    },
    {
        path: '/chat',//求租聊天
        component: Chat,
    },
    {
        path: '/call',//求租聊天
        component: Call,
    },
    {
        path: '/purchase',//求购
        component: purchase
    },
    {
        path: '/mapRoom',//地图找房
        component: mapRoom
    },
    {
        path: '/information',//资讯
        component: information
    },
    {
        path: '/housingRing',//楼市圈
        component: housingRing
    },
    {
        path: '/broker',//经纪人
        component: broker
    },
    {
        path: '/brokerDetail',//经纪人详情
        component: brokerDetail
    },
    {
        path: '/groupPurchase',//团购看房
        component: groupPurchase
    },
    {
        path: '/liveSeeRoom',//直播看房
        component: liveSeeRoom
    },
    {
        path:'/infoDetail',//咨询详情
        component:infoDetail
    },
    {
        path:'/paymentDetails',//支付明细
        component:paymentDetails
    },
    {
        from:'/',
        to:'/login'
    }
]

export default routes