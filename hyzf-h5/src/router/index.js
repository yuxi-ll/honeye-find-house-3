import {Switch,Route,Redirect} from 'react-router-dom';

const beforeEach=(item)=>{
    if(window.localStorage.getItem('token'))
    {
        return <Route path={item.path} component={item.component}></Route>
    }
    return <Redirect to='/login'></Redirect>
}
const RouteView=(props)=>{
    const {routes}=props;
    return <Switch>
        {
            routes.map((item,index)=>item.path?
            <Route key={index} 
            path={item.path} 
            render={(History)=>item.isLogin?beforeEach(item):<item.component routes={item.children?item.children:[]} {...History}>
                <RouteView routes={item.children?item.children:''}></RouteView>
            </item.component>}></Route>
            :<Redirect {...item} key={index}></Redirect>)
        }
    </Switch>
}

export default RouteView