/*
 * @Author: your name
 * @Date: 2021-06-03 13:43:41
 * @LastEditTime: 2021-06-03 13:57:52
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\hyzf-jingjiren\src\App.js
 */
import RouteView from './router/index';
import routes from './router/config'
function App() {
  return (
    <div className="App">
      <RouteView routes={routes}></RouteView>
    </div>
  );
}

export default App;
