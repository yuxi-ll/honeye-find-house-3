import React, { useEffect, useState, useReducer } from 'react'
import axios from '../../../utils/request';
import {  Picker, Toast } from 'zarm';
import './index.css'
import './common.css'
const reducer = (state, action) => {
  const { type, value } = action;
  switch (type) {
    case 'visible':
      return {
        ...state,
        visible: !state.visible,
      };
    case 'value':
      return {
        ...state,
        value
      };
    default:
      return {
        ...state
      }
  }
};
const Home = (props) => {
  useEffect(() =>{
    getCityList()
  }, [])
  const getCityList = async () => {
    const cityList = await axios.get('/area/city?level=1');
    setSINGLE_DATA(cityList.data.body.filter(v => v.label && v.value))
  }
  const [SINGLE_DATA, setSINGLE_DATA] = useState([]);
  const initState = {
    visible: false,
    value: '上海',
    //dataSource: SINGLE_DATA,
  };
  const [state, dispatch] = useReducer(reducer, initState);
  const setVisible = (key) => {
    dispatch({ type: 'visible', key });
  };

  const setValue = (value) => {
    dispatch({ type: 'value', value });
  };
  return (
    <div className='home flex-col'>
      <Picker
        visible={state.visible}
        value={state.value}
        dataSource={SINGLE_DATA}
        onOk={(selected) => {
          Toast.show('切换成功');
          setValue(
            selected.map((item) => item.label),
          );
          setVisible(false);
        }}
        onCancel={() => setVisible(false)}
      />
      <div className="wrap8 flex-col">
        <div className="block11 flex-col">
          <div className="bd6 flex-row">
            <span className="info26">{state.value}</span>
            <img
              className="icon12"
              src={
                'https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPngb760d2a5a486f99adc0929f9b34f4e7d1c6ee8c0496e3368deaa16ba2472be81'
              }
              alt=''
              onClick={() => setVisible(true)}
            />
            <div className="group17 flex-col">
              <span className="txt22">请输入查询的房源</span>
            </div>
          </div>
        </div>
      </div>
      <div className='navlist' style={{display: 'flex', flexWrap: 'wrap' }}>
        <span onClick={() => props.history.push('/buyHouse')}>新房</span>
        <span onClick={() => props.history.push('/rentingHouse')}>出租房</span>
        <span onClick={() => props.history.push('/second')}>二手房</span>
        <span onClick={() => props.history.push('/message')}>消息</span>
        <span onClick={() => props.history.push('/wanted')}>求租</span>
        <span onClick={() => props.history.push('/purchase')}>求购</span>
        <span onClick={() => props.history.push('/housingRing')}>楼市圈</span>
        <span onClick={() => props.history.push('/liveSeeRoom')}>直播</span>
        <span onClick={() => props.history.push('/groupPurchase')}>团购</span>
        <span onClick={() => props.history.push('/broker')}>排行榜</span>
        <span onClick={() => props.history.push('/zhan')}>战绩</span>
      </div>
    </div>
  )
}

export default Home