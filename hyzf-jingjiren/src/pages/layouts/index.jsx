import React from 'react'
import '../../font/iconfont.css'
import './index.css'
const index = (props) => {
    return (
        <div className='layouts'>
           <main>
               {
                   props.children
               }
           </main>
        </div>
    )
}

export default index
