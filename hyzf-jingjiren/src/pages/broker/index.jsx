import React from 'react'
import http from "../../utils/request"
import {

    LeftOutlined,
    SearchOutlined,
    MessageTwoTone,
    PhoneTwoTone
} from '@ant-design/icons';
import { useHistory } from "react-router-dom"
import { useState, useEffect } from "react"
import style from "./index.module.css"
const Broker = () => {
    const history = useHistory()
    const [data, setdata] = useState([])
    let [datas, setdatas] = useState([])

    useEffect(() => {
        getlist()
    }, [])
    // 渲染列表
    const getlist = () => {
        http.get('http://localhost:3000/api/broker').then(res => {
            setdata([...res.data.list])
            setdatas([...res.data.list])
        })
    }
    // 点击详情
    const handleDetail = (item) => {
        history.push({ pathname: '/brokerDetail', query: { id: item.id } })
    }
    // 模糊搜索
    const handleinput = (e) => {
        console.log(e.target.value)

        datas = datas.filter(item => item.name.includes(e.target.value)
        )
        console.log(datas)
        setdata([...datas])



    }

    return (
        <div className={style.borker}>
            {/* 头部 */}
            <div className={style.header}>
                <div className={style.fanhui}>
                    <LeftOutlined onClick={() => history.go(-1)} />
                    <span>经纪人</span>
                    <span></span>
                </div>
                <div className={style.search}>
                    <SearchOutlined />
                    <input type="text" placeholder="请输入需要查询的经纪人" onInput={handleinput} />
                </div>
                <div className={style.one}>
                    1
                </div>
                <img src="https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=816133258,3098083250&fm=11&gp=0.jpg" alt="" className={style.one1} />
                <div className={style.layer14}><span className={style.info7}>客行</span></div>
                <div className={style.layer15} />
                < div className={style.two}>
                    2
                </div>
                <img src="https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=962163636,3069389995&fm=11&gp=0.jpg" alt="" className={style.two2} />
                <div className={style.three}>
                    3
                </div>
                <img src="https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fitem%2F201906%2F27%2F20190627194435_skfyy.thumb.700_0.jpg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1624850329&t=c34a5701ece2d2a0a50b3dd2e0f2f7f9" alt="" className={style.three3} />
                <div className={style.layer11} />
                <div className={style.layer13} />
                <div className={style.layer16} />
                <div className={style.white}>
                    <span className={style.word2}>周絮</span>
                    <span className={style.word3}>晚宁</span>
                </div>
                <div className={style.san}>
                    <div className={style.san1}>

                    </div>
                    <div className={style.san2}>

                    </div>
                    <div className={style.san3}>

                    </div>
                    <div className={style.san4}>

                    </div>
                </div>
            </div>
            {/* 主体 */}
            <div className={style.main}>
                {
                    data.map((item, index) => <li key={index} onClick={() => handleDetail(item)}>
                        <h2>{item.id}</h2>
                        <img src={item.img} alt="" />
                        <div className={style.cname}>
                            <span><b>{item.name}</b></span>
                            <span>丫丫找房</span>
                        </div>
                        <div className={style.cic}>
                            <MessageTwoTone twoToneColor="rgba(253, 102, 106, 1)" />
                            <PhoneTwoTone twoToneColor="rgba(253, 102, 106, 1)" />
                        </div>
                    </li>)
                }
            </div>

        </div>
    )
}

export default Broker
