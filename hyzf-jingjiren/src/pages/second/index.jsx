import React from 'react'
import style from './second.module.css'
const Second = (props) => {
    return <div className={style.second}>
        <div className={style.househeader}>
            <span onClick={() => props.history.push('/layout/home')}>&lt;</span>
            <span>二手房</span>
            <span>···</span>
        </div>
        <div className={style.mapmain}>
                <div className={style.search}>
                    <span>上海<i className={style.trangle}></i></span>
                    <div className={style.searchli}><i><img src="https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng453dc40899615ef956fb1319425f8b80ff7071d4627831942e4ce1ede2f8c2ba" alt="" /></i><span>请输入搜索区域/商圈</span></div>
                    <span className={style.searchBtn}>搜索</span>
                </div>
                <div className={style.select}>
                    <div className={style.selectnav}>
                        <span onClick={() => props.history.push('/areamap')}>区域<i className={style.trangle}></i></span>
                        <span onClick={() => props.history.push('/price') }>价格<i className={style.trangle}></i></span>
                        <span onClick={() => props.history.push('/house') }>户型<i className={style.trangle}></i></span>
                        <span onClick={() => props.history.push('/methods') }>距离<i className={style.trangle}></i></span>
                    </div>
                </div>
            </div>
    </div>
}

export default Second