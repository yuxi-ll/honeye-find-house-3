import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import axios from '../../utils/request'
import style from './houseRing.module.css'
const HousingRing = () => {
    const [list, setList] = useState([])
    const history = useHistory()
    useEffect(() =>{
        getAgent()
    }, [])
    const getAgent = async () => {
        const { data: res } = await axios.get('http://localhost:3000/api/broker')
        console.log(res)
        setList([...res.list])
    }
    return (
        <div className={style.housingRing}>
            <div className={style.househeader}>
                <span onClick={() => history.push('/layout/home')}>&lt;</span>
                <span>楼盘圈</span>
                <span>···</span>
            </div>
            <div className={style.housemain}>
                {
                    list.map(item => <div key={item.id} className={style.item}>
                        <div className={style.head}>
                            <img src={item.img} alt="" />
                            <span>{item.name}</span>
                        </div>
                        <div className={style.chat}>
                            <span onClick={() => history.push({pathname:'/chat', state: {name: item.name}})}>在线咨询</span>
                            <span onClick={() => history.push('/call')}>拨打电话</span>
                        </div>
                    </div>)
                }
            </div>
        </div>
    )
}

export default HousingRing
