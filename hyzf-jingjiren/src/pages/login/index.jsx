import React from 'react'
import './common.css';
import './index.css';
import axios from '../../utils/request'
import { useState } from 'react';
import {Toast,Icon} from 'zarm'
const Login = (props) => {
    //console.log(props,'sss')
    const [username,setUsername] = useState('');
    const [password,setPassword] = useState('');
    const handleLogin=async()=>{
       const loginInfo=await axios.post('/user/login',{
           username,password
       })
        //props.history.push('/layout/home')
        if(loginInfo.data.status!==200)
        {
            Toast.show(
                <div className="box">
                  <Icon type="wrong-round-fill" className="box-icon" style={{ color: '#fff' }} />
                  <div className="box-text">登录失败</div>
                </div>,
              );
        }
        else
        {
            window.localStorage.setItem('token',loginInfo.data.body.token);
            props.history.push('/layout/home')
        }
    }
    const handleUser=(e)=>{
        setUsername(username=>e.target.value)
    }
    const handlePwd=(e)=>{
        setPassword(password=>e.target.value)
    }
    return (
        <div className="page flex-col">
            <div className="box1 flex-col">
            <span className="info1">欢迎来到宏烨租房</span>
            <span className="info2">根植于上海的便民平台</span>
            <div className="group3 flex-col">
                <input className="word2" type="text" placeholder='请输入账号' value={username} onChange={(e)=>handleUser(e)}/>
            </div>
            <div className="group4 flex-col">
                <input className="txt1" type="password" placeholder='请输入密码' value={password} onChange={(e)=>handlePwd(e)}/>
            </div>
            <button
                className="group5 flex-col"
            >
                <span className="info3" onClick={handleLogin}>登录</span>
            </button>
            <div className="group6 flex-row">
                <span className="info4">忘记密码</span>
                <div className="box3 flex-col" />
                <span className="txt2">用户注册</span>
            </div>
            <span className="word3">微信快速登录</span>
            <img
                className="label4"
                src='https://lanhu.oss-cn-beijing.aliyuncs.com/SketchPng54540044451619b88d1445e5667a4b3a5283a9ff734697ffdf41c2836cdda539'
                alt=""
            />
            </div>
      </div>
    )
}

export default Login
