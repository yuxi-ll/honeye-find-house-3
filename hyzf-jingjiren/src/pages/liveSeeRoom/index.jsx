import React from 'react'
import style from './live.module.css'
const LiveSeeRoom = (props) => {
    return (
        <div className={style.live}>
            <div className={style.househeader}>
                <span onClick={() => props.history.push('/layout/home')}>&lt;</span>
                <span>直播</span>
                <span>···</span>
            </div>
            直播看房
        </div>
    )
}

export default LiveSeeRoom
