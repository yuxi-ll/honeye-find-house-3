import React from 'react'
import style from './rentHouse.module.css'
const RentingHouse = (props) => {
    return (
        <div className={style.rentHouse}>
            <div className={style.househeader}>
                <span onClick={() => props.history.push('/layout/home')}>&lt;</span>
                <span>出租房</span>
                <span>···</span>
            </div>
        </div>
    )
}

export default RentingHouse
