/*
 * @Author: your name
 * @Date: 2021-05-26 18:43:54
 * @LastEditTime: 2021-05-31 20:12:25
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\honeye-find-house-3\hyzf-h5\src\api\lyx-api\index.js
 */
import axios from '../../utils/request'
const CITY_KEY="hkzf_city";
 
const getCityl = ()=>JSON.parse(localStorage.getItem(CITY_KEY));

const setCity =(value)=>localStorage.setItem(CITY_KEY,JSON.stringify(value));
//获取当前城市定位
export const getCurrentCity = () => {
    //1
    const curCity = getCityl()
    //2
    if (!curCity) {
        //没有值的话
        //可以去调用报读api进行ip定位

        return new Promise(resolve => {
            const myCity = new window.BMap.LocalCity();
            myCity.get(async result => {
                const res = await axios.get("/area/info", {
                    params: {
                        name: result.name
                    }
                })
                //去解构出来  {label ,value};
                const {
                    label,
                    value
                } = res.data.body;

                //把定位的城市存到本地;
                setCity({
                    label,
                    value
                })
                resolve({
                    label,
                    value
                })
            })
        })
    } else {
        return Promise.resolve(curCity)
    }
}

//获取城市列表
export const getCity = () => {
    return axios.get('/area/city?level=1')
}
//获取市区列表
export const getArea = ({
    id
}) => {
    return axios.get(`/houses/condition?id=${id}`)
}
//获取房屋详情信息
export const getHouseDet = ({params}) => {
    return axios.get(`/houses/${params}`)
}

//获取房源列表
export const getHouse = ({cityId}) => {
    return axios.get('/houses', {
        params: {cityId}
    })
}

//添加至收藏列表
export const addFavorites = (id) => {
    return axios.get('/user/favorites', {
        id
    })
}

//删除收藏
export const delFavorites = (id) => {
    return axios.delete('/user/favorites', {
        id
    })
}

export const getSelectHou = ({params}) => {
    return axios.get('/houses', {params})
}