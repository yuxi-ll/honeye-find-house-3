/*
 * @Author: your name
 * @Date: 2021-01-21 11:36:25
 * @LastEditTime: 2021-05-31 08:43:40
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \maplist\src\components\HouseItem\index.js
 */
import React from 'react'
import styles from './index.module.css'
import { useHistory } from 'react-router-dom'
let url= 'http://157.122.54.189:9060'
const HouseItem = (props) => {
    const { houseImg, title, tags, price, desc, houseCode, cityId } = props
    const history = useHistory()
    return (
        <div className={styles.house} onClick={() => history.push({pathname:'/houseDetail', state: {houseCode, cityId}})}>
            <div className={styles.imgWrap}>
                <img src={url+houseImg} alt="" className={styles.img} />
            </div>
            <div className={styles.content}>
                <h3 className={styles.title}>{title}</h3>
                <p className={styles.desc}>{desc}</p>
                <p className={styles.tag}>
                    <span className={styles.tag1}>{tags[0]}</span>
                    <span className={styles.tag2}>{tags[1]}</span>
                    <span className={styles.tag3}>{tags[2]}</span>
                </p>
                <p className={styles.price}><span className={styles.priceNum}>{price}</span>元/月</p>
            </div>
        </div>
    )
}

export default HouseItem