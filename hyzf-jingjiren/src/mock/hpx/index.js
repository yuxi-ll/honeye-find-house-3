import Mock from 'mockjs';

Mock.mock('/api/bill',()=>{
    return Mock.mock({
        'bill':[
            {
                dateTitle:'2021年1月',
                id:'@id',
                children:[
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-10
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-110
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-60
                    }
                ]
            },
            {
                dateTitle:'2021年2月',
                id:'@id',
                children:[
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-103
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-106
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-120
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-230
                    }
                ]
            },
            {
                dateTitle:'2021年3月',
                id:'@id',
                children:[
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-104
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-110
                    }
                ]
            },
            {
                dateTitle:'2021年4月',
                id:'@id',
                children:[
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-109
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-10
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-40
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-30
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-50
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-34
                    }
                ]
            },
            {
                dateTitle:'2021年5月',
                id:'@id',
                children:[
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-15
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-126
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-130
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-70
                    },
                    {
                        img:'@image(40x40,@color)',
                        id:'@id',
                        msg:'微信支付 宏烨找房',
                        money:-45
                    }
                ]
            }
        ]
    })
})