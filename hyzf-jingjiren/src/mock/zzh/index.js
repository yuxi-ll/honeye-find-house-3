import Mock from 'mockjs';

Mock.mock("/api/house",(()=>{
    return Mock.mock({
        "house|6":[
            {
                "img":"https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3579736809,2878872634&fm=26&gp=0.jpg",
                "headline":"@ctitle(8)",
                "money|100000-999999":100000,
                "address":"@ctitle(8,10)",
                "advantage":"@ctitle(3)"
            }
        ]
    })
}))

Mock.mock("/api/agent",(()=>{
    return Mock.mock({
        "agent|3":[
            {
                "tu":"https://dss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1950846641,3729028697&fm=26&gp=0.jpg",
                "ming":"@ctitle(4)",
                "biao":"@ctitle(5)"
            }
        ]
    })
}))


Mock.mock("/api/hua", (() => {
    return Mock.mock({
        "hua": [
            {
            "name": "@cname",
            "time|1-5": 1,
            "title": "@ctitle(20)",
            "area": "@ctitle(2)",
            "type":"多层住宅",
            "region":"海淀区",
            "price|3000-4000": 3000,
        },{
            "name": "@cname",
            "time|1-5": 1,
            "title": "@ctitle(20)",
            "area": "@ctitle(2)",
            "type":"高层住宅",
            "region":"朝阳区",
            "price|3000-4000": 3000,
        },
        {
            "name": "@cname",
            "time|1-5": 1,
            "title": "@ctitle(20)",
            "area": "@ctitle(2)",
            "type":"小高层住宅",
            "region":"丰台区",
            "price|3000-4000": 3000,
        },
        {
            "name": "@cname",
            "time|1-5": 1,
            "title": "@ctitle(20)",
            "area": "@ctitle(2)",
            "type":"商铺/门面房",
            "region":"大兴区",
            "price|3000-4000": 3000,
        },
        {
            "name": "@cname",
            "time|1-5": 1,
            "title": "@ctitle(20)",
            "area": "@ctitle(2)",
            "type":"厂房",
            "region":"延庆区",
            "price|3000-4000": 3000,
        },
        {
            "name": "@cname",
            "time|1-5": 1,
            "title": "@ctitle(20)",
            "area": "@ctitle(2)",
            "type":"写字楼",
            "region":"昌平区",
            "price|3000-4000": 3000,
        },
        {
            "name": "@cname",
            "time|1-5": 1,
            "title": "@ctitle(20)",
            "area": "@ctitle(2)",
            "type":"独院/别墅",
            "region":"海淀区",
            "price|3000-4000": 3000,
        },
        {
            "name": "@cname",
            "time|1-5": 1,
            "title": "@ctitle(20)",
            "area": "@ctitle(2)",
            "type":"车库",
            "region":"海淀区",
            "price|3000-4000": 3000,
        },
    ]
    })
}))