import Mock from "mockjs"
Mock.mock("/api/list",{
    "list|10": [
        {
            "img": "https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3579736809,2878872634&fm=26&gp=0.jpg",
            "title": "@ctitle(3,8)",
            "price|+1232": 12313,
            "address": "@ctitle(2,9)",
            "total": "@ctitle(2)",
        }
    ]
})
export default Mock