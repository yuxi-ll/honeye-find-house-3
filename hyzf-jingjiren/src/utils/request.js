import axios from 'axios';//引入axios
import Nprogress from "nprogress"//引入进度条插件
import "nprogress/nprogress.css"//进度条样式

const instance=axios.create({
    baseURL:'http://157.122.54.189:9060/',
    timeout:50000//请求延时
});

instance.interceptors.request.use(config=>{
    Nprogress.start()
    config.headers.Authorization=localStorage.getItem('token')
    return config
},(error)=>{
    return Promise.reject(error)
})

instance.interceptors.response.use(res=>{
    Nprogress.done()
    return res
},(error)=>{
    if(error.response.status===401)
    {
        window.location.href="/"
    }
    else if(error.response.status===500)
    {
        alert('服务器内部错误')
    }
    return Promise.reject(error)
})

export default instance