import loadable from '@loadable/component';
const layouts = loadable(() => import('../pages/layouts/index'))//首页布局容器
const login = loadable(() => import('../pages/login/index'))//登录
const Register = loadable(() => import('../pages/register/index'))//注册
const home = loadable(() => import('../pages/layouts/home/index'))//首页
const buyHouse = loadable(() => import('../pages/buyHouse/index'))//买房
const rentingHouse = loadable(() => import('../pages/rentingHouse/index'))//租房
const wanted = loadable(() => import('../pages/wanted/index'))//求租
const purchase = loadable(() => import('../pages/purchase/index'))//求购
const housingRing = loadable(() => import('../pages/housingRing/index'))//楼市圈
const broker = loadable(() => import('../pages/broker/index'))//经纪人
const groupPurchase = loadable(() => import('../pages/groupPurchase/index'))//团购看房
const liveSeeRoom = loadable(() => import('../pages/liveSeeRoom/index'))//直播看房
const HouseDetail = loadable(() => import('../pages/mapRoom/houseDetail'))//房屋详情列表
const Vr = loadable(() => import('../pages/mapRoom/vrhouse'))//vr看房
const Chat = loadable(() => import('../pages/wanted/chat'))//求租聊天页面
const Call = loadable(() => import("../pages/wanted/call"))//求租打电话页面
const brokerDetail = loadable(() => import("../pages/brokerDetail/index"))//经纪人详情
const zhan = loadable(() => import("../pages/zhan/index"))//战绩
const second = loadable(() => import("../pages/second/index"))//二手房
const message = loadable(() => import('../pages/message/index'))//消息
const routes = [
    {
        path: '/layout',
        component: layouts,
        children: [
            {
                path: '/layout/home',
                title: '首页',
                icon: 'iconfont icon-76shouye',
                component: home
            },
        ]
    },
    {
        path: '/login',//登录
        component: login
    },
    {
        path: '/vr',
        component: Vr,//vr
    },
    {
        path: '/HouseDetail',
        component: HouseDetail,//房屋详情
    },
    {
        path: '/register',//注册
        component: Register
    },
    {
        path: '/buyHouse',//买房
        component: buyHouse
    },
    {
        path: '/rentingHouse',//租房
        component: rentingHouse
    },
    {
      path: '/second',//二手房
      component: second  
    },
    {
        path: '/wanted',//求租
        component: wanted,
    },
    {
        path: '/chat',//求租聊天
        component: Chat,
    },
    {
        path: '/call',//求租聊天
        component: Call,
    },
    {
        path: '/purchase',//求购
        component: purchase
    },
    {
        path: '/housingRing',//楼市圈
        component: housingRing
    },
    {
        path: '/broker',//排行榜
        component: broker
    },
    {
        path: '/brokerDetail',//经纪人详情
        component: brokerDetail
    },
    {
        path: '/groupPurchase',//团购看房
        component: groupPurchase
    },
    {
        path: '/liveSeeRoom',//直播看房
        component: liveSeeRoom
    },
    {
        path: '/zhan',//战绩
        component: zhan
    },
    {
        path: '/message',//消息
        component: message
    },
    {
        from:'/',
        to:'/login'
    }
]

export default routes