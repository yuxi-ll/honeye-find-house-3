/*
 * @Author: your name
 * @Date: 2021-05-16 19:18:10
 * @LastEditTime: 2021-05-24 22:32:00
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \react-admin\admin\src\index.js
 */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter } from 'react-router-dom'
import './mock/index'
import "./mock/Ymock/index"
import './index.css';
import 'antd/dist/antd.css';
import "./mock/ssh"
import "./mock/yky/index.js"
import "./mock/zzh/second.js"
import "./mock/zzh/sell.js"
import './mock/Ymock/index'

ReactDOM.render(
   <BrowserRouter >
        <App/>
    </BrowserRouter>,
    document.getElementById('root')
);