/*
 * @Author: your name
 * @Date: 2021-05-18 18:43:25
 * @LastEditTime: 2021-05-25 08:06:47
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \react-admin\admin\src\pages\util\http.js
 */
import axios from 'axios'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
// 创建一个新的axios实例;
const instance = axios.create()
// 添加请求拦截器
instance.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    config.headers.Authorization = sessionStorage.getItem('token')
    NProgress.start()
    return config
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
})

// 添加响应拦截器
instance.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    NProgress.done()
    return response
}, function (error) {
    // 对响应错误做点什么
    if (error.response.status === 401) {
        // 也可以使用location.href的形式进行跳转，只是不常用;
        window.location.href = '/'
    } else if (error.response.status === 500) {
        alert('服务器错误')
    }
    return Promise.reject(error)
})

export default instance