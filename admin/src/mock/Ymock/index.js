import Mock from "mockjs"
//地区管理
Mock.mock("/api/area", {
    "list|10": [
        {
            "key|+1": 1,//序号
            "title": "@ctitle(2,3)",//省份
            "qyid|+12": 3,//启用城市
            "jyid|+12": 6,//禁用城市
        }
    ]
})
//城市
Mock.mock("/api/city", {
    "list|10": [
        {
            "key|+1": 1,//序号
            "name": "@cname(2,3)",//城市
            "title": "@ctitle(3,4)",//区域
            "zt": "正常"
        }
    ]
})
//学校管理
Mock.mock("/api/school", {
    "list|15": [
        {
            "key|+1": 1,//序号
            "name": "@cname(2,3)",//学校名称
            "area": "@ctitle(2,3)",//区域
        }
    ]
})
//添加学校
Mock.mock("/api/addschool", {
    "province": [
        {
            value: '河南',
            label: '河南',
        },
        {
            value: '河北',
            label: '河北',
        },
        {
            value: '上海',
            label: '上海',
        },
        {
            value: '北京',
            label: '北京',
        },
        {
            value: '广州',
            label: '广州',
        },
        {
            value: '山西',
            label: '山西',
        },
    ],
    // "city": [
    //     {
    //         value: '焦作',
    //         label: '焦作',
    //     },
    //     {
    //         value: '邯郸',
    //         label: '邯郸',
    //     },
    //     {
    //         value: '郑州',
    //         label: '郑州',
    //     },
    //     {
    //         value: '廊坊',
    //         label: '廊坊',
    //     },{
    //         value: '大同',
    //         label: '大同',
    //     },
    // ],
    // "county|10": [
    //     {
    //         title: "@ctitle(2,3)"
    //     }
    // ]
})
//地铁管理下拉框数据
Mock.mock("/api/subway",{
    "subway":[
        {
            value: '河南',
            label: '河南',
        },
        {
            value: '河北',
            label: '河北',
        },
        {
            value: '上海',
            label: '上海',
        },
        {
            value: '北京',
            label: '北京',
        },
        {
            value: '广州',
            label: '广州',
        },
        {
            value: '山西',
            label: '山西',
        },
    ]
})
//地铁table表格数据
Mock.mock("/api/table",{
    "list|10":[
        {
            "key|+1":1,
            "title":"@ctitle(2,3)",
            "area":"@cname(2,3)",
            "id|+1":1,
            "status":"状态",
            "handle":"操作"
        }
    ]
})
//地铁管理table表格数据
Mock.mock("/api/station",{
    "list|10":[
        {
            "key|+1":1,
            "title":"@ctitle(2,3)",
            "area":"@cname(2,3)",
            "id|+1":1,
            "status":"状态",
            "handle":"操作"
        }
    ]
})
//出租管理表格数据
Mock.mock("/api/rentout",{
    "list|10":[
        {
            "key|+1":1,
            "name":"@cname(2,3)",
            "title":"@ctitle(2,3)",
            "area|+21":123,
            "moneny|+4322":1321,
            "id|+1":1,
            "status":"正常",
            "handle":"操作"
        }
    ]
})
export default Mock