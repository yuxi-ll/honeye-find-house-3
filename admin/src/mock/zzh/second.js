import Mock from "mockjs"

Mock.mock("/api/second", () => {
    return Mock.mock({
        "second|10": [
            {
                'id|+1': 1,
                "lastName": "@cname",
                "age|100-1000": 100,
                "address": /^上海|北京$/,
                "key": '@id'
            }
        ]
    })
})