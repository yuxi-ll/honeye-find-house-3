import Mock from "mockjs"

Mock.mock("/api/sell",()=>{
    return Mock.mock({
        "sell|10":[
            {
                "id|+1":1,
                "rank|80-100":80,
                "province":"@ctitle(2)",
                "city":"@ctitle(2)",
                "town":"@ctitle(2)",
                "key":"@id"   
            }
        ]
    })
})