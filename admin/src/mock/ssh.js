import Mock from 'mockjs'

const data=Mock.mock({
    "list|10":[{
        num:'采菊东篱下@increment(1)',
        'key|+1':1,
        target:"@integer(1000,9999)xxoo",
        grade:"@integer(60,900)",
    }]
})
const data2=Mock.mock({
    "typelist|11":[{
        num:"文采斐然@increment(1)",
        'key|+1':1,
        firm:"@integer(0,9999)号线",
        grade:"@integer(60,900)",
    }]
})
const data3=Mock.mock({
    "typelist|11":[{
        num:"出口成章@increment(1)",
        'key|+1':1,
        firm:"@integer(0,9999)号线",
        grade:"@integer(60,900)",
    }]
})
const dataadd=Mock.mock({
    "typelist|11":[{
        num:"增加@increment(1)",
        'key|+1':1,
        firm:"@integer(0,9999)号线",
        grade:"@integer(60,900)",
    }]
})
const datarec=Mock.mock({
    "typelist|11":[{
        num:"回收@increment(1)",
        'key|+1':1,
        firm:"@integer(0,9999)号线",
        grade:"@integer(60,900)",
    }]
})
const single=Mock.mock({
    "typelist|11":[{
        num:"单页面@increment(1)",
        'key|+1':1,
        firm:"@integer(0,9999)号线",
        grade:"@integer(60,900)",
    }]
})

const type=Mock.mock({
    "typelist|11":[{
        num:"分类@increment(1)",
        'key|+1':1,
        firm:"@integer(0,9999)号线",
        grade:"@integer(60,900)",
    }]
})

Mock.mock("/api/getcollect",()=>{
    return data
})
Mock.mock("/api/gettypelist",()=>{
    return data2
})
Mock.mock("/api/articellist",()=>{
    return data3
})
Mock.mock("/api/dataadd",()=>{
    return dataadd
})
Mock.mock("/api/datarec",()=>{
    return datarec
})
Mock.mock("/api/single",()=>{
    return single
})
Mock.mock("/api/listtype",()=>{
    return type
})