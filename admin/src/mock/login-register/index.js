/*
 * @Author: your name
 * @Date: 2021-05-19 08:29:13
 * @LastEditTime: 2021-05-19 09:40:56
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\honeye-find-house-3\admin\src\mock\login-register\index.js
 */
import Mock from 'mockjs'

const users = [{
    username: 'admin',
    phone: '13345653465',
    password: '123'
}, {
    username: 'user',
    phone: '14324342344',
    password: '123'
}]
//模拟登录接口
Mock.mock('/api/login','post', (options) => {
    console.log(users)
    const {
        username,
        password
    } = JSON.parse(options.body)
    const flag = users.some(item => item.username === username && item.password === password)
    if (flag) {
        return{
            code: 0,
            msg: '登录成功',
            token: `${username}wwqwewr`,
            data: username
        }
    } else {
        return{
            code: -1,
            msg: '登录失败，账号密码错误',
        }
    }
})

//注册接口
Mock.mock('/api/register', (options) => {
    const { username, password, phone } = JSON.parse(options.body)
    const flag = users.some(item => item.username===username||item.phone===phone) 
    if(flag) {
        return {
            code: -1,
            msg: '用户已存在',
        }
    }else {
        users.push({
            username: username,
            password: password,
            phone: phone
        })
        return {
            code: 0,
            msg: '注册成功，请登录',
        }
    }
})