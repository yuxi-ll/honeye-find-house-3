/*
 * @Author: your name
 * @Date: 2021-05-19 22:54:04
 * @LastEditTime: 2021-05-25 20:42:26
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\honeye-find-house-3\admin\src\mock\agent\index.js
 */
import Mock from 'mockjs'
let data = Mock.mock({
    'list|40': [{
        'reviewer': /^1[3-9]\d{9}$/, //评论人
        'key': '@id',
        'name': '@cname', //经纪人
        'last': '@date'+' '+'@time', //评论时间
        'content': '@cword', //问答内容
        'state': /开启|禁用/, //是否开启 
    }]
})

let list = [...data.list]

//获取数据接口
Mock.mock('/api/getagent', () => {
    return {
        code: 0,
        msg: '获取成功',
        data: data.list
    }
})

//删除数据接口
Mock.mock(('/api/deleteagent'), (options) => {
    // console.log(options)
    const {
        id
    } = JSON.parse(options.body)
    data.list = data.list.filter(item => item.key !== id)
    return {
        code: 0,
        msg: '删除成功',
        data: data.list
    }
})

//条件搜索
Mock.mock('/api/selectValueagent', (options) => {
    const value = JSON.parse(options.body)
    list = data.list.filter(item => item.state.includes(value.value))
    if (list.length) {
        return {
            code: 0,
            msg: '搜索成功',
            data: list
        }
    } else {
        return {
            code: 1,
            msg: '搜索失败',
        }
    }
})

//改变switch
Mock.mock('/api/changeSwitchagent', options => {
    const {state, record} = JSON.parse(options.body)
    data.list.map(item => {
        if(item.key === record.key) {
            item.state = state?'开启':'禁用'
        }
    })
    return {
        code: 0,
        msg: '更改成功',
        data: data.list
    }
})

//批量删除
Mock.mock(('/api/deleteSeleagent'), (options) => {
    // console.log(options)
    const {
        selection
    } = JSON.parse(options.body)
    console.log(selection)
    data.list = data.list.filter(item => !selection.includes(item.key))
    return {
        code: 0,
        msg: '删除成功',
        data: data.list
    }
})

