/*
 * @Author: your name
 * @Date: 2021-05-19 22:54:04
 * @LastEditTime: 2021-05-23 18:49:21
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\honeye-find-house-3\admin\src\mock\peopleunity\index.js
 */
import Mock from 'mockjs'
let data = Mock.mock({
    'list|40': [{
        'phone': /^1[3-9]\d{9}$/, //手机号
        'key': '@id',
        'name': '@cname', //用户名
        'last': '@date'+' '+'@time', //最后登录时间
        'ip': '@ip', //最后登录ip
        'isOpen': /开启|禁用/, //是否开启 
        'role': /普通|超级用户/,
    }]
})

let list = [...data.list]

//获取数据接口
Mock.mock('/api/people', () => {
    return {
        code: 0,
        msg: '获取成功',
        data: data.list
    }
})

//删除数据接口
Mock.mock(('/api/deletepeople'), (options) => {
    // console.log(options)
    const {
        id
    } = JSON.parse(options.body)
    data.list = data.list.filter(item => item.key !== id)
    return {
        code: 0,
        msg: '删除成功',
        data: data.list
    }
})

//编辑数据
Mock.mock('/api/updatepeople', (options) => {
    const record = JSON.parse(options.body)
    data.list = data.list.map(item => item.key === record.key ? record : item)
    return {
        code: 0,
        msg: '编辑成功',
        data: data.list
    }
})

//添加数据
Mock.mock('/api/addpeople', (options) => {
    const record = JSON.parse(options.body)
    data.list.unshift(record)
    return {
        code: 0,
        msg: '添加成功',
        data: data.list
    }
})

//模糊搜索
Mock.mock('/api/searchValuepeople', (options) => {
    const value = JSON.parse(options.body)
    list = data.list.filter(item => item.name.includes(value.value)||item.phone.includes(value.value))
    if (list.length) {
        return {
            code: 0,
            msg: '搜索成功',
            data: list
        }
    } else {
        return {
            code: 1,
            msg: '搜索失败',
        }
    }
})

//改变switch
Mock.mock('/api/changeSwitchpeople', options => {
    const {state, record} = JSON.parse(options.body)
    data.list.map(item => {
        if(item.key === record.key) {
            item.isOpen = state?'开启':'禁用'
        }
    })
    return {
        code: 0,
        msg: '更改成功',
        data: data.list
    }
})