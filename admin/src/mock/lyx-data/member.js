/*
 * @Author: your name
 * @Date: 2021-05-19 22:54:04
 * @LastEditTime: 2021-05-25 19:20:47
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\honeye-find-house-3\admin\src\mock\memberunity\index.js
 */
import Mock from 'mockjs'
let data = Mock.mock({
    'list|40': [{
        'type': /普通会员|普通经纪人|中级经纪人|高级经纪人/, //会员类别
        'key': '@id',
        'username': /^1[3-9]\d{9}$/, //手机号
        'nickname': '@cname',//昵称
        'last': '@date' + ' ' + '@time', //注册时间
        'state': /开启|禁用/, //是否开启 
    }]
})

let list = [...data.list]

//获取数据接口
Mock.mock('/api/member', () => {
    return {
        code: 0,
        msg: '获取成功',
        data: data.list
    }
})

//删除数据接口
Mock.mock(('/api/deletemember'), (options) => {
    // console.log(options)
    const {
        id
    } = JSON.parse(options.body)
    data.list = data.list.filter(item => item.key !== id)
    return {
        code: 0,
        msg: '删除成功',
        data: data.list
    }
})

//编辑数据
Mock.mock('/api/updatemember', (options) => {
    const record = JSON.parse(options.body)
    data.list = data.list.map(item => item.key === record.key ? record : item)
    return {
        code: 0,
        msg: '编辑成功',
        data: data.list
    }
})

//添加数据
Mock.mock('/api/addmember', (options) => {
    const record = JSON.parse(options.body)
    data.list.unshift(record)
    return {
        code: 0,
        msg: '添加成功',
        data: data.list
    }
})

//模糊搜索
Mock.mock('/api/searchValuemember', (options) => {
    const value = JSON.parse(options.body)
    console.log(value)
    list = data.list.filter(item => item.username.includes(value))
    if (list.length) {
        return {
            code: 0,
            msg: '搜索成功',
            data: list
        }
    } else {
        return {
            code: 1,
            msg: '搜索失败',
        }
    }
})

//条件搜索
Mock.mock('/api/selectValuemember', (options) => {
    const value = JSON.parse(options.body)
    list = data.list.filter(item => item.type.includes(value.value)||item.state.includes(value.value))
    if (list.length) {
        return {
            code: 0,
            msg: '搜索成功',
            data: list
        }
    } else {
        return {
            code: 1,
            msg: '搜索失败',
        }
    }
})

//改变switch
Mock.mock('/api/changeSwitchmember', options => {
    const {state, record} = JSON.parse(options.body)
    data.list.map(item => {
        if(item.key === record.key) {
            item.state = state?'开启':'禁用'
        }
    })
    return {
        code: 0,
        msg: '更改成功',
        data: data.list
    }
})

//批量删除
Mock.mock(('/api/deleteSelemember'), (options) => {
    // console.log(options)
    const {
        selection
    } = JSON.parse(options.body)
    console.log(selection)
    data.list = data.list.filter(item => !selection.includes(item.key))
    return {
        code: 0,
        msg: '删除成功',
        data: data.list
    }
})

