/*
 * @Author: your name
 * @Date: 2021-05-19 22:54:04
 * @LastEditTime: 2021-05-30 10:16:18
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\honeye-find-house-3\admin\src\mock\housereview\index.js
 */
import Mock from 'mockjs'
let data = Mock.mock({
    'list|40': [{
        'name': /^1[3-9]\d{9}$/, //手机号
        'key': '@id',
        'house': '呼兰小区'+ '@integer(1,1000)', //楼盘
        'last': '@date'+' '+'@time', //提交时间
        'content': '@cword(5,10)', //问答内容
        'state': /开启|禁用/, //是否开启 
    }]
})

let list = [...data.list]

//获取数据接口
Mock.mock('/api/housereview', () => {
    return {
        code: 0,
        msg: '获取成功',
        data: data.list
    }
})

//删除数据接口
Mock.mock(('/api/deletehousereview'), (options) => {
    // console.log(options)
    const {
        id
    } = JSON.parse(options.body)
    data.list = data.list.filter(item => item.key !== id)
    return {
        code: 0,
        msg: '删除成功',
        data: data.list
    }
})


//改变switch
Mock.mock('/api/changeSwitchhousereview', options => {
    const {state, record} = JSON.parse(options.body)
    data.list.map(item => {
        if(item.key === record.key) {
            item.state = state?'开启':'禁用'
        }
    })
    return {
        code: 0,
        msg: '更改成功',
        data: data.list
    }
})

//批量删除
Mock.mock(('/api/deleteSelehousereview'), (options) => {
    // console.log(options)
    const {
        selection
    } = JSON.parse(options.body)
    console.log(selection)
    data.list = data.list.filter(item => !selection.includes(item.key))
    return {
        code: 0,
        msg: '删除成功',
        data: data.list
    }
})

