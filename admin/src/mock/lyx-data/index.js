/*
 * @Author: your name
 * @Date: 2021-05-19 22:54:04
 * @LastEditTime: 2021-05-24 18:43:08
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\honeye-find-house-3\admin\src\mock\community\index.js
 */
import Mock from 'mockjs'
let data = Mock.mock({
    'list|40': [{
        'address': '@province' + '@city' + '@county', //所在地区
        'key': '@id',
        'name': '兰花小区' + '@integer(1,10000)', //小区名称
        'last': '@date'+' '+'@time', //更新时间
        'range|+1': 1, //排名
        'isOpen': /开启|禁用/, //是否开启 
    }]
})

let list = [...data.list]

//获取数据接口
Mock.mock('/api/community', () => {
    return {
        code: 0,
        msg: '获取成功',
        data: data.list
    }
})

//删除数据接口
Mock.mock(('/api/deleteComm'), (options) => {
    // console.log(options)
    const {
        id
    } = JSON.parse(options.body)
    data.list = data.list.filter(item => item.key !== id)
    return {
        code: 0,
        msg: '删除成功',
        data: data.list
    }
})

//编辑数据
Mock.mock('/api/updateComm', (options) => {
    const record = JSON.parse(options.body)
    data.list = data.list.map(item => item.key === record.key ? record : item)
    return {
        code: 0,
        msg: '编辑成功',
        data: data.list
    }
})

//添加数据
Mock.mock('/api/addComm', (options) => {
    const record = JSON.parse(options.body)
    data.list.unshift(record)
    return {
        code: 0,
        msg: '添加成功',
        data: data.list
    }
})

//模糊搜索
Mock.mock('/api/searchValue', (options) => {
    const value = JSON.parse(options.body)
    console.log(value)
    list = data.list.filter(item => item.name.includes(value))
    if (list.length) {
        return {
            code: 0,
            msg: '搜索成功',
            data: list
        }
    } else {
        return {
            code: 1,
            msg: '搜索失败',
        }
    }
})

//条件搜索
Mock.mock('/api/selectValue', (options) => {
    const value = JSON.parse(options.body)
    list = data.list.filter(item => item.address.includes(value.value)||item.isOpen.includes(value.value))
    if (list.length) {
        return {
            code: 0,
            msg: '搜索成功',
            data: list
        }
    } else {
        return {
            code: 1,
            msg: '搜索失败',
        }
    }
})

//改变switch
Mock.mock('/api/changeSwitch', options => {
    const {state, record} = JSON.parse(options.body)
    data.list.map(item => {
        if(item.key === record.key) {
            item.isOpen = state?'开启':'禁用'
        }
    })
    return {
        code: 0,
        msg: '更改成功',
        data: data.list
    }
})

//批量删除
Mock.mock(('/api/deleteSele'), (options) => {
    // console.log(options)
    const {
        selection
    } = JSON.parse(options.body)
    console.log(selection)
    data.list = data.list.filter(item => !selection.includes(item.key))
    return {
        code: 0,
        msg: '删除成功',
        data: data.list
    }
})

