/*
 * @Author: your name
 * @Date: 2021-05-19 22:54:04
 * @LastEditTime: 2021-05-24 18:53:39
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\honeye-find-house-3\admin\src\mock\community\index.js
 */
import Mock from 'mockjs'
let success = Mock.mock({
    'list|40': [{
        'address': '@province' + '@city' + '@county', //所在地区
        'key': '@id',
        'name': '成交户型' + '@integer(1,10000)', //小区名称
        'last': '@date'+' '+'@time', //成交时间
        'price': '@integer(10,1000)', //价格
        'acreage': '@integer(50,100)', //占地面积
    }]
})

let list = [...success.list]

//获取数据接口
Mock.mock('/api/success', () => {
    return {
        code: 0,
        msg: '获取成功',
        data: success.list
    }
})

//删除数据接口
Mock.mock(('/api/deleteSucc'), (options) => {
    // console.log(options)
    const {
        id
    } = JSON.parse(options.body)
    success.list = success.list.filter(item => item.key !== id)
    return {
        code: 0,
        msg: '删除成功',
        data: success.list
    }
})

//编辑数据
Mock.mock('/api/updateSucc', (options) => {
    const record = JSON.parse(options.body)
    success.list = success.list.map(item => item.key === record.key ? record : item)
    return {
        code: 0,
        msg: '编辑成功',
        data: success.list
    }
})

//添加数据
Mock.mock('/api/addSucc', (options) => {
    const record = JSON.parse(options.body)
    success.list.unshift(record)
    return {
        code: 0,
        msg: '添加成功',
        data: success.list
    }
})

//模糊搜索
Mock.mock('/api/searchValueSucc', (options) => {
    const value = JSON.parse(options.body)
    console.log(value)
    list = success.list.filter(item => item.name.includes(value))
    if (list.length) {
        return {
            code: 0,
            msg: '搜索成功',
            data: list
        }
    } else {
        return {
            code: 1,
            msg: '搜索失败',
        }
    }
})

//条件搜索
Mock.mock('/api/selectValueSucc', (options) => {
    const value = JSON.parse(options.body)
    console.log(value)
    list = success.list.filter(item => item.address.includes(value.value))
    if (list.length) {
        return {
            code: 0,
            msg: '搜索成功',
            data: list
        }
    } else {
        return {
            code: 1,
            msg: '搜索失败',
        }
    }
})

//批量删除
Mock.mock(('/api/deleteSeleSucc'), (options) => {
    // console.log(options)
    const {
        selection
    } = JSON.parse(options.body)
    console.log(selection)
    success.list = success.list.filter(item => !selection.includes(item.key))
    return {
        code: 0,
        msg: '删除成功',
        data: success.list
    }
})