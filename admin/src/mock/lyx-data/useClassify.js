/*
 * @Author: your name
 * @Date: 2021-05-19 22:54:04
 * @LastEditTime: 2021-05-25 10:32:53
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\honeye-find-house-3\admin\src\mock\peopleunity\index.js
 */
import Mock from 'mockjs'
let userClassify = Mock.mock({
    'list|40': [{
        'name': /普通会员|普通经纪人|中级经纪人|高级经纪人/, //分类名称
        'key': '@id',
        'second': /是|否/, //二手房审核
        'rent': /是|否/, //出租房审核
        'sellOffice': /是|否/, //写字楼出售审核
        'rentOffice': /是|否/, //写字楼出租审核
        'sellStore': /是|否/,//商铺出售审核
        'rentStore': /是|否/,//商铺出租审核
        'annual': /是|否/,//年费
        'class': '@integer(1, 10)',//等级
        'state': /开启|禁用/,//状态
    }]
})

let list = [...userClassify.list]

//获取数据接口
Mock.mock('/api/getUserClassify', () => {
    return {
        code: 0,
        msg: '获取成功',
        data: userClassify.list
    }
})

//删除数据接口
Mock.mock(('/api/deleteUsersClassify'), (options) => {
    // console.log(options)
    const {
        id
    } = JSON.parse(options.body)
    userClassify.list = userClassify.list.filter(item => item.key !== id)
    return {
        code: 0,
        msg: '删除成功',
        data: userClassify.list
    }
})

//编辑数据
Mock.mock('/api/updateUsersClassify', (options) => {
    const record = JSON.parse(options.body)
    userClassify.list = userClassify.list.map(item => item.key === record.key ? record : item)
    return {
        code: 0,
        msg: '编辑成功',
        data: userClassify.list
    }
})

//添加数据
Mock.mock('/api/addUsersClassify', (options) => {
    const record = JSON.parse(options.body)
    userClassify.list.unshift(record)
    return {
        code: 0,
        msg: '添加成功',
        data: userClassify.list
    }
})

//改变switch
Mock.mock('/api/changeSwitchUsersClassify', options => {
    const {state, record} = JSON.parse(options.body)
    userClassify.list.map(item => {
        if(item.key === record.key) {
            item.state = state?'开启':'禁用'
        }
    })
    return {
        code: 0,
        msg: '更改成功',
        data: userClassify.list
    }
})

//批量删除
Mock.mock(('/api/deleteSeleUsersClassify'), (options) => {
    // console.log(options)
    const {
        selection
    } = JSON.parse(options.body)
    console.log(selection)
    userClassify.list = userClassify.list.filter(item => !selection.includes(item.key))
    return {
        code: 0,
        msg: '删除成功',
        data: userClassify.list
    }
})