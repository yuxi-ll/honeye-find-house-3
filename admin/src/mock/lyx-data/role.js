/*
 * @Author: your name
 * @Date: 2021-05-19 22:54:04
 * @LastEditTime: 2021-05-30 10:31:56
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\honeye-find-house-3\admin\src\mock\role\index.js
 */
import Mock from 'mockjs'
let data = Mock.mock({
    'list|40': [{
        'key': '@id',
        'name': '@cname', //角色名称
        'expandedKeys': ['system', 'modles', 'auth', 'houses', 'rent'],
        'checkedKeys': ['modles', 'houses'],
        'selectedKeys': [],
        'treeData': [
            {
                title: '系统',
                key: 'system',
                children: [
                    {
                        title: '模块管理',
                        key: 'modles',
                        children: [
                            {
                                title: '地区管理',
                                key: 'area',
                            },
                            {
                                title: '学校管理',
                                key: 'school',
                            },
                            {
                                title: '地铁管理',
                                key: 'subway',
                            },
                        ],
                    },
                    {
                        title: '人员权限',
                        key: 'auth',
                        children: [
                            {
                                title: '人员管理',
                                key: 'authman',
                            },
                            {
                                title: '角色管理',
                                key: 'role',
                            },
                        ],
                    },
                ],
            },
            {
                title: '楼盘',
                key: 'houses',
                children: [
                    {
                        title: '楼盘管理',
                        key: 'houseman',
                        children: [
                            {
                                title: '楼盘列表',
                                key: 'houselist'
                            }
                        ]
                    },
                    {
                        title: '开发商管理',
                        key: 'developers',
                    },
                ],
            },
            {
                title: '二手/租房',
                key: 'rent',
                children: [
                    {
                        title: '小区管理',
                        key: 'community',
                    },
                    {
                        title: '二手房管理',
                        key: 'second',
                    },
                    {
                        title: '出租房',
                        key: 'renthouse'
                    }
                ]
            },
        ]
    }]
})

let list = [...data.list]

//获取数据接口
Mock.mock('/api/role', () => {
    return {
        code: 0,
        msg: '获取成功',
        data: data.list
    }
})

//删除数据接口
Mock.mock(('/api/deleterole'), (options) => {
    // console.log(options)
    const {
        id
    } = JSON.parse(options.body)
    data.list = data.list.filter(item => item.key !== id)
    return {
        code: 0,
        msg: '删除成功',
        data: data.list
    }
})

//编辑数据
Mock.mock('/api/updaterole', (options) => {
    const record = JSON.parse(options.body)
    data.list = data.list.map(item => item.key === record.key ? record : item)
    return {
        code: 0,
        msg: '编辑成功',
        data: data.list
    }
})

//添加数据
Mock.mock('/api/addrole', (options) => {
    const record = JSON.parse(options.body)
    data.list.unshift(record)
    return {
        code: 0,
        msg: '添加成功',
        data: data.list
    }
})
