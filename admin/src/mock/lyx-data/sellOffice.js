/*
 * @Author: your name
 * @Date: 2021-05-24 19:37:59
 * @LastEditTime: 2021-05-25 08:48:48
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\honeye-find-house-3\admin\src\mock\lyx-data\sellOffice.js
 */
import Mock from 'mockjs'
let office = Mock.mock({
    'list|40': [{
        'house': '中山水城200平方米'+'@province'+'@city', //房型
        'key': '@id',
        'name': '地王大厦' + '@integer(1,10000)', //楼盘名称
        'price': '@integer(10,1000)', //价格
        'acreage': '@integer(50,100)', //占地面积
        'range|+1': 1,//排名 
        'state': /开启|禁用/,//状态
    }]
})

let list = [...office.list]

//获取数据接口
Mock.mock('/api/office', () => {
    return {
        code: 0,
        msg: '获取成功',
        data: office.list
    }
})

//删除数据接口
Mock.mock(('/api/deleteOffice'), (options) => {
    // console.log(options)
    const {
        id
    } = JSON.parse(options.body)
    office.list = office.list.filter(item => item.key !== id)
    return {
        code: 0,
        msg: '删除成功',
        data: office.list
    }
})

//编辑数据
Mock.mock('/api/updateOffice', (options) => {
    const record = JSON.parse(options.body)
    office.list = office.list.map(item => item.key === record.key ? record : item)
    return {
        code: 0,
        msg: '编辑成功',
        data: office.list
    }
})

//添加数据
Mock.mock('/api/addOffice', (options) => {
    const record = JSON.parse(options.body)
    office.list.unshift(record)
    return {
        code: 0,
        msg: '添加成功',
        data: office.list
    }
})

//模糊搜索
Mock.mock('/api/searchValueOffice', (options) => {
    const value = JSON.parse(options.body)
    console.log(value)
    list = office.list.filter(item => item.name.includes(value))
    if (list.length) {
        return {
            code: 0,
            msg: '搜索成功',
            data: list
        }
    } else {
        return {
            code: 1,
            msg: '搜索失败',
        }
    }
})

//条件搜索
Mock.mock('/api/selectValueOffice', (options) => {
    const value = JSON.parse(options.body)
    console.log(value)
    list = office.list.filter(item => item.house.includes(value.value)||item.state.includes(value.value))
    if (list.length) {
        return {
            code: 0,
            msg: '搜索成功',
            data: list
        }
    } else {
        return {
            code: 1,
            msg: '搜索失败',
        }
    }
})

//批量删除
Mock.mock(('/api/deleteSeleOffice'), (options) => {
    // console.log(options)
    const {
        selection
    } = JSON.parse(options.body)
    console.log(selection)
    office.list = office.list.filter(item => !selection.includes(item.key))
    return {
        code: 0,
        msg: '删除成功',
        data: office.list
    }
})

//改变switch
Mock.mock('/api/changeSwitchOffice', options => {
    const {state, record} = JSON.parse(options.body)
    office.list.map(item => {
        if(item.key === record.key) {
            item.isOpen = state?'开启':'禁用'
        }
    })
    return {
        code: 0,
        msg: '更改成功',
        office: office.list
    }
})