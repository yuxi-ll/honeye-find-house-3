/*
 * @Author: your name
 * @Date: 2021-05-30 09:30:39
 * @LastEditTime: 2021-05-30 10:01:31
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\honeye-find-house-3\admin\src\mock\lyx-data\appointment.js
 */
import Mock from 'mockjs'
let appointment = Mock.mock({
    'list|40': [{
        'name': '@cname',//联系人
        'phone': /^1[3-9]\d{9}$/,//联系电话
        'key': '@id',
        'house': '宏烨' + '@integer(1,10000)', //房源名称
        'type': /新房|二手房|出租房|写字楼出售|写字楼出租|商品出售|商铺出租/,//房源类型
        'methods': /团购报名|领取红包|预约看房/,//预约方式
        'last': '@date'+' '+'@time', //提交时间
        'agent': '@cname', //经纪人
        'state': /开启|禁用/, //状态
    }]
})

let list = [...appointment.list]

//获取数据接口
Mock.mock('/api/appointment', () => {
    return {
        code: 0,
        msg: '获取成功',
        data: appointment.list
    }
})

//删除数据接口
Mock.mock(('/api/deleteapp'), (options) => {
    // console.log(options)
    const {
        id
    } = JSON.parse(options.body)
    appointment.list = appointment.list.filter(item => item.key !== id)
    return {
        code: 0,
        msg: '删除成功',
        data: appointment.list
    }
})

//编辑数据
Mock.mock('/api/updateapp', (options) => {
    const record = JSON.parse(options.body)
    appointment.list = appointment.list.map(item => item.key === record.key ? record : item)
    return {
        code: 0,
        msg: '编辑成功',
        data: appointment.list
    }
})
//条件搜索
Mock.mock('/api/selectValueapp', (options) => {
    const value = JSON.parse(options.body)
    console.log(value)
    list = appointment.list.filter(item => item.type.includes(value.value)||item.methods.includes(value.value))
    if (list.length) {
        return {
            code: 0,
            msg: '搜索成功',
            data: list
        }
    } else {
        return {
            code: 1,
            msg: '搜索失败',
        }
    }
})

//批量删除
Mock.mock(('/api/deleteSeleapp'), (options) => {
    // console.log(options)
    const {
        selection
    } = JSON.parse(options.body)
    console.log(selection)
    appointment.list = appointment.list.filter(item => !selection.includes(item.key))
    return {
        code: 0,
        msg: '删除成功',
        data: appointment.list
    }
})

//改变switch
Mock.mock('/api/changeSwitchapp', options => {
    const {state, record} = JSON.parse(options.body)
    appointment.list.map(item => {
        if(item.key === record.key) {
            item.state = state?'开启':'禁用'
        }
    })
    return {
        code: 0,
        msg: '更改成功',
        data: appointment.list
    }
})