import Mock from "mockjs";
const data = Mock.mock({
    // 广告
    "list|20": [
        {
            // 序号
            'xuhao': '广告@increment(1)',
            // 企业类型
            'types': '@integer(1000,9999)' + '号线',
            // 投放时间
            'marketTime|100-999': 1,
            // 广告内容

            'advContent|10-99': 1,
            // id
            'key': '@id'
        }
    ],
    // 推荐
    'list1|20': [
        {
            // 序号
            'xuhao': '桃花不换酒@increment(1)',
            // 企业
            'types': '@integer(1000,9999)' + '号线',
            // 推荐优先级
            'priority|100-999': 1,
            // id
            'key': '@id'
        }
    ],
    // 楼盘
    'list2|20': [
        {
            // 楼盘名称
            'name': '唯此心如旧@increment(1)',
            // 所属区域
            'area': '@ctitle(2,5)' + '公司',
            // 价格
            'price': '青山区00元/平方米',
            // 排序
            'sorts|1-9': 1,
            // 状态
            'state': '@ctitle(2)' + '区',
            // id
            'key': '@id',
            // 户型
            'img1': 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3777137150,3789911500&fm=11&gp=0.jpg',
            // 相册
            'img2': 'https://ss0.bdstatic.com/70cFuHSh_Q1YnxGkpoWK1HF6hhy/it/u=2284197561,802618237&fm=26&gp=0.jpg',
            // 动态
            'img3': 'https://ss1.bdstatic.com/70cFuXSh_Q1YnxGkpoWK1HF6hhy/it/u=2362849222,1797549619&fm=26&gp=0.jpg',
            // 沙盘
            'img4': 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=1690534887,122608978&fm=26&gp=0.jpg'
        }
    ],
    // 开发商
    'list3|20': [
        {
            // 开发商名称
            'name': '@ctitle(2,4)' + '公司',
            // 图片
            'img': '@image(70x70,@color)',
            // 排序
            'sorts|100-888': 1,
            // 类型
            'types': /住宅|商铺|别墅/,
            // 状态
            'states': /热卖中|售空/,
            // id
            'key': '@id'
        }
    ],
    // 团购列表
    'list4|20': [
        {
            // 楼盘名称
            'name': /汤臣一品|绿地房产|澳门|平安门|恒大华府/,
            // 图片
            'img': '@image(70x70,@color)',
            // 排序
            'sorts|10-88': 1,
            // 位置
            'area': '@ctitle(2)' + '区',
            // 类型
            'types': /商铺|别墅|住宅/,
            // 售价
            'price|1000-9999': 1,
            // id
            'key': '@id'
        }
    ]
})
let list = [...data.list]
let list1 = [...data.list1]
let list2 = [...data.list2]
let list3 = [...data.list3]
let list4 = [...data.list4]
Mock.mock('/api/advertising', () => {
    return data
})
// 模拟广告删除接口
Mock.mock('/api/advertisingDelete', (options) => {
    console.log(options)
    const { id } = JSON.parse(options.body)
    data.list = data.list.filter(item => item.key != id)
    return {
        data: data.list
    }
})

// 模拟推荐删除接口
Mock.mock('/api/recommendDelete', (options) => {
    console.log(options)
    const { id } = JSON.parse(options.body)
    data.list1 = data.list1.filter(item => item.key != id)
    return {
        data1: data.list1
    }
})
// 模拟楼盘删除接口
Mock.mock('/api/premisesDelete', (options) => {
    console.log(options)
    const { id } = JSON.parse(options.body)
    data.list2 = data.list2.filter(item => item.key != id)
    return {
        data2: data.list2
    }
})

// 模拟开发商删除接口
Mock.mock('/api/developersDelete', (options) => {
    console.log(options)
    const { id } = JSON.parse(options.body)
    data.list3 = data.list3.filter(item => item.key != id)
    return {
        data3: data.list3
    }
})
// 模拟团购删除接口
Mock.mock('/api/groupDelete', (options) => {
    console.log(options)
    const { id } = JSON.parse(options.body)
    data.list4 = data.list4.filter(item => item.key != id)
    return {
        data4: data.list4
    }
})



// 模拟编辑接口
Mock.mock('/api/edit', (options) => {

    console.log(options)
    const { id, xuhao, types, marketTime, advContent } = JSON.parse(options.body)
    console.log(id, xuhao, types, marketTime, advContent)
    data.list.map(item => {

        if (item.key === id) {
            // console.log(item.xuhao, xuhao)
            item.xuhao = xuhao;
            item.types = types;
            item.marketTime = marketTime;
            item.advContent = advContent;
        }
    }
    )
    return {
        data: data.list
    }
})

// 模拟推荐编辑接口
Mock.mock('/api/recommendedit', (options) => {

    console.log(options)
    const { id, xuhao, types, priority } = JSON.parse(options.body)
    console.log(id, xuhao, types, priority)
    data.list1.map(item => {

        if (item.key === id) {
            // console.log(item.xuhao, xuhao)
            item.xuhao = xuhao;
            item.types = types;
            item.priority = priority;

        }
    }
    )
    return {
        data1: data.list1
    }
})

// 模拟楼盘编辑接口
Mock.mock('/api/premisesedit', (options) => {

    console.log(options)
    const { id, name, area, price, sorts, state } = JSON.parse(options.body)
    console.log(id, name, area, price, sorts, state)
    data.list2.map(item => {

        if (item.key === id) {
            // console.log(item.xuhao, xuhao)
            item.name = name;
            item.area = area;
            item.price = price;
            item.sorts = sorts;
            item.state = state;

        }
    }
    )
    return {
        data2: data.list2
    }
})
// 模拟开发商编辑接口
Mock.mock('/api/developersedit', (options) => {

    console.log(options)
    const { id, name, img, sorts, types, states } = JSON.parse(options.body)

    data.list3.map(item => {

        if (item.key === id) {
            // console.log(item.xuhao, xuhao)
            item.name = name;
            item.img = img;
            item.sorts = sorts;
            item.types = types;
            item.states = states;

        }
    }
    )
    return {
        data3: data.list3
    }
})
// 模拟团购编辑接口
Mock.mock('/api/groupedit', (options) => {

    console.log(options)
    const { id, name, img, sorts, area, types, price } = JSON.parse(options.body)
    console.log(options.body);
    data.list4.map(item => {

        if (item.key === id) {
            // console.log(item.xuhao, xuhao)
            item.name = name;
            item.img = img;
            item.sorts = sorts;
            item.types = types;
            item.price = price;
            item.area = area
        }
    }
    )
    return {
        data4: data.list4
    }
})



// 广告模糊搜索
Mock.mock('/api/search1', (options) => {
    const { value } = JSON.parse(options.body)
    console.log(value)
    data.list = list.filter(item => item.xuhao.includes(value) || item.types.includes(value))
    return {
        data: data.list
    }

})


// 推荐模糊搜索
Mock.mock('/api/recommendsearch1', (options) => {
    const { value } = JSON.parse(options.body)
    console.log(value)
    data.list1 = list1.filter(item => item.xuhao.includes(value) || item.types.includes(value) || item.priority.toString().includes(value))
    return {
        data1: data.list1
    }

})

// 楼盘模糊搜索
Mock.mock('/api/premisessearch1', (options) => {
    const { value, value1 } = JSON.parse(options.body)
    console.log(value, value1)
    list2 = data.list2.filter(item => item.name.includes(value) || item.area.includes(value1))
    return {
        data2: list2
    }

})
// 开发商模糊搜索
Mock.mock('/api/developerssearch1', (options) => {
    const { value, value1 } = JSON.parse(options.body)
    console.log(value, value1)
    list3 = data.list3.filter(item => item.name.includes(value) && item.states.includes(value1))
    return {
        data3: list3
    }

})
// 团购模糊搜索
Mock.mock('/api/groupsearch1', (options) => {
    const { value, value1 } = JSON.parse(options.body)
    console.log(value, value1)
    list4 = data.list4.filter(item => item.name.includes(value) && item.area.includes(value1))
    return {
        data4: list4
    }

})

// 楼盘添加接口
Mock.mock('/api/premisesadd', (options) => {
    const { key, name, area, price, sorts, state } = JSON.parse(options.body)
    data.list2.unshift({ key, name, area, price, sorts, state })
    return {
        data2: data.list2
    }
})
// 团购添加接口
Mock.mock('/api/groupadd', (options) => {
    const { key, name, img, sorts, area, types, price } = JSON.parse(options.body)
    data.list4.unshift({ key, name, img, sorts, area, types, price })
    return {
        data4: data.list4
    }
})
// 开发商添加接口
Mock.mock('/api/developadd', (options) => {
    const { key, name, img, sorts, types, states } = JSON.parse(options.body)
    data.list3.unshift({ key, name, img, sorts, types, states })
    return {
        data3: data.list3
    }
})




