/*
 * @Author: your name
 * @Date: 2021-05-16 19:18:10
 * @LastEditTime: 2021-05-18 14:32:35
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \react-admin\admin\src\App.js
 */
import React from 'react'
import route from './routes/route'
import Routeview from './routes/index'
const App = () => {
  return <div className="app">
      <Routeview routes={route}></Routeview>
  </div>
}

export default App