import { Select } from 'antd';

const { Option } = Select;
const Selection = (props) => {
    const handleChange = (value) => {
        props.selectValue(value)
    }
    return <Select defaultValue="请选择"  style={{width: '100%', padding: '10px 0' }} onChange={handleChange}>
        {
            props.type.map(item => <Option key={item} value={item}>{item}</Option>)
        }
    </Select>
}

export default Selection