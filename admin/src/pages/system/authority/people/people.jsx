import React, { useState, useEffect } from 'react'
import { Breadcrumb, Card, Table, Button, Col, Row, Input, Switch, Modal, Form, Popconfirm, message } from 'antd'
import { getpeople, deletepeople, updatepeople, addpeople, searchValuepeople, changeSwitchpeople } from '../../../../api/index'
import './people.css'
import moment from 'moment'

const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
};

const People = () => {
    const [peopleList, setpeopleList] = useState([])
    const [selectionType, setSelectionType] = useState('checkbox');
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [isEdit, setIsEdit] = useState(false)
    const [form] = Form.useForm();
    const [record, setRecord] = useState({})
    const [seaValue, setSeaValue] = useState('')

    useEffect(() => {
        getpeopleList()
    }, [])
    const columns = [
        {
            title: '用户名',
            dataIndex: 'name',
        },
        {
            title: '手机号',
            dataIndex: 'phone',
        },
        {
            title: '角色',
            dataIndex: 'role',
        },
        {
            title: '最后登录时间',
            dataIndex: 'last',
            defaultSortOrder: 'descend',
            sorter: (a, b) => new Date(a.last).getTime() - new Date(b.last).getTime(),
        },
        {
            title: '最后登录ip',
            dataIndex: 'ip',
        },
        {
            title: '状态',
            dataIndex: 'isOpen',
            render: (isOpen, record) => {
                return <Switch checked={isOpen === '开启' ? true : false} onChange={(e) => onChangeSwitch(e, record)} />
            }
        }, {
            title: '操作',
            dataIndex: 'action',
            render: (text, record) => {
                return <div>
                    <Button type='primary' onClick={() => edit(record)}>编辑</Button>
                    <Popconfirm
                        title="确定删除吗"
                        onConfirm={() => confirm(record)}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                    >
                        <Button type='error'>删除</Button>
                    </Popconfirm>
                </div>
            }
        }
    ];
    //获取人员列表
    const getpeopleList = async () => {
        const { data: res } = await getpeople()
        if (res.code === 0) {
            message.success(res.msg)
            setpeopleList([...res.data])
        }
    }
    //开关
    const onChangeSwitch = async (checked, record) => {
        const { data: res } = await changeSwitchpeople({ state: checked, record })
        if (res.code === 0) {
            message.success(res.msg)
            setpeopleList([...res.data])
        }
    }
    //编辑
    const edit = async (record) => {
        form.setFieldsValue(record)
        setIsModalVisible(true)
        setIsEdit(true)
        setRecord({ ...record })
    }
    //点击确认
    const handleOk = () => {
        if (isEdit) {
            editItem()
        } else {
            addItem()
        }
        setIsModalVisible(false);
    };
    //编辑
    const editItem = async () => {
        const formItem = { ...record, ...form.getFieldValue() }
        const { data: res } = await updatepeople({ ...formItem })
        if (res.code === 0) {
            message.success(res.msg)
            setpeopleList([...res.data])
        }
    }
    //添加
    const addItem = async () => {
        const formItem = form.getFieldValue()
        formItem.key = Math.random().toString().slice(2, 8)
        formItem.range = peopleList.length + 1
        formItem.last = moment(new Date()).format('YYYY-MM-DD HH:MM:SS')

        formItem.ip = Math.random().toString().slice(2, 5) + '.' + Math.random().toString().slice(2, 5) + '.' + Math.random().toString().slice(2, 4) + '.' + Math.random().toString().slice(2, 5)
        const { data: res } = await addpeople({ ...formItem })
        if (res.code === 0) {
            message.success(res.msg)
            setpeopleList([...res.data])
        }
    }
    //点击取消
    const handleCancel = () => {
        setIsModalVisible(false);
    };
    //确认删除
    const confirm = (record) => {
        deleteItem(record)
        message.success('删除成功');
    }
    //取消删除
    const cancel = (e) => {
        message.error('取消成功');
    }
    //删除
    const deleteItem = async (record) => {
        const { data: res } = await deletepeople({ id: record.key })
        setpeopleList([...res.data])
    }
    //模糊搜索
    const onChange = (e) => {
        setSeaValue(e.target.value)
    }
    //回车按下
    const pressEnter = async () => {
        if (seaValue === '') {
            getpeopleList()
        } else {
            const { data: res } = await searchValuepeople({ value: seaValue })
            if (res.code === 0) {
                message.success(res.msg)
                setpeopleList([...res.data])
            } else {
                message.error(res.msg)
            }
        }
    }
    return <div className="people">
        <Card>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/home">首页</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/people">系统</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/people">权限管理</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/people">人员管理</a>
                </Breadcrumb.Item>
            </Breadcrumb>
        </Card>
        <Row gutter={16}>
            <Col className="gutter-row" span={12}>
                <Input placeholder="搜索用户名或手机号" allowClear={true} value={seaValue} onChange={onChange} onPressEnter={pressEnter} style={{ width: '100%', margin: '10px 0' }} />
            </Col>
            <Col className="gutter-row" span={4}>
                <Button type='primary' style={{ width: '100%', margin: '10px 0' }} onClick={() => { setSeaValue(''); getpeopleList(); }}>重置</Button>
            </Col>
            <Col className="gutter-row" span={4}>
                <Button type='primary' style={{ width: '100%', margin: '10px 0' }} onClick={() => {
                    form.setFieldsValue({ name: '', phone: '', role: '' });
                    setIsModalVisible(true);
                    setIsEdit(false);
                }}>新建</Button>
            </Col>
        </Row>
        <Card style={{ width: '100%', height: '100%' }}>
            <Table
                columns={columns}
                dataSource={peopleList}
                pagination={{
                    defaultPageSize: 5,
                    defaultCurrent: 1
                }}
            />
        </Card>
        <Modal title={isEdit ? "编辑内容" : "添加信息"} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
            <Form
                {...layout}
                form={form}
                name="basic"
                initialValues={{ remember: true }}
            >
                <Form.Item
                    label="用户名称"
                    name="name"
                    rules={[{ required: true, message: '请输入用户名称' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="手机号"
                    name="phone"
                    rules={[{ required: true, message: '请输入手机号' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="角色"
                    name="role"
                    rules={[{ required: true, message: '请输入角色' }]}
                >
                    <Input />
                </Form.Item>
            </Form>
        </Modal>
    </div>
}
export default People
