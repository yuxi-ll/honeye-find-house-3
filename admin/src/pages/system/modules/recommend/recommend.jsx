import React from 'react'
import { useState, useEffect } from "react"
import http from "../../../../util/http"
import { Input } from 'antd';
import "../advertising/advertising.css"
import { Table, Space } from 'antd';
import { Modal, } from 'antd';
import { Form, } from 'antd';
import { Button } from 'antd';
import { SearchOutlined } from '@ant-design/icons'
import { Breadcrumb, Card } from 'antd';
const Recommend = () => {

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [selectionType, setSelectionType] = useState('checkbox');


    const handleOk = () => {
        setIsModalVisible(false);
        // form.submit()

    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    const [data, setdata] = useState([])
    const [record, setrecord] = useState(undefined)
    const [value, setvalue] = useState(String)
    useEffect(() => {
        getlist();
        form.setFieldsValue(record)

    }, [])
    // 列表
    const columns = [{
        title: '序号',
        dataIndex: "xuhao"


    },
    {
        title: '企业',
        dataIndex: "types"

    },
    {
        title: '推荐优先级',
        dataIndex: 'priority',

        sorter: {
            compare: (a, b) => a.priority - b.priority,
            multiple: 3,
        },
    },

    {
        title: "操作",

        render: (text, record) => (
            <Space size="middle">
                <a type="primary" onClick={() => showModal(record)}>编辑</a>
                <a onClick={() => handleDelete(record)}>删除</a>
            </Space>
        )
    }
    ]
    // 点击删除
    const handleDelete = (record) => {
        http.post('/api/recommendDelete', { id: record.key }).then(res => {
            console.log(res.data.data)
            setdata([...res.data.data1])
        })
    }

    // 点击编辑
    const showModal = (record) => {
        setIsModalVisible(true);
        setrecord(record);
        console.log(record)
    };
    // 渲染列表
    const getlist = () => {
        http.get('/api/advertising').then(res => {
            setdata([...res.data.list1])
            // console.log(res.data.list)

        })
    }

    // 模糊搜索
    const handleSearch = (e) => {
        setvalue(e.target.value)
        // console.log(value)
    }
    // 搜索序号
    const handleSearch1 = () => {
        http.post('/api/recommendsearch1', { value: value }).then(res => {
            setdata([...res.data.data1])
        })
    }
    // 搜索类型


    // 弹出框

    // console.log(data)
    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 16,
        },
    };
    const tailLayout = {
        wrapperCol: {
            offset: 8,
            span: 16,
        },
    };



    const onFinish = (values) => {
        console.log('Success:', values);
        setIsModalVisible(false);
        http.post('/api/recommendedit', {
            id: record.key, xuhao: values.xuhao, types: values.types, priority: values.priority

        }).then(res => {
            // console.log(res.data.data)

            // console.log(res.data.data)
            setdata([...res.data.data1])
        })
        // console.log(record)
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    const onOk = () => {
        form.submit()
        console.log(record.key)
        console.log(data)

    }

    const [form] = Form.useForm();
    useEffect(() => {
        form.setFieldsValue(record)
    }, [isModalVisible])
    // 复选
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        getCheckboxProps: (record) => ({
            disabled: record.name === 'Disabled User',
            // Column configuration not to be checked
            name: record.name,
        }),
    };
    return <div>
        <h3>
            <Card>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/home">首页</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/recommend">系统</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/recommend">模块管理</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/recommend">推荐位管理</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
            </Card>
        </h3>
        {/* 模糊搜索 */}
        <div className="search">
            <div className="search1">
                <span>广告序号：</span>
                <Input placeholder="请输入" onChange={handleSearch} />
                <Button icon={<SearchOutlined />} onClick={handleSearch1}></Button>

            </div>
            <div className="search2">
                <span>企业类型：</span>
                <Input placeholder="请输入" onChange={handleSearch} />
                <Button icon={<SearchOutlined />} onClick={handleSearch1}></Button>
            </div>
            <div className="search2">
                <span>优先级：</span>
                <Input placeholder="请输入" onChange={handleSearch} />
                <Button icon={<SearchOutlined />} onClick={handleSearch1}></Button>
            </div>
        </div>
        {/* 广告列表 */}
        <div className="advertisingList">
            <h4>广告位管理</h4>
            <Table rowSelection={{
                type: selectionType,
                ...rowSelection,
            }} dataSource={data} columns={columns} pagination={{ defaultPageSize: 5 }} />

        </div>
        {/* 弹出框 */}


        <div className="dialog">
            <Modal title="Basic Modal" visible={isModalVisible} onOk={onOk} onCancel={handleCancel}
                forceRender
            >
                <Form
                    form={form}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    {/* {JSON.stringify(record)} */}
                    <Form.Item
                        label="序号"
                        name="xuhao"
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="企业"
                        name="types"

                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="推荐优先级"
                        name="priority"

                    >
                        <Input />
                    </Form.Item>

                </Form>
            </Modal>
        </div>
    </div>
}

export default Recommend