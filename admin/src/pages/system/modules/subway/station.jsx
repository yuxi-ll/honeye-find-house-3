import React, { useState, useEffect } from 'react'
import { Card, Breadcrumb, Col, Button, Radio, Table, Input, Form, Divider, Space, Modal } from "antd"
import Select from "../../../../component/Select"
import { getArea, getCity, getCommunity, selectVal } from "../../../../api/index"
import axios from "axios"
const Station = () => {
    const [province, setProvince] = useState([])//获取省数据
    const [area, setArea] = useState([])//获取市数据
    const [commList, setCommList] = useState([])//获取区数据
    const [vals, setVals] = useState('')//搜索
    const [selectionType, setSelectionType] = useState('checkbox');
    const { Column, ColumnGroup } = Table;//表格
    const [station, setStation] = useState([])//获取表格数据
    const [isModalVisible, setIsModalVisible] = useState(false);//弹框
    const [form] = Form.useForm()
    const [isEdit, setIsEdit] = useState(false)//编辑框
    const [obj, setObj] = useState({ '标题': '', '区域': '', '排序': '', '状态': '' })
    const [listItem, setlistItem] = useState({})
    useEffect(() => {
        getCityList()//获取省数据
        getAreaList()//获取市数据
        getCommList()//获取区数据
        getStationList()//获取地铁管理列表数据
    }, [])
    //获取城市列表
    const getCityList = async () => {
        const { data: res } = await getCity()
        let province = res.body.map(item => item.label)
        province.unshift('请选择')
        setProvince([...province])
    }
    //获取市列表
    const getAreaList = async () => {
        const { data: res } = await getArea()
        let area = res.body.map(item => item.label)
        area.unshift('请选择')
        setArea([...area])
    }
    const getCommList = async () => {
        const { data: res } = await getCommunity()
        if (res.code === 0) {
            setCommList([...res.data])
        }
    }
    //获取地铁管理表格的数据
    const getStationList = async () => {
        const station = await axios("/api/station")
        setStation(set => [...station.data.list])
        console.log(station)
    }
    //选择器
    const selectValue = async (value) => {
        //console.log(value)
        if (value === '请选择') {
            getCommList()
        } else {
            const { data: res } = await selectVal({ value })
            setCommList([...res.data])
        }
    }

    //模糊搜索
    const changeVals = (e) => {
        setVals(e.target.value, vals)
    }
    //搜索
    const Search = () => {

    }
    //弹框
    const showModal = (record) => {
        setIsModalVisible(true);
        console.log(record)
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    //删除
    const handleDelete = (key) => {
        console.log(key)
        let list = station.filter(item => item.key !== key)
        setStation([...list])
    }
    //编辑
    const onFinish = (values) => {
        if (isEdit) {
            const formItem = form.getFieldValue();
            const Item = { ...listItem, ...formItem }
            console.log(Item)
            station.map((item) => {
                if (item.key === item.key) {
                    //item.key = Item.key
                    item.title = Item.title
                    item.area = Item.area
                    item.id = Item.id
                    item.handle = Item.handle
                }
            })
            console.log(station)
        } else {
            addItem()
        }
    }
    //添加
    const addItem = async () => {
        const formItem = form.getFieldValue()
        formItem.key = Math.random().toString().slice(3, 8)
        formItem.range = station.length + 1
        station.unshift(formItem)
        console.log(station)
        setStation([...station])
    }
    const onFinishFailed = () => {

    }
    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 16,
        },
    };

    const tailLayout = {
        wrapperCol: {
            offset: 8,
            span: 16,
        },
    };

    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        getCheckboxProps: (record) => ({
            disabled: record.name === 'Disabled User',
            // Column configuration not to be checked
            name: record.name,
        }),
    };
    return <div className="station">
        <div className="breadcrumb">
            <Card>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/home/subway">首页</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/station">系统</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
            </Card>
        </div>
        <div className="select">
            <Card>
                <Col className="gutter-row" span={4}>
                    <Select type={province} selectValue={selectValue}></Select>
                </Col>
                <Col className="gutter-row" span={4}>
                    <Select type={area} selectValue={selectValue}></Select>
                </Col>
                <Col className="gutter-row" span={4}>
                    <Select type={commList} selectValue={selectValue}></Select>
                </Col>
                <Col>
                    <input type="text" placeholder="请输入要查询的关键字" value={vals} onChange={changeVals} />
                    <button onClick={Search}>搜索</button>
                </Col>
            </Card>
        </div>
        <div className="handle">
            <Card>
                <Button

                    onClick={() => {
                        form.setFieldsValue({ key: '', title: '', area: '', id: '' })
                        setIsModalVisible(true)
                        setIsEdit(false)
                    }}

                >添加</Button>
                <Button>删除</Button>
            </Card>
            <div className="table">
                <Radio.Group
                    onChange={({ target: { value } }) => {
                        setSelectionType(value);
                    }}
                    value={selectionType}
                >
                    <Radio value="checkbox">Checkbox</Radio>
                    <Radio value="radio">radio</Radio>
                </Radio.Group>

                <Divider />

                <Table
                    rowSelection={{
                        type: selectionType,
                        ...rowSelection,
                    }}
                    dataSource={station}
                    pagination={{ defaultPageSize: 4 }}
                    style={{ textAlign: 'center' }}
                >

                    <Column title="序号" dataIndex="key" key="key" />
                    <Column title="地铁线" dataIndex="title" key="title" />
                    <Column title="区域" dataIndex="area" key="area" />
                    <Column title="排序" dataIndex="id" key="id" />
                    <Column title="状态" dataIndex="status" key="status" />
                    <Column title="操作" dataIndex="handle" key="handle" render={(text, record) => (
                        <Space size="middle">
                            <Button onClick={() => { handleDelete(record.key) }} style={{ borderRadius: 5 }}>删除</Button>
                            <Button onClick={() => { showModal(record) }} style={{ borderRadius: 5 }}>编辑</Button>
                            <Button onClick={() => { }} style={{ borderRadius: 5 }}>详情</Button>
                        </Space>
                    )} />
                </Table>
            </div>
        </div>
        <div className="modal">
            <Modal title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                <div className="form">
                    <Form
                        {...layout}
                        name="basic"
                        initialValues={{
                            remember: true,
                            key:obj.key,
                            title: obj.title,
                            area: obj.area,
                            status: obj.status,
                            id: obj.id
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        form={form}
                    >
                         <Form.Item
                            label="序号"
                            name="key"
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="地铁线"
                            name="title"
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="区域"
                            name="area"
                        >
                            <Input />

                        </Form.Item>

                        <Form.Item
                            label="排序"
                            name="id"
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="状态"
                            name="status"
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item {...tailLayout}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </Modal>
        </div>
    </div>
}

export default Station