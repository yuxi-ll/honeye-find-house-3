import React from 'react'
import { Breadcrumb, Card, Cascader, Form, Button, Modal, Input, Table, Radio, Divider, Space } from 'antd'
import { useState, useEffect } from "react"
import "../subway/index.css"
import axios from "axios"
const Subway = (props) => {
    const [list, setList] = useState([])//下拉列表数据
    const [data, getdata] = useState([])//table表格数据
    const [obj, setObj] = useState({ '序号': '', '标题': '', '区域': '', '排序': '',})//存放编辑后的数据
    const [vals, setVals] = useState('')//搜索
    const { Column, ColumnGroup } = Table;//表格
    const [isEdit, setIsEdit] = useState(false)//编辑框
    const [form] = Form.useForm()
    const [listItem, setlistItem] = useState({})
    const [selectionType, setSelectionType] = useState('checkbox');
    const [isModalVisible, setIsModalVisible] = useState(false);
    useEffect(() => {
        getMessage()
        getTableMessage()
    }, [])

    //下拉列表数据
    const getMessage = async () => {
        const list = await axios("/api/subway")
        setList(set => [...list.data.subway])
        console.log(list)
    }

    //table数据
    const getTableMessage = async () => {
        const data = await axios("/api/table")
        getdata(set => [...data.data.list])
        console.log(data)
    }

    function onChange(value) {
        console.log(value);
    }


    const showModal = (record) => {
        console.log(record)
        setIsModalVisible(true);
        form.setFieldsValue(record)
    };

    const handleOk = (row) => {
        console.log(row)
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    //编辑确认
    const onFinish = (values) => {
        if (isEdit) {
            const formItem = form.getFieldValue();
            const Item = { ...listItem, ...formItem }
            console.log(Item)
            data.map((item) => {
                if (item.key === Item.key) {
                    item.key = Item.key
                    item.title = Item.title
                    item.area = Item.area
                    item.id = Item.id
                    item.status = Item.status
                }
            })
        } else {
            addItem()
        }
        setIsModalVisible(false)
    }

    //编辑取消
    const onFinishFailed = () => {
        setIsModalVisible(false)
    }

    //删除
    const handleDelete = (key) => {
        let list = data.filter(item => item.key !== key)
        getdata([...list])
    }

    //模糊搜索
    const changeVals = (e) => {
        setVals(e.target.value, vals)
    }

    //搜索
    const Search = () => {
        const res = data.filter(item => item.title.includes(vals))
        getdata(res)
    }

    //添加
    const addItem = async () => {
        const formItem = form.getFieldValue()
        formItem.key = Math.random().toString().slice(3, 8)
        formItem.range = data.length + 1
        data.unshift(formItem)
        console.log(data)
        getdata([...data])
    }

    //跳转详情
    const handleTo = () => {
        props.history.push("/home/station")
    }

    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 16,
        },
    };

    const tailLayout = {
        wrapperCol: {
            offset: 8,
            span: 16,
        },
    };

    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        getCheckboxProps: (record) => ({
            disabled: record.name === 'Disabled User',
            // Column configuration not to be checked
            name: record.name,
        }),
    };

    return <div className="subway">
        <div className="breadcrumb">
            <Card>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/home/subway">首页</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
            </Card>
        </div>
        <div className="select">
            <Card>
                <span><Cascader options={list} onChange={onChange} placeholder="请选择省份" style={{ width: 170 }} /></span>
                <span><Cascader options={list} onChange={onChange} placeholder="请选择市区" style={{ width: 170 }} /></span>
                <span className="active">

                    <input type="text" style={{ height: 30 }} value={vals} onChange={changeVals} />
                    <Button onClick={Search}>搜索</Button>
                </span>
            </Card>
        </div>
        <div className="handle">
            <Button type="primary" onClick={() => {
                form.setFieldsValue({ key: '', title: '', area: '', id: '' })
                setIsModalVisible(true)
                setIsEdit(false)
            }}>
                添加
            </Button>
            <Button type="primary">
                删除
            </Button>
            <Modal title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                <div className="form">
                    <Form
                        {...layout}
                        name="basic"
                        initialValues={{
                            remember: true,
                            key: obj.key,
                            title: obj.title,
                            area: obj.area,
                            status: obj.status,
                            id: obj.id
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        form={form}
                    >

                        <Form.Item
                            label="序号"
                            name="key"
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="地铁线"
                            name="title"
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item
                            label="区域"
                            name="area"
                        >
                            <Input />

                        </Form.Item>

                        <Form.Item
                            label="排序"
                            name="id"
                        >
                            <Input />
                        </Form.Item>

                        <Form.Item {...tailLayout}>
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
            </Modal>
        </div>
        <div className="table">
            <Radio.Group
                onChange={({ target: { value } }) => {
                    setSelectionType(value);
                }}
                value={selectionType}
            >
                <Radio value="checkbox">Checkbox</Radio>
                <Radio value="radio">radio</Radio>
            </Radio.Group>

            <Divider />

            <Table
                rowSelection={{
                    type: selectionType,
                    ...rowSelection,
                }}
                dataSource={data}
                pagination={{ defaultPageSize: 4 }}
                style={{ textAlign: 'center' }}
            >

                <Column title="序号" dataIndex="key" key="key" />
                <Column title="地铁线" dataIndex="title" key="title" />
                <Column title="区域" dataIndex="area" key="area" />
                <Column title="排序" dataIndex="id" key="id" />
                <Column title="状态" dataIndex="status" key="status" />
                <Column title="操作" dataIndex="handle" key="handle" render={(text, record) => (
                    <Space size="middle">
                        <Button onClick={() => { handleDelete(record.key) }} style={{ borderRadius: 5 }}>删除</Button>
                        <Button onClick={() => { showModal(record) }} style={{ borderRadius: 5 }}>编辑</Button>
                        <Button onClick={() => { handleTo() }} style={{ borderRadius: 5 }}>详情</Button>
                    </Space>
                )} />
            </Table>
        </div>
    </div>
}
export default Subway