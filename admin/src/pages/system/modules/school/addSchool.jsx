import React from 'react'
import { Breadcrumb, Card, Cascader, Radio, Upload, message, Button } from 'antd';
import { useState, useEffect } from "react"
import { UploadOutlined } from '@ant-design/icons';
import "../school/index.css"
import axios from '../../../../util/http';
const AddSchool = () => {
    const [state, getState] = useState([])//省份数据
    useEffect(() => {
        getMessage()
    }, [])
    const getMessage = async () => {
        const state = await axios("/api/addschool")
        getState(set => [...state.data.province])
        console.log(state)
    }
    const [value, setValue] = React.useState(1);

    const onchange = e => {
        console.log('radio checked', e.target.value);
        setValue(e.target.value);
    };

    function onChange(value) {
        console.log(value);
    }
    const props = {
        name: 'file',
        action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
        headers: {
            authorization: 'authorization-text',
        },
        onChange(info) {
            if (info.file.status !== 'uploading') {
                console.log(info.file, info.fileList);
            }
            if (info.file.status === 'done') {
                message.success(`${info.file.name} file uploaded successfully`);
            } else if (info.file.status === 'error') {
                message.error(`${info.file.name} file upload failed.`);
            }
        },
    };
    return <div className="addschool">
        <div className="breadcrumb">
            <Card>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/home/school">首页</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/addschool">系统</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
            </Card>
        </div>
        <div className="content">
            <Card className="card">
                <p>
                    <span >所属地区:</span>
                    <span style={{marginLeft:10}}><Cascader options={state} onChange={onChange} placeholder="请选择省份" style={{ width: 140 }} /></span>
                    <span><Cascader options={state} onChange={onChange} placeholder="请选择城市" style={{ width: 140 }} /></span>
                    <span><Cascader options={state} onChange={onChange} placeholder="请选择区县" style={{ width: 140 }} /></span>
                </p>
                <p>
                    <span>学校名称:</span>
                    <span><input type="text" placeholder="请输入学校名称" style={{ width: 320, marginLeft: 10 }} /></span>
                </p>
                <p>
                    <span>学校地址:</span>
                    <span><input type="text" placeholder="请输入学校地址" style={{ width: 320, marginLeft: 10 }} /></span>
                </p>
                <p>
                    <span>学校性质:</span>
                    <span><input type="text" placeholder="请输入学校性质" style={{ width: 320, marginLeft: 10 }} /></span>
                </p>
                <p>
                    <span>坐标位置:</span>
                    <span><input type="text" placeholder="请输入坐标位置" style={{ width: 240, marginLeft: 10 }} /></span>
                    <span><button>标注位置</button></span>
                </p>
                <div style={{ marginLeft: 10 }} className="check">
                    <span>类型:</span>
                    <Radio.Group onChange={onchange} value={value}>
                        <Radio value={1}>幼儿园</Radio>
                        <Radio value={2}>小学</Radio>
                        <Radio value={3}>初中</Radio>
                        <Radio value={4}>高中</Radio>
                    </Radio.Group>
                </div>
                <div className="upload">
                    <span>缩略图:</span>
                    <Upload {...props}>
                        <Button>上传图片</Button>
                    </Upload>
                </div>
                <div className="introduce">
                    <span>介绍:</span>
                    <textarea name="" id="" cols="40" rows="7"></textarea>
                </div>
                <div className="submit">
                    <Button>提交</Button>
                </div>
            </Card>
        </div>
    </div>
}

export default AddSchool