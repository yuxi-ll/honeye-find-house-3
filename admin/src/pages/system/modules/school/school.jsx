import React from 'react'
import { Breadcrumb, Cascader, Table, Space, Modal, Button, Form, Input, Checkbox, Card } from 'antd';
import { useState, useEffect } from "react"
import axios from "axios"
import "../school/index.css"
const School = (props) => {
    const [state, getstate] = useState([])//学校列表的数据
    const { Column } = Table;
    const [selectionType] = useState('checkbox');//复选框
    const [obj] = useState({ key: '', name: '', area: '' })//定义表格的空数组
    const [form] = Form.useForm()
    const [input, setinput] = useState([])//模糊搜索
    const [isModalVisible, setIsModalVisible] = useState(false);

    useEffect(() => {
        getProvince()
    }, [])
    //获取数据
    const getProvince = async () => {
        const state = await axios("/api/school")
        getstate(set => [...state.data.list])
        console.log(state)
    }
   
    const showModal = (record) => {
        console.log(record)
        setIsModalVisible(true)
        form.setFieldsValue(record);
    };

    const handleOk = () => {
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 16,
        },
    };
    const tailLayout = {
        wrapperCol: {
            offset: 8,
            span: 16,
        },
    };
    //from表单确认框并且更改数据
    const onFinish = (values) => {
        console.log(values)
        state.map((item) => {
            if (item.key === values.key) {
                item.key = values.key;
                item.name = values.name;
                item.area = values.area;
            }
        })
        getstate([...state])
    };
    //模糊搜索
    const handleSearch = (e) => {
        setinput(e.target.value, input)
    }
    //模糊搜索
    const Search = () => {
        const res = state.filter(item => item.name.includes(input) || item.area.includes(input))
        getstate(res)
        console.log(res)
    }
    const onFinishFailed = (values) => {
        console.log(values)
    };
    //表格删除
    const handleDelete = (key) => {
        let list = state.filter(item => item.key !== key)
        getstate([...list])
    }
    //跳转到添加学校
    const handleTo = () => {
        props.history.push("/home/addschool")
    }
    //省数据
    const province = [
        {
            value: '上海市',
            label: '上海市',
        },
        {
            value: '河南省',
            label: '河南省',
        },
        {
            value: '河北省',
            label: '河北省',
        },
        {
            value: '山西省',
            label: '山西省',
        },
        {
            value: '四川省',
            label: '四川省',
        },
        {
            value: '山东省',
            label: '山东省',
        },
        {
            value: '湖南省',
            label: '湖南省',
        },
        {
            value: '湖北省',
            label: '湖北省',
        }
    ];
    //市数据
    const city = [
        {
            value: '唐山市',
            label: '唐山市',
        },
        {
            value: '廊坊市',
            label: '廊坊市',
        },
        {
            value: '焦作市',
            label: '焦作市',
        },
        {
            value: '保定市',
            label: '保定市',
        },
        {
            value: '邯郸市',
            label: '邯郸市',
        },
        {
            value: '太原市',
            label: '太原市',
        },
        {
            value: '郑州市',
            label: '郑州市',
        },
    ];

    function onChange(value) {
        console.log(value);
    }

    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        getCheckboxProps: (record) => ({
            disabled: record.name === 'Disabled User',
            // Column configuration not to be checked
            name: record.name,
        }),
    };
    return <div className="school">

        <div className="breadcrumb">
            <Card>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/school/school">首页</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
            </Card>
        </div>
        <div className="select">
            <div className="one">
                <Cascader options={province} onChange={onChange} placeholder="请选择省份" />
            </div>
            <div className="two">
                <Cascader options={city} onChange={onChange} placeholder="请选择市区" />
            </div>
            <div className="three">
                <Card>
                    <div className="input" style={{width:300}}>
                        <input type="text" values={input} onChange={handleSearch} />
                    </div>
                    <div className="btn">
                        <button onClick={Search} >搜索</button>
                    </div>
                </Card>
            </div>
        </div>
        <div className="form">
            {/* type	多选/单选	checkbox | radio	checkbox	 */}
            <div className="modal">
                <Button type="primary" onClick={() => { handleTo() }}>
                    添加
                </Button>
                <Button type="primary">
                    删除
                </Button>
                <Modal title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                    <Form
                        {...layout}
                        name="basic"
                        initialValues={{
                            remember: true,
                            key: obj.key,
                            name: obj.name,
                            area: obj.area
                        }}
                        onFinish={onFinish}
                        onFinishFailed={onFinishFailed}
                        form={form}
                    >
                        <Form.Item
                            label="key"
                            name="key"
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="name"
                            name="name"
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="area"
                            name="area"
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item {...tailLayout}>
                            <Button type="primary" htmlType="submit">
                                submit
                            </Button>
                        </Form.Item>
                    </Form>
                </Modal>
            </div>
            <div className="table">
                <Table dataSource={state} rowSelection={{ type: selectionType, ...rowSelection }} pagination={{ defaultPageSize: 4 }}>
                    <Column title="序号" dataIndex="key" key="key" />
                    <Column title="学校名称" dataIndex="name" key="name" />
                    <Column title="区域" dataIndex="area" key="area" />
                    <Column
                        title="Action"
                        key="action"
                        render={(text, record) => (
                            <Space size="middle">
                                <a onClick={() => { showModal(record) }} style={{ borderRadius: 5 }}>编辑</a>
                                <a onClick={() => { handleDelete(record.key) }} style={{ borderRadius: 5 }}>删除</a>
                            </Space>
                        )}
                    />
                </Table>
            </div>
        </div>
    </div>
}

export default School