import React, { useState, useEffect } from "react"
import axios from "axios"
import "../area/index.css"
import { Table, Tag, Space, Button, Modal, Form, Input, Breadcrumb, Card } from 'antd';
const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};

const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  },
};

const Area = (props) => {
  const { Column, ColumnGroup } = Table;
  const [form] = Form.useForm()
  const [obj, setObj] = useState({ firstName: '', lastName: '', age: '', address: '' })
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [Data, setData] = useState([])

  useEffect(() => {
    handleUserInfo()
  }, [])
  const handleUserInfo = async () => {
    const Data = await axios("/api/area")
    setData(set => [...Data.data.list])
    console.log(Data)
  }
  // 删除
  const handleDelete = (key) => {
    let list = Data.filter(item => item.key !== key)
    setData([...list])
  }

  //编辑确认
  const onFinish = (values) => {
    //console.log('Success:', values);
    Data.map((item) => {
      if (item.key === values.key) {
        item.key = values.key
        item.title = values.title
        item.qyid = values.qyid
        item.jyid = values.jyid
      }
    })
    setData([...Data])
  };
  //编辑取消
  const onFinishFailed = () => {
    setIsModalVisible(false)
  };

  const showModal = (record) => {
    console.log(record)
    setIsModalVisible(true);
    form.setFieldsValue(record)
  };

  const handleOk = (row) => {
    console.log(row)
    setIsModalVisible(false);
  };

  const handleCancel = (row) => {
    setIsModalVisible(false);
  };

  const handleTo = () => {
    props.history.push("/home/city")
  }
  return (
    <div className="area">

      {/* 面包屑 */}
      <div className="breadcrumb">
        <Card>
          <Breadcrumb>
            <Breadcrumb.Item>
              <a href="/home/area">首页</a>
            </Breadcrumb.Item>
          </Breadcrumb>
        </Card>
      </div>

      {/* 表格 */}
      <div className="table">
        <Table dataSource={Data} pagination={{ defaultPageSize: 3 }}>

          <Column title="序号" dataIndex="key" key="key" />
          <Column title="省份" dataIndex="title" key="title" />

          <Column title="启用城市" dataIndex="qyid" key="qyid" />
          <Column title="禁用城市" dataIndex="jyid" key="jyid" />
          <Column
            title="操作"
            key="action"
            render={(text, record) => (
              <Space size="middle">
                <Button onClick={() => { handleDelete(record.key) }}>删除</Button>
                <Button onClick={() => { showModal(record) }}>编辑</Button>
                <Button onClick={() => { handleTo() }}>详情</Button>
              </Space>
            )}
          />
        </Table>
      </div>

      {/* 弹框 */}
      <div className="modal">
        <Modal title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
          {/* form表单 */}
          <div className="form">
            <Form
              {...layout}
              name="basic"
              initialValues={{
                remember: true,
                key: obj.key,
                title: obj.title,
                qyid: obj.qyid,
                jyid: obj.jyid
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              form={form}
            >

              <Form.Item
                label="key"
                name="key"
              >
                <Input />
              </Form.Item>
              <Form.Item
                label="title"
                name="title"
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="qyid"
                name="qyid"
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="jyid"
                name="jyid"
              >
                <Input />
              </Form.Item>

              <Form.Item {...tailLayout}>
                <Button type="primary" htmlType="submit">
                  Submit
              </Button>
              </Form.Item>
            </Form>
          </div>
        </Modal>
      </div>
    </div>
  )
}
export default Area