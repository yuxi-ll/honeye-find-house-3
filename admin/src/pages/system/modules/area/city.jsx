import React from 'react'
import { Breadcrumb, Cascader, Table, Tag, Space, Card } from 'antd';
import { useState, useEffect } from "react"
import axios from "axios"
import "../area/index.css"
const City = () => {
    const [list, setList] = useState([])//定义空数组，存放城市数据
    const { Column, ColumnGroup } = Table;
    useEffect(() => {
        handleUserInfo()
    }, [])
    //获取城市数据
    const handleUserInfo = async () => {
        const list = await axios("/api/city")
        setList(set => [...list.data.list]);
        console.log(list)
    }
    return <div className="city">
        <div className="breadcrumb">
            <Card>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/home/area">首页</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/city">城市</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
            </Card>
        </div>
        <div className="form">
            <Table dataSource={list} pagination={{ defaultPageSize: 3 }}>
                <Column title="序号" dataIndex="key" key="key" />
                <Column title="城市" dataIndex="name" key="name" />
                <Column title="区域" dataIndex="title" key="title" />
                <Column title="状态" dataIndex="zt" key="zt" />
            </Table>
        </div>
    </div>
}

export default City