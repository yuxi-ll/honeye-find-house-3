import React, { useState } from 'react';
import { Form, Input, Select, Checkbox, Button, message } from 'antd';
import { register } from '../../api/index'
import Verification from './verification'
import ParticlesBg from 'particles-bg'
import "./register.css"
let config = {
    num: [4, 7],
    rps: 0.1,
    radius: [5, 40],
    life: [1.5, 3],
    v: [2, 3],
    tha: [-50, 50],
    alpha: [0.6, 0],
    scale: [.1, 0.9],
    position: "all",
    //color: ["random", "#ff0000"],
    cross: "dead",
    random: 10
};
const { Option } = Select;
const Register = (props) => {
    const [form] = Form.useForm();
    const { history } = props
    const [send, setSend] = useState(false)
    const [code, setCode] = useState('')
    const [count, setCount] = useState(0)
    const [num, setNum] = useState(3)
    const [down, setDown] = useState(false)
    const onFinish = async (values) => {
        if (code.toLocaleLowerCase() === values.verification.toLocaleLowerCase()) {
            const { data: res } = await register({ ...values })
            if (res.code === 0) {
                message.success(res.msg)
                history.push('/login')
            } else {
                message.error(res.msg)
            }
        } else {
            message.error('验证码错误')
        }

    };
    const prefixSelector = (
        <Form.Item name="prefix" noStyle>
            <Select
                style={{
                    width: 80,
                }}
            >
                <Option value="86">+86</Option>
                <Option value="87">+87</Option>
            </Select>
        </Form.Item>
    );
    const tailFormItemLayout = {
        wrapperCol: {
            xs: {
                span: 24,
                offset: 0,
            },
            sm: {
                span: 24,
                offset: 8,
            },
        },
    };
    const Send = () => {
        setSend(!send)
    }
    const reloadPic = (code) => {
        let num = 3;
        setCode(code)
        setCount(count + 1)
        if (count >= 3) {
            message.warn('操作频率过高,请稍后')
            setCount(0)
            setSend(false)
            setDown(true)
            let timer = setInterval(() => {
                num -= 1
                setNum(num)
                if (num === 0) {
                    setDown(false)
                    setNum(3)
                    clearInterval(timer)
                }
            }, 1000)
        }
    }
    return <div className="registerLay">
        <ParticlesBg type="custom" config={config} bg={true} />
        <div className="register">
            <Form
                form={form}
                name="register"
                onFinish={onFinish}
                initialValues={{
                    residence: ['zhejiang', 'hangzhou', 'xihu'],
                    prefix: '86',
                }}
                scrollToFirstError
            >
                <Form.Item
                    name="password"
                    label="密码"
                    rules={[
                        {
                            required: true,
                            message: '请输入您的密码',
                        },
                    ]}
                    hasFeedback
                >
                    <Input.Password />
                </Form.Item>
                <Form.Item
                    name="confirm"
                    label="确认密码"
                    dependencies={['password']}
                    hasFeedback
                    rules={[
                        {
                            required: true,
                            message: '请确认您的密码',
                        },
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                                if (!value || getFieldValue('password') === value) {
                                    return Promise.resolve();
                                }

                                return Promise.reject(new Error('两次密码不一致'));
                            },
                        }),
                    ]}
                >
                    <Input.Password />
                </Form.Item>
                <Form.Item
                    name="phone"
                    label="手机号"
                    rules={[
                        {
                            required: true,
                            message: '请输入您的手机号',
                        },
                    ]}
                >
                    <Input
                        addonBefore={prefixSelector}
                        style={{
                            width: '100%',
                        }}
                    />
                </Form.Item>
                <div className="verify">
                    <Form.Item
                        name="verification"
                        label="验证码"
                        rules={[
                            {
                                required: true,
                                message: '请输入验证码',
                            },
                        ]}
                    >
                        <Input
                            style={{
                                width: '100%',
                                height: '31px'
                            }}
                        />
                    </Form.Item>
                    {send ? <Verification reloadPic={reloadPic}></Verification> : <Button onClick={Send}>{down ? num + 's' : '获取验证码'}</Button>}
                </div>
                <Form.Item
                    name="username"
                    label="用户名"
                    tooltip="您想要别人怎么称呼您"
                    rules={[
                        {
                            required: true,
                            message: '请输入您的昵称',
                            whitespace: true,
                        },
                    ]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    name="agreement"
                    valuePropName="checked"
                    rules={[
                        {
                            validator: (_, value) =>
                                value ? Promise.resolve() : Promise.reject(new Error('Should accept agreement')),
                        },
                    ]}
                    {...tailFormItemLayout}
                >
                    <Checkbox>
                        我已阅读 <a href="">agreement</a>
                    </Checkbox>
                </Form.Item>
                <Form.Item {...tailFormItemLayout}>
                    <Button type="primary" onClick={() => history.push('/login')} style={{margin: '0 5px'}}>
                        登录
                    </Button>
                    <Button type="primary" htmlType="submit">
                        注册
                    </Button>
                </Form.Item>
            </Form>
        </div>
    </div >
}

export default Register