import React, { useState, useEffect } from 'react'
import { Breadcrumb, Card, Table, Button, Col, Row, Input, Switch, Modal, Form, Popconfirm, message } from 'antd'
import Select from '../../../component/Select'
import { getappointment, deleteapp, deleteSeleapp, selectValapp, changeSwitchapp, updateapp } from '../../../api/index'
const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
};
const Appointment = () => {
    const [appList, setappList] = useState([])
    const [type, setType] = useState(['请选择', '新房', '二手房', '出租房', '写字楼出售', '写字楼出租', '商品出售', '商铺出租'])
    const [methods, setMethods] = useState(['请选择', '团购报名', '领取红包', '预约看房'])
    const [selectionType, setSelectionType] = useState('checkbox');
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [isEdit, setIsEdit] = useState(false)
    const [form] = Form.useForm();
    const [record, setRecord] = useState({})
    const [selection, setSelection] = useState([])
    useEffect(() => {
        getappointmentList()
    }, [])
    const columns = [
        {
            title: '联系人',
            dataIndex: 'name',
            width: 100,
        },
        {
            title: '联系电话',
            dataIndex: 'phone',
            width: 100,
        },
        {
            title: '房源名称',
            dataIndex: 'house',
            width: 100,
        },
        {
            title: '房源类型',
            dataIndex: 'type',
            width: 100,
        },
        {
            title: '预约方式',
            dataIndex: 'methods',
            width: 100,
        },
        {
            title: '经纪人',
            width: 100,
            dataIndex: 'agent',
        },
        {
            title: '提交时间',
            dataIndex: 'last',
            width: 100,
            defaultSortOrder: 'descend',
            sorter: (a, b) => new Date(a.last).getTime() - new Date(b.last).getTime(),
        },
        {
            title: '状态',
            dataIndex: 'isOpen',
            width: 100,
            render: (text, record) => {
                return <Switch checked={record.state === '开启' ? true : false} onChange={(e) => onChangeSwitch(e, record)} />
            }
        }, {
            title: '操作',
            dataIndex: 'action',
            width: 180,
            fixed: 'right',
            render: (text, record) => {
                return <div>
                    <Button type='text' onClick={() => edit(record)}>分配经纪人</Button>
                    <Popconfirm
                        title="确定删除吗"
                        onConfirm={() => confirm(record)}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                    >
                        <Button type='text'>删除</Button>
                    </Popconfirm>
                </div>
            }
        }
    ];
    //复选框
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelection([...selectedRowKeys])
        },
    };
    //批量删除
    const confirmSelect = async () => {
        const { data: res } = await deleteSeleapp({ selection })
        if (res.code === 0) {
            message.success(res.msg)
            console.log(res.data)
            setappList([...res.data])
        }
    }
    //获取小区列表
    const getappointmentList = async () => {
        const { data: res } = await getappointment()
        if (res.code === 0) {
            message.success(res.msg)
            setappList([...res.data])
        }
    }
    //开关
    const onChangeSwitch = async (checked, record) => {
        const { data: res } = await changeSwitchapp({ state: checked, record })
        if (res.code === 0) {
            message.success(res.msg)
            setappList([...res.data])
        }
    }
    //编辑
    const edit = async (record) => {
        form.setFieldsValue(record)
        setIsModalVisible(true)
        setIsEdit(true)
        setRecord({ ...record })
    }
    //点击确认
    const handleOk = () => {
        if (isEdit) {
            editItem()
        }
        setIsModalVisible(false);
    };
    //编辑
    const editItem = async () => {
        const formItem = { ...record, ...form.getFieldValue() }
        const { data: res } = await updateapp({ ...formItem })
        if (res.code === 0) {
            message.success(res.msg)
            setappList([...res.data])
        }
    }
    //点击取消
    const handleCancel = () => {
        setIsModalVisible(false);
    };
    //确认删除
    const confirm = (record) => {
        deleteItem(record)
        message.success('删除成功');
    }
    //取消删除
    const cancel = (e) => {
        message.error('取消成功');
    }
    //删除
    const deleteItem = async (record) => {
        const { data: res } = await deleteapp({ id: record.key })
        setappList([...res.data])
    }
    //选择器
    const selectValue = async (value) => {
        console.log(value)
        if (value === '请选择') {
            getappointmentList()
        } else {
            const { data: res } = await selectValapp({ value })
            if (res.code === 0) {
                message.success(res.msg)
                setappList([...res.data])
            } else {
                message.error(res.msg)
            }
        }
    }
    return <div className="appunity">
        <Card>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/home">首页</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/appunity">用户</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/appunity">预约管理</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/appunity">预约列表</a>
                </Breadcrumb.Item>
            </Breadcrumb>
        </Card>
        <Row gutter={16}>
            <Col className="gutter-row" span={4}>
                <Select type={type} selectValue={selectValue}></Select>
            </Col>
            <Col className="gutter-row" span={4}>
                <Select type={methods} selectValue={selectValue}></Select>
            </Col>
            <Col className="gutter-row" span={4}>
                <Popconfirm
                    title="确定删除吗"
                    onConfirm={confirmSelect}
                    onCancel={cancel}
                    okText="Yes"
                    cancelText="No"
                >
                    <Button type='primary' style={{ width: '100%', margin: '10px 0' }}>删除</Button>
                </Popconfirm>
            </Col>
        </Row>
        <Card style={{ width: '100%', height: '100%' }}>
            <Table
                rowSelection={{
                    type: selectionType,
                    ...rowSelection,
                }}
                scroll={{ x: 1300 }}
                columns={columns}
                dataSource={appList}
                pagination={{
                    defaultPageSize: 5,
                    defaultCurrent: 1
                }}
            />
        </Card>
        <Modal title={isEdit ? "编辑内容" : "添加信息"} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
            <Form
                {...layout}
                form={form}
                name="basic"
                initialValues={{ remember: true }}
            >
                <Form.Item
                    label="经纪人姓名"
                    name="agent"
                    rules={[{ required: true, message: '请输入经纪人名称' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="房源名称"
                    name="house"
                    rules={[{ required: true, message: '请输入房源名称' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="房源类型"
                    name="type"
                    rules={[{ required: true, message: '请输入房源类型' }]}
                >
                    <Input />
                </Form.Item>
            </Form>
        </Modal>
    </div >
}
export default Appointment
