import React, { useState, useEffect } from 'react'
import { Breadcrumb, Switch, Card, Table, Button, Col, Row, Input, Modal, Form, Popconfirm, message } from 'antd'
import { gethousereview, deletehousereview, deleteSelehousereview, changeSwitchhousereview } from '../../../api/index'
const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
};
const HouseReview = () => {
    const [housereviewList, sethousereviewList] = useState([])
    const [selectionType, setSelectionType] = useState('checkbox');
    const [selection, setSelection] = useState([])
    const columns = [
        {
            title: '用户名',
            dataIndex: 'name',
            width: 120,
        },
        {
            title: '楼盘',
            dataIndex: 'house',
            width: 120,
        },
        {
            title: '评论时间',
            dataIndex: 'last',
            width: 120,
        },
        {
            title: '评论内容',
            dataIndex: 'content',
            width: 120,
        },
        {
            title: '状态',
            dataIndex: 'state',
            width: 120,
            render: (state, record) => {
                return <Switch checked={state === '开启' ? true : false} onChange={(e) => onChangeSwitch(e, record)} />
            }
        },
        {
            title: '操作',
            dataIndex: 'action',
            width: 180,
            render: (text, record) => {
                return <div>
                    <Popconfirm
                        title="确定删除吗"
                        onConfirm={() => confirm(record)}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                    >
                        <Button type='error'>删除</Button>
                    </Popconfirm>
                </div>
            }
        }
    ];
    useEffect(() => {
        gethousereviewList()
    }, [])
    //获取经纪人点评列表
    const gethousereviewList = async () => {
        const { data: res } = await gethousereview()
        if (res.code === 0) {
            message.success(res.msg)
            sethousereviewList([...res.data])
        }
    }
    //批量删除
    const confirmSelect = async () => {
        const { data: res } = await deleteSelehousereview({ selection })
        if (res.code === 0) {
            message.success(res.msg)
            console.log(res.data)
            sethousereviewList([...res.data])
        }
    }
    //确认删除
    const confirm = (record) => {
        deleteItem(record)
        message.success('删除成功');
    }
    //取消删除
    const cancel = (e) => {
        message.error('取消成功');
    }
    //删除
    const deleteItem = async (record) => {
        const { data: res } = await deletehousereview({ id: record.key })
        sethousereviewList([...res.data])
    }
    //复选框
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelection([...selectedRowKeys])
        },
    };
    //开关
    const onChangeSwitch = async (checked, record) => {
        const { data: res } = await changeSwitchhousereview({ state: checked, record })
        if (res.code === 0) {
            message.success(res.msg)
            sethousereviewList([...res.data])
        }
    }
    return <div className="houseReview">
        <Card>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/home">首页</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/houseReview">用户</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/houseReview">评论管理</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/houseReview">楼盘点评</a>
                </Breadcrumb.Item>
            </Breadcrumb>
        </Card>
        <Row gutter={16}>
            <Col className="gutter-row" span={4}>
                <Popconfirm
                    title="确定删除吗"
                    onConfirm={confirmSelect}
                    onCancel={cancel}
                    okText="Yes"
                    cancelText="No"
                >
                    <Button type='primary' style={{ width: '100%', margin: '10px 0' }}>删除</Button>
                </Popconfirm>
            </Col>
        </Row>
        <Card style={{ width: '100%', height: '100%' }}>
            <Table
                rowSelection={{
                    type: selectionType,
                    ...rowSelection,
                }}
                columns={columns}
                dataSource={housereviewList}
                pagination={{
                    defaultPageSize: 5,
                    defaultCurrent: 1
                }}
            />
        </Card>
    </div >
}
export default HouseReview
