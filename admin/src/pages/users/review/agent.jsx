import React, { useState, useEffect } from 'react'
import { Breadcrumb, Switch, Card, Table, Button, Col, Row, Input, Modal, Form, Popconfirm, message } from 'antd'
import { getagent, deleteagent, deleteSeleagent, changeSwitchagent, selectValagent } from '../../../api/index'
import Select from '../../../component/Select'

const Agent = () => {
    const [agentList, setagentList] = useState([])
    const [state, setState] = useState(['请选择', '开启', '禁用'])
    const [selectionType, setSelectionType] = useState('checkbox');
    const [selection, setSelection] = useState([])
    const columns = [
        {
            title: '评论人',
            dataIndex: 'reviewer',
            width: 120,
        },
        {
            title: '经纪人',
            dataIndex: 'name',
            width: 120,
        },
        {
            title: '评论时间',
            dataIndex: 'last',
            width: 120,
        },
        {
            title: '问答内容',
            dataIndex: 'content',
            width: 120,
        },
        {
            title: '状态',
            dataIndex: 'state',
            width: 120,
            render: (state, record) => {
                return <Switch checked={state === '开启' ? true : false} onChange={(e) => onChangeSwitch(e, record)} />
            }
        },
        {
            title: '操作',
            dataIndex: 'action',
            width: 180,
            render: (text, record) => {
                return <div>
                    <Popconfirm
                        title="确定删除吗"
                        onConfirm={() => confirm(record)}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                    >
                        <Button type='error'>删除</Button>
                    </Popconfirm>
                </div>
            }
        }
    ];
    useEffect(() => {
        getagentList()
    }, [])
    //获取经纪人点评列表
    const getagentList = async () => {
        const { data: res } = await getagent()
        if (res.code === 0) {
            message.success(res.msg)
            setagentList([...res.data])
        }
    }
    //批量删除
    const confirmSelect = async () => {
        const { data: res } = await deleteSeleagent({ selection })
        if (res.code === 0) {
            message.success(res.msg)
            console.log(res.data)
            setagentList([...res.data])
        }
    }
    //确认删除
    const confirm = (record) => {
        deleteItem(record)
        message.success('删除成功');
    }
    //取消删除
    const cancel = (e) => {
        message.error('取消成功');
    }
    //删除
    const deleteItem = async (record) => {
        const { data: res } = await deleteagent({ id: record.key })
        setagentList([...res.data])
    }
    //复选框
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelection([...selectedRowKeys])
        },
    };
    //开关
    const onChangeSwitch = async (checked, record) => {
        const { data: res } = await changeSwitchagent({ state: checked, record })
        if (res.code === 0) {
            message.success(res.msg)
            setagentList([...res.data])
        }
    }
    //选择器
    const selectValueagent = async (value) => {
        if (value === '请选择') {
            getagentList()
        } else {
            const { data: res } = await selectValagent({ value })
            if (res.code === 0) {
                message.success(res.msg)
                setagentList([...res.data])
            } else {
                message.error(res.msg)
            }
        }
    }
    return <div className="community">
        <Card>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/home">首页</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/agent">用户</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/agent">评论管理</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/agent">经纪人点评</a>
                </Breadcrumb.Item>
            </Breadcrumb>
        </Card>
        <Row gutter={16}>
            <Col className="gutter-row" span={4}>
                <Select type={state} selectValue={selectValueagent}></Select>
            </Col>
            <Col className="gutter-row" span={4}>
                <Button type='primary' style={{ width: '100%', margin: '10px 0' }} onClick={() => { getagentList(); }}>重置</Button>
            </Col>
            <Col className="gutter-row" span={4}>
                <Popconfirm
                    title="确定删除吗"
                    onConfirm={confirmSelect}
                    onCancel={cancel}
                    okText="Yes"
                    cancelText="No"
                >
                    <Button type='primary' style={{ width: '100%', margin: '10px 0' }}>删除</Button>
                </Popconfirm>
            </Col>
        </Row>
        <Card style={{ width: '100%', height: '100%' }}>
            <Table
                rowSelection={{
                    type: selectionType,
                    ...rowSelection,
                }}
                columns={columns}
                dataSource={agentList}
                pagination={{
                    defaultPageSize: 5,
                    defaultCurrent: 1
                }}
            />
        </Card>
    </div >
}
export default Agent
