import React, { useState, useEffect } from 'react'
import { Breadcrumb, Switch, Card, Table, Button, Col, Row, Input, Modal, Form, Popconfirm, message } from 'antd'
import { getMember, deletemember, deleteSelemember, updatemember, addmember, changeSwitchUsers, searchValuemember, selectValmember } from '../../../../api/index'
import Select from '../../../../component/Select'
import moment from 'moment'
const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
};
const Member = () => {
    const [memberList, setmemberList] = useState([])
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [state, setState] = useState(['请选择', '开启', '禁用'])
    const [type, setType] = useState(['请选择', '普通会员','普通经纪人','中级经纪人','高级经纪人'])
    const [isEdit, setIsEdit] = useState(false)
    const [form] = Form.useForm();
    const [selectionType, setSelectionType] = useState('checkbox');
    const [selection, setSelection] = useState([])
    const [seaValue, setSeaValue] = useState('')
    const [record, setRecord] = useState({})

    useEffect(() => {
        getmemberList()
    }, [])
    const columns = [
        {
            title: '会员类别',
            dataIndex: 'type',
            width: 120,
        },
        {
            title: '用户名',
            dataIndex: 'username',
            width: 120,
        },
        {
            title: '昵称',
            dataIndex: 'nickname',
            width: 120,
        },
        {
            title: '手机号',
            dataIndex: 'username',
            width: 120,
        },
        {
            title: '注册时间',
            dataIndex: 'last',
            width: 120,
        },
        {
            title: '状态',
            dataIndex: 'state',
            width: 120,
            render: (state, record) => {
                return <Switch checked={state === '开启' ? true : false} onChange={(e) => onChangeSwitch(e, record)} />
            }
        },
        {
            title: '操作',
            dataIndex: 'action',
            width: 180,
            render: (text, record) => {
                return <div>
                    <Button type='primary' onClick={() => edit(record)}>编辑</Button>
                    <Popconfirm
                        title="确定删除吗"
                        onConfirm={() => confirm(record)}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                    >
                        <Button type='error'>删除</Button>
                    </Popconfirm>
                </div>
            }
        }
    ];
    //获取会员列表
    const getmemberList = async () => {
        const { data: res } = await getMember()
        if (res.code === 0) {
            message.success(res.msg)
            setmemberList([...res.data])
        }
    }
    //点击确认
    const handleOk = () => {
        if (isEdit) {
            editItem()
        } else {
            addItem()
        }
        setIsModalVisible(false);
    };
    //编辑
    const edit = async (record) => {
        form.setFieldsValue(record)
        setIsModalVisible(true)
        setIsEdit(true)
        setRecord({ ...record })
    }
    //编辑
    const editItem = async () => {
        const formItem = { ...record, ...form.getFieldValue() }
        const { data: res } = await updatemember({ ...formItem })
        if (res.code === 0) {
            message.success(res.msg)
            setmemberList([...res.data])
        }
    }
    //添加
    const addItem = async () => {
        const formItem = form.getFieldValue()
        formItem.key = Math.random().toString().slice(2, 8)
        formItem.range = memberList.length + 1
        formItem.last = moment(new Date()).format('YYYY-MM-DD HH:MM:SS')
        formItem.ip = Math.random().toString().slice(2, 5) + '.' + Math.random().toString().slice(2, 5) + '.' + Math.random().toString().slice(2, 4) + '.' + Math.random().toString().slice(2, 5)
        const { data: res } = await addmember({ ...formItem })
        if (res.code === 0) {
            message.success(res.msg)
            setmemberList([...res.data])
        }
    }
    //批量删除
    const confirmSelect = async () => {
        const { data: res } = await deleteSelemember({ selection })
        if (res.code === 0) {
            message.success(res.msg)
            console.log(res.data)
            setmemberList([...res.data])
        }
    }
    //点击取消
    const handleCancel = () => {
        setIsModalVisible(false);
    };
    //确认删除
    const confirm = (record) => {
        deleteItem(record)
        message.success('删除成功');
    }
    //取消删除
    const cancel = (e) => {
        message.error('取消成功');
    }
    //删除
    const deleteItem = async (record) => {
        const { data: res } = await deletemember({ id: record.key })
        setmemberList([...res.data])
    }
    //复选框
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelection([...selectedRowKeys])
        },
    };
    //开关
    const onChangeSwitch = async (checked, record) => {
        const { data: res } = await changeSwitchUsers({ state: checked, record })
        if (res.code === 0) {
            message.success(res.msg)
            setmemberList([...res.data])
        }
    }
     //模糊搜索
     const onChange = (e) => {
        setSeaValue(e.target.value)
    }
    //回车按下
    const pressEnter = async () => {
        if (seaValue === '') {
            getmemberList()
        } else {
            const { data: res } = await searchValuemember(seaValue)
            if (res.code === 0) {
                message.success(res.msg)
                setmemberList([...res.data])
            } else {
                message.error(res.msg)
            }
        }
    }
    //选择器
    const selectValue = async (value) => {
        console.log(value)
        if (value === '请选择') {
            getmemberList()
        } else {
            const { data: res } = await selectValmember({ value })
            if (res.code === 0) {
                message.success(res.msg)
                setmemberList([...res.data])
            } else {
                message.error(res.msg)
            }
        }
    }
    return <div className="role">
        <Card>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/home">首页</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/usersClassify">用户</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/usersClassify">用户管理</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/usersClassify">会员管理</a>
                </Breadcrumb.Item>
            </Breadcrumb>
        </Card>
        <Row gutter={16}>
            <Col className="gutter-row" span={4}>
                <Select type={type} selectValue={selectValue}></Select>
            </Col>
            <Col className="gutter-row" span={4}>
                <Select type={state} selectValue={selectValue}></Select>
            </Col>
            <Col className="gutter-row" span={4}>
                <Input placeholder="搜索用户名称" allowClear={true} value={seaValue} onChange={onChange} onPressEnter={pressEnter} style={{ width: '100%', margin: '10px 0' }} />
            </Col>
            <Col className="gutter-row" span={4}>
                <Button type='primary' style={{ width: '100%', margin: '10px 0' }} onClick={() => { setSeaValue(''); getmemberList(); }}>重置</Button>
            </Col>
            <Col className="gutter-row" span={4}>
                <Button type='primary' style={{ width: '100%', margin: '10px 0' }} onClick={() => {
                    form.setFieldsValue({ name: '', phone: '', role: '' });
                    setIsModalVisible(true);
                    setIsEdit(false);
                }}>添加</Button>
            </Col>
            <Col className="gutter-row" span={4}>
                <Popconfirm
                    title="确定删除吗"
                    onConfirm={confirmSelect}
                    onCancel={cancel}
                    okText="Yes"
                    cancelText="No"
                >
                    <Button type='primary' style={{ width: '100%', margin: '10px 0' }}>删除</Button>
                </Popconfirm>
            </Col>
        </Row>
        <Card style={{ width: '100%', height: '100%' }}>
            <Table
                rowSelection={{
                    type: selectionType,
                    ...rowSelection,
                }}
                columns={columns}
                dataSource={memberList}
                pagination={{
                    defaultPageSize: 5,
                    defaultCurrent: 1
                }}
            />
        </Card>
        <Modal title={isEdit ? "编辑内容" : "添加信息"} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
            <Form
                {...layout}
                form={form}
                name="basic"
                initialValues={{ remember: true }}
            >
                <Form.Item
                    label="会员类型"
                    name="type"
                    rules={[{ required: true, message: '请输入会员类型' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="手机号"
                    name="username"
                    rules={[{ required: true, message: '请输入手机号' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="昵称"
                    name="nickname"
                    rules={[{ required: true, message: '请输入昵称' }]}
                >
                    <Input />
                </Form.Item>
            </Form>
        </Modal>
    </div>
}
export default Member