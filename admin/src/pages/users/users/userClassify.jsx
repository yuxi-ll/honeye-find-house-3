import React, { useState, useEffect } from 'react'
import { Breadcrumb, Switch, Card, Table, Button, Col, Row, Input, Modal, Form, Popconfirm, message } from 'antd'
import { getUserClassify, deleteUserClassify, delectSeleUserClassify, updateUserClassify, addUserClassify, changeSwitchUsers } from '../../../api/index'
import moment from 'moment'
const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
};
const UserClassify = () => {
    const [userClassifyList, setUserClassifyList] = useState([])
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [isEdit, setIsEdit] = useState(false)
    const [form] = Form.useForm();
    const [selectionType, setSelectionType] = useState('checkbox');
    const [selection, setSelection] = useState([])
    const [record, setRecord] = useState({})

    useEffect(() => {
        getUserClassifyList()
    }, [])
    const columns = [
        {
            title: '分类名称',
            dataIndex: 'name',
        },
        {
            title: '二手房审核',
            dataIndex: 'second',
        },
        {
            title: '出租房审核',
            dataIndex: 'rent',
        },
        {
            title: '写字楼出售审核',
            dataIndex: 'sellOffice',
        },
        {
            title: '写字楼出租审核',
            dataIndex: 'rentOffice',
        },
        {
            title: '商铺出售审核',
            dataIndex: 'sellStore',
        },
        {
            title: '商铺出租审核',
            dataIndex: 'rentStore',
        },
        {
            title: '年费（元/年）',
            dataIndex: 'annual',
        },
        {
            title: '等级',
            dataIndex: 'class',
        },
        {
            title: '状态',
            dataIndex: 'state',
            render: (state, record) => {
                return <Switch checked={state === '开启' ? true : false} onChange={(e) => onChangeSwitch(e, record)} />
            }
        },
        {
            title: '操作',
            dataIndex: 'action',
            fixed: 'right',
            width: 180,
            render: (text, record) => {
                return <div>
                    <Button type='primary' onClick={() => edit(record)}>编辑</Button>
                    <Popconfirm
                        title="确定删除吗"
                        onConfirm={() => confirm(record)}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                    >
                        <Button type='error'>删除</Button>
                    </Popconfirm>
                </div>
            }
        }
    ];
    //获取角色列表
    const getUserClassifyList = async () => {
        const { data: res } = await getUserClassify()
        if (res.code === 0) {
            message.success(res.msg)
            setUserClassifyList([...res.data])
        }
    }
    //点击确认
    const handleOk = () => {
        if (isEdit) {
            editItem()
        } else {
            addItem()
        }
        setIsModalVisible(false);
    };
    //编辑
    const edit = async (record) => {
        form.setFieldsValue(record)
        setIsModalVisible(true)
        setIsEdit(true)
        setRecord({ ...record })
    }
    //编辑
    const editItem = async () => {
        const formItem = { ...record, ...form.getFieldValue() }
        const { data: res } = await updateUserClassify({ ...formItem })
        if (res.code === 0) {
            message.success(res.msg)
            setUserClassifyList([...res.data])
        }
    }
    //添加
    const addItem = async () => {
        const formItem = form.getFieldValue()
        formItem.key = Math.random().toString().slice(2, 8)
        formItem.range = userClassifyList.length + 1
        formItem.last = moment(new Date()).format('YYYY-MM-DD HH:MM:SS')
        formItem.ip = Math.random().toString().slice(2, 5) + '.' + Math.random().toString().slice(2, 5) + '.' + Math.random().toString().slice(2, 4) + '.' + Math.random().toString().slice(2, 5)
        const { data: res } = await addUserClassify({ ...formItem })
        if (res.code === 0) {
            message.success(res.msg)
            setUserClassifyList([...res.data])
        }
    }
    //批量删除
    const confirmSelect = async () => {
        const { data: res } = await delectSeleUserClassify({ selection })
        if (res.code === 0) {
            message.success(res.msg)
            console.log(res.data)
            setUserClassifyList([...res.data])
        }
    }
    //点击取消
    const handleCancel = () => {
        setIsModalVisible(false);
    };
    //确认删除
    const confirm = (record) => {
        deleteItem(record)
        message.success('删除成功');
    }
    //取消删除
    const cancel = (e) => {
        message.error('取消成功');
    }
    //删除
    const deleteItem = async (record) => {
        const { data: res } = await deleteUserClassify({ id: record.key })
        setUserClassifyList([...res.data])
    }
    //复选框
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelection([...selectedRowKeys])
        },
    };
    //开关
    const onChangeSwitch = async (checked, record) => {
        const { data: res } = await changeSwitchUsers({ state: checked, record })
        if (res.code === 0) {
            message.success(res.msg)
            setUserClassifyList([...res.data])
        }
    }
    return <div className="role">
        <Card>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/home">首页</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/usersClassify">用户</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/usersClassify">用户管理</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/usersClassify">用户分类</a>
                </Breadcrumb.Item>
            </Breadcrumb>
        </Card>
        <Row gutter={16}>
            <Col className="gutter-row" span={4}>
                <Button type='primary' style={{ width: '100%', margin: '10px 0' }} onClick={() => {
                    form.setFieldsValue({ name: '', phone: '', role: '' });
                    setIsModalVisible(true);
                    setIsEdit(false);
                }}>添加</Button>
            </Col>
            <Col className="gutter-row" span={4}>
                <Popconfirm
                    title="确定删除吗"
                    onConfirm={confirmSelect}
                    onCancel={cancel}
                    okText="Yes"
                    cancelText="No"
                >
                    <Button type='primary' style={{ width: '100%', margin: '10px 0' }}>删除</Button>
                </Popconfirm>
            </Col>
        </Row>
        <Card style={{ width: '100%', height: '100%' }}>
            <Table
                rowSelection={{
                    type: selectionType,
                    ...rowSelection,
                }}
                scroll={{ x: 1300 }}
                columns={columns}
                dataSource={userClassifyList}
                pagination={{
                    defaultPageSize: 5,
                    defaultCurrent: 1
                }}
            />
        </Card>
        <Modal title={isEdit ? "编辑内容" : "添加信息"} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
            <Form
                {...layout}
                form={form}
                name="basic"
                initialValues={{ remember: true }}
            >
                <Form.Item
                    label="分类名称"
                    name="name"
                    rules={[{ required: true, message: '请输入分类名称' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="二手房审核"
                    name="second"
                    rules={[{ required: true, message: '请输入二手房审核' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="出租房审核"
                    name="rent"
                    rules={[{ required: true, message: '出租房审核' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="写字楼出售审核"
                    name="sellOffice"
                    rules={[{ required: true, message: '请输入写字楼出售审核' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="写字楼出租审核"
                    name="rentOffice"
                    rules={[{ required: true, message: '请输入写字楼出租审核' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="商铺出售审核"
                    name="sellStore"
                    rules={[{ required: true, message: '请输入商铺出售审核' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="商铺出租审核"
                    name="rentStore"
                    rules={[{ required: true, message: '请输入商铺出租审核' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="年费"
                    name="annual"
                    rules={[{ required: true, message: '请输入年费' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="等级"
                    name="class"
                    rules={[{ required: true, message: '请输入等级' }]}
                >
                    <Input />
                </Form.Item>
            </Form>
        </Modal>
    </div>
}
export default UserClassify
