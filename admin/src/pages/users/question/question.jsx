import React, { useState, useEffect } from 'react'
import { Breadcrumb, Switch, Card, Table, Button, Col, Row, Input, Modal, Form, Popconfirm, message } from 'antd'
import { getquestion, deletequestion, deleteSelequestion, changeSwitchquestion } from '../../../api/index'

const Question = () => {
    const [questionList, setquestionList] = useState([])
    const [selectionType, setSelectionType] = useState('checkbox');
    const [selection, setSelection] = useState([])
    const [record, setRecord] = useState({})
    const columns = [
        {
            title: '用户',
            dataIndex: 'name',
            width: 120,
        },
        {
            title: '手机号',
            dataIndex: 'phone',
            width: 120,
        },
        {
            title: '问答内容',
            dataIndex: 'content',
            width: 120,
        },
        {
            title: '提交时间',
            dataIndex: 'last',
            width: 120,
        },
        {
            title: '状态',
            dataIndex: 'state',
            width: 120,
            render: (isOpen, record) => {
                return <Switch checked={record.isOpen === '开启' ? true : false} onChange={(e) => onChangeSwitch(e, record)} />
            }
        },
        {
            title: '操作',
            dataIndex: 'action',
            width: 220,
            render: (text, record) => {
                return <div>
                    <Button type='primary'>查看回复</Button>
                    <Popconfirm
                        title="确定删除吗"
                        onConfirm={() => confirm(record)}
                        onCancel={cancel}
                        okText="是"
                        cancelText="否"
                    >
                        <Button type='error'>删除</Button>
                    </Popconfirm>
                </div>
            }
        }
    ];
    useEffect(() => {
        getquestionList()
    }, [])
    //获取问答列表
    const getquestionList = async () => {
        const { data: res } = await getquestion()
        if (res.code === 0) {
            message.success(res.msg)
            setquestionList([...res.data])
        }
    }
    //批量删除
    const confirmSelect = async () => {
        const { data: res } = await deleteSelequestion({ selection })
        if (res.code === 0) {
            message.success(res.msg)
            console.log(res.data)
            setquestionList([...res.data])
        }
    }
    //确认删除
    const confirm = (record) => {
        deleteItem(record)
        message.success('删除成功');
    }
    //取消删除
    const cancel = (e) => {
        message.error('取消成功');
    }
    //删除
    const deleteItem = async (record) => {
        const { data: res } = await deletequestion({ id: record.key })
        setquestionList([...res.data])
    }
    //复选框
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelection([...selectedRowKeys])
        },
    };
    //开关
    const onChangeSwitch = async (checked, record) => {
        const { data: res } = await changeSwitchquestion({ state: checked, record })
        if (res.code === 0) {
            message.success(res.msg)
            setquestionList([...res.data])
        }
    }
    return <div className="community">
        <Card>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/home">首页</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/question">用户</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/question">问答管理</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/question">问答列表</a>
                </Breadcrumb.Item>
            </Breadcrumb>
        </Card>
        <Row gutter={16}>
            <Col className="gutter-row" span={4}>
                <Popconfirm
                    title="确定删除吗"
                    onConfirm={confirmSelect}
                    onCancel={cancel}
                    okText="是"
                    cancelText="否"
                >
                    <Button type='primary' style={{ width: '100%', margin: '10px 0' }}>删除</Button>
                </Popconfirm>
            </Col>
        </Row>
        <Card style={{ width: '100%', height: '100%' }}>
            <Table
                rowSelection={{
                    type: selectionType,
                    ...rowSelection,
                }}
                columns={columns}
                dataSource={questionList}
                pagination={{
                    defaultPageSize: 5,
                    defaultCurrent: 1
                }}
            />
        </Card>
    </div >
}
export default Question
