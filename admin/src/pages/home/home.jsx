/*
 * @Author: your name
 * @Date: 2021-05-16 19:18:10
 * @LastEditTime: 2021-05-30 10:51:32
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \react-admin\admin\src\App.js
 */
import React, { useState } from 'react'
import { Layout, Menu, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { NavLink, useHistory } from 'react-router-dom'
import ParticlesBg from 'particles-bg'
import routes from '../../routes/routes'
import './home.css'
const { SubMenu } = Menu
const { Header, Content, Sider } = Layout;
let config = {
  num: [1, 4],
  rps: 0.1,
  radius: [5, 40],
  life: [1.5, 3],
  v: [2, 3],
  tha: [-50, 50],
  alpha: [0.5, 0],
  scale: [.1, 0.9],
  position: "all",
  //color: ["random", "#ff0000"],
  cross: "dead",
  random: 10
};
const rootSubmenuKeys = ['modulesl', 'authority', 'housesMan', 'developer', 'groups', 'communityl', 'secondHouses', 'rentHouses', 'office', 'store', 'collects', 'articles', 'singlel', 'userman', 'review', 'appointmentList', 'questionList'];
const Home = (props) => {
  const [openKeys, setOpenKeys] = useState(['modulesl', 'housesMan', 'communityl', 'office', 'collects', 'userman']);
  const [currentIndex, setCurrentIndex] = useState(0)
  const history = useHistory()
  const onOpenChange = keys => {
    const latestOpenKey = keys.find(key => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };
  return (
    <Layout className="layout">
      <ParticlesBg type="custom" config={config} bg={{ position: 'absolute' }} />
      <Header className="header" style={{display: 'flex', justifyContent: 'space-between'}}>
        <div>宏烨找房管理后台</div>
        <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['modulesl']}>
          {
            routes.map((item, index) => {
              return <Menu.Item key={item.name} onClick={() => setCurrentIndex(index)}>{item.title}</Menu.Item>
            })
          }
        </Menu>
        <Button onClick={() => {history.push('/login'); sessionStorage.clear()}} type='primary'>退出</Button>
      </Header>
      <Content style={{ padding: '0 50px' }}>
        <Layout className="site-layout-background" style={{ padding: '24px 0' }}>
          <Sider className="site-layout-background" width={200}>
            <Menu
              mode="inline"
              openKeys={openKeys}
              onOpenChange={onOpenChange}
              style={{ height: '100%' }}
            >
              {
                routes[currentIndex].children.map((item, index) => <SubMenu key={item.name} icon={<UserOutlined />} title={item.title}>
                  {
                    item.children.map((items, indexs) => <Menu.Item key={items.name}>
                      <NavLink to={items.path}>{items.title}</NavLink>

                    </Menu.Item>)
                  }
                </SubMenu>)
              }
            </Menu>
          </Sider>
          <Content className="content" style={{ padding: '0 24px', minHeight: 500 }}>
            <Content>
              {
                props.children
              }
            </Content>
          </Content>
        </Layout>
      </Content>
    </Layout>
  )
}

export default Home