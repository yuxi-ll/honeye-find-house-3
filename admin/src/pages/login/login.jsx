import React from 'react'
import ParticlesBg from 'particles-bg'
import { Form, Input, Button, Checkbox, message } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { login } from '../../api/index'
import avatar from '../../assets/avatar.png'
import "./login.css"
let config = {
    num: [4, 7],
    rps: 0.1,
    radius: [5, 40],
    life: [1.5, 3],
    v: [2, 3],
    tha: [-50, 50],
    alpha: [0.6, 0],
    scale: [.1, 0.9],
    position: "all",
    //color: ["random", "#ff0000"],
    cross: "dead",
    random: 10
};
const Login = (props) => {
    const { history } = props
    const onFinish = async (values) => {
        const { data: res } = await login({ ...values })
        if (res.code === 0) {
            sessionStorage.setItem('token', res.token)
            message.success(res.msg)
            history.push('/home')
        } else {
            message.error(res.msg)
        }
    };
    return <div className="loginLay">
        <ParticlesBg type="custom" config={config} bg={true} />
        <div className="login">
            <div className="avatar">
                <img src={avatar} alt="" />
            </div>
            <Form
                name="normal_login"
                className="login-form"
                onFinish={onFinish}
            >
                <Form.Item
                    name="username"
                    rules={[
                        {
                            required: true,
                            message: '请输入用户名',
                        },
                    ]}
                >
                    <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: '请输入密码',
                        },
                    ]}
                >
                    <Input
                        prefix={<LockOutlined className="site-form-item-icon" />}
                        type="password"
                        placeholder="Password"
                    />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                        登录
                    </Button> Or <a href="/register">注册</a>
                </Form.Item>
            </Form>
        </div>

    </div>
}
export default Login