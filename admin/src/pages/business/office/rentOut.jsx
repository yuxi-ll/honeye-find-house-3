import React, { useState, useEffect } from 'react'
import { Card, Breadcrumb, Col, Row, message, Space, Form, Input, Modal, Table, Radio, Divider, Button } from "antd"
import Select from "../../../component/Select"
import { getArea, getCity, getCommunity, selectVal } from "../../../api/index"
import "../../business/office/index.css"
import axios from '../../../util/http'
const Rentout = () => {
    const [city, setCity] = useState([])//城市数据
    const [area, setArea] = useState([])//获取区列表
    const [commList, setCommList] = useState([])//获取校区列表
    const [rentout, setRentOut] = useState([])//获取表格数据
    const [isModalVisible, setIsModalVisible] = useState(false);
    const { Column, ColumnGroup } = Table//表格
    const [vals, setVals] = useState([])//模糊搜索
    const [selectionType, setSelectionType] = useState('checkbox');
    const [form] = Form.useForm()
    const [isEdit, setIsEdit] = useState(false)//编辑框
    const [listItem, setlistItem] = useState({})
    const [obj, setObj] = useState({ '序号': '', '楼盘名称': '', '标题': '', '面积': '', '租金': '', '排序': '', '状态': '' })
    useEffect(() => {
        getCityList()
        getAreaList()
        getCommList()
        getRentOutList()
    }, [])
    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 16,
        },
    };
    const tailLayout = {
        wrapperCol: {
            offset: 8,
            span: 16,
        },
    };
    //获取城市数据
    const getCityList = async () => {
        const { data: res } = await getCity()
        let city = res.body.map(item => item.label)
        //console.log(city)
        city.unshift('请选择')
        setCity([...city])
    }
    //获取区列表
    const getAreaList = async () => {
        const { data: res } = await getArea()
        let area = res.body.map(item => item.label)
        //console.log(area)
        area.unshift('请选择')
        setArea([...area])
    }
    //获取小区列表
    const getCommList = async () => {
        const { data: res } = await getCommunity()
        if (res.code === 0) {
            setCommList([...res.data])
        }
    }
    //获取表格数据
    const getRentOutList = async () => {
        const rentout = await axios.get("/api/rentout")
        setRentOut(set => [...rentout.data.list])
        console.log(rentout)
    }
    //选择器
    const selectValue = async (value) => {
        console.log(value)
        if (value === '请选择') {
            getCommList()
        } else {
            const { data: res } = await selectVal({ value })
            if (res.code === 0) {
                message.success(res.msg)
                setCommList([...res.data])
            } else {

            }
        }
    }
    const showModal = (record) => {
        console.log(record)
        setIsModalVisible(true);
        form.setFieldsValue(record)
    };

    const handleOk = (row) => {
        console.log(row)
        setIsModalVisible(false);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    //模糊搜索
    const changeVals = (e) => {
        setVals(e.target.value, vals)
    }
    //搜索
    const Search = () => {
        const res = rentout.filter(item => item.title.includes(vals))
        setRentOut(res)
        console.log(res)
    }
    //删除
    const handleDelete = (key) => {
        let list = rentout.filter(item => item.key !== key)
        setRentOut([...list])
    }
    //编辑确认
    const onFinish = (values) => {
        if (isEdit) {
            rentout.map((item) => {
                if (item.key === values.key) {
                    item.ey = values.key
                    item.name = values.name
                    item.title = values.title
                    item.area = values.area
                    item.moneny = values.moneny
                    item.id = values.id
                    item.status = values.status
                }
            })
        } else {
            addItem()
        }
    }

    //编辑取消
    const onFinishFailed = () => {
        setIsModalVisible(false)
    }
    //添加
    const addItem = async () => {
        const formItem = form.getFieldValue()
        //formItem.key = Math.random().toString().slice(3, 8)
        console.log(formItem)
        formItem.range = rentout.length + 1
        rentout.unshift(formItem)
        setRentOut([...rentout])
    }
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        getCheckboxProps: (record) => ({
            disabled: record.name === 'Disabled User',
            // Column configuration not to be checked
            name: record.name,
        }),
    };
    return <div className="rentout">
        <div className="header">
            <Card>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/home/rentOut">首页</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
            </Card>
        </div>
        <div className="select">
            <Card>
                <Row gutter={16}>
                    <Col className="gutter-row" span={4} >
                        <Select type={city} selectValue={selectValue}></Select>
                    </Col>
                    <Col className="gutter-row" span={4}>
                        <Select type={area} selectValue={selectValue}></Select>
                    </Col>
                    <Col>
                        <div className="search">
                            <input type="text" value={vals} onChange={changeVals} />
                            <button onClick={Search}>搜索</button>
                        </div>
                    </Col>
                </Row>
            </Card>
        </div>
        <div className="handle">
            <Card>
                <Button type="primary" onClick={() => {
                    form.setFieldsValue({ '序号': '', '楼盘名称': '', '标题': '', '面积': '', '租金': '', '排序': '', '状态': '' })
                    setIsModalVisible(true)
                    setIsEdit(false)
                }}>
                    添加
                </Button>
                <Button type="primary">
                    删除
                </Button>
                <Modal title="Basic Modal" visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                    <div className="form">
                        <Form
                            {...layout}
                            name="basic"
                            initialValues={{
                                remember: true,
                                key: obj.key,
                                name: obj.name,
                                title: obj.title,
                                area: obj.area,
                                moneny: obj.moneny,
                                status: obj.status,
                                id: obj.id
                            }}
                            onFinish={onFinish}
                            onFinishFailed={onFinishFailed}
                            form={form}
                        >
                            <Form.Item
                                label="序号"
                                name="key"
                            >
                                <Input />
                            </Form.Item>


                            <Form.Item
                                label="楼盘名称"
                                name="name"
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item
                                label="标题"
                                name="title"
                            >
                                <Input />

                            </Form.Item>

                            <Form.Item
                                label="面积"
                                name="area"
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item
                                label="租金"
                                name="moneny"
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item
                                label="序号"
                                name="id"
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item
                                label="状态"
                                name="status"
                            >
                                <Input />
                            </Form.Item>

                            <Form.Item {...tailLayout}>
                                <Button type="primary" htmlType="submit">
                                    Submit
                            </Button>
                            </Form.Item>
                        </Form>
                    </div>
                </Modal>
            </Card>
        </div>
        <div className="table">
            <Card>
                <Radio.Group
                    onChange={({ target: { value } }) => {
                        setSelectionType(value);
                    }}
                    value={selectionType}
                >
                    <Radio value="checkbox">Checkbox</Radio>
                    <Radio value="radio">radio</Radio>
                </Radio.Group>
                <Divider />
                <Table
                    rowSelection={{
                        type: selectionType,
                        ...rowSelection,
                    }}
                    dataSource={rentout}
                    pagination={{ defaultPageSize: 3 }}
                    style={{ textAlign: 'center' }}
                >
                    <Column title="序号" dataIndex="key" key="key" />
                    <Column title="楼盘名称" dataIndex="name" key="name" />
                    <Column title="标题" dataIndex="title" key="title" />
                    <Column title="面积" dataIndex="area" key="area" />
                    <Column title="租金" dataIndex="moneny" key="moneny" />
                    <Column title="排序" dataIndex="id" key="id" />
                    <Column title="状态" dataIndex="status" key="status" />
                    <Column title="操作" dataIndex="handle" key="handle" render={(text, record) => (
                        <Space size="middle">
                            <Button onClick={() => { handleDelete(record.key) }} style={{ borderRadius: 5 }}>删除</Button>
                            <Button onClick={() => { showModal(record) }} style={{ borderRadius: 5 }}>编辑</Button>
                            <Button onClick={() => { }} style={{ borderRadius: 5 }}>详情</Button>
                        </Space>
                    )} />
                </Table>
            </Card>

        </div>
    </div>
}

export default Rentout