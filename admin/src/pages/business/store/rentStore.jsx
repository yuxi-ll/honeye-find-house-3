import React from 'react'
import { Breadcrumb, message } from 'antd';
import "../store/rent.css"
import { Table, Button, Space, Input, Form, Modal, Col, Card, Popconfirm } from 'antd';
import axios from 'axios';
import { useState, useEffect } from "react"
import { getCity, getCommunity, selectVal, getArea } from "../../../api/index"
import Select from "../../../component/Select"

const Rentstore = () => {
    const [list, setList] = useState([]);//渲染数据
    const [isModalVisible, setIsModalVisible] = useState(false)//编辑
    const [form] = Form.useForm();//编辑
    const [listItem, setlistItem] = useState({})//编辑
    const [vals, setVals] = useState('')//搜索
    const [isEdit, setIsEdit] = useState(false)//搜索
    const [city, setCity] = useState([])//城市
    const [seaValue, setSeaValue] = useState('')
    const [commList, setCommList] = useState([])//小区
    const [area, setArea] = useState([])//区
    //代替声明周期
    useEffect(() => {
        handleStore()//表格数据接口
        getCityList()//城市接口
        getCommList()//小区接口
        getAreaList()//区接口
    }, [])
    const handleStore = async () => {
        const flower = await axios.get("/api/sell")
        setList(list => flower.data.sell)
        console.log(list)
    }
    const columns = [
        {
            title: '序号',
            dataIndex: 'id',
            render: text => <a>{text}</a>
        },
        {
            title: '排序',
            dataIndex: 'rank',
            sorter: {
                compare: (a, b) => a.rank - b.rank,
                multiple: 2,
            },

        },
        {
            title: '省份',
            dataIndex: 'province',
        },
        {
            title: '市区',
            dataIndex: 'city',
        },
        {
            title: '县城',
            dataIndex: 'town',
        },
        {
            title: '操作',
            key: 'action',
            render: (text, record) => (
                <Space size='middle'>
                    <a onClick={() => {
                        setlistItem({ ...record })
                        form.setFieldsValue({ ...record })
                        setIsModalVisible(true)
                        setIsEdit(true)
                    }}
                    >编辑</a>
                    <Popconfirm
                        title="你确定要删除么?"
                        onConfirm={()=>confirm({...record})}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                    >
                        <a>删除</a>
                    </Popconfirm>
                </Space>
            )
        },
    ];
    function confirm(e) {
        console.log(e);
        message.success('确定');
        handleDelete(e)//调用删除弹框
    }

    function cancel(e) {
        console.log(e);
        message.error('取消');
    }
    //删除
    const handleDelete = (row) => {
        console.log("hahahah")
        const ind = list.findIndex(item => item.id === row.id)
        console.log(ind)
        list.splice(ind, 1)
        setList(list => [...list])
    }

    //获取城市列表
    const getCityList = async () => {
        const { data: res } = await getCity()
        let city = res.body.map(item => item.label)
        city.unshift('请选择')
        setCity([...city])
    }
    //获取区列表
    const getAreaList = async () => {
        const { data: res } = await getArea()
        let area = res.body.map(item => item.label)
        area.unshift('请选择')
        setArea([...area])
    }
    //获取小区列表
    const getCommList = async () => {
        const { list: res } = await getCommunity()
    }
    //选择器
    const selectValue = async (value) => {
        console.log(value)
        if (value === "请选择") {
            getCommList()
        } else {
            const { data: res } = await selectVal({ value })
            if (res.code === 0) {
                message.success(res.msg)
                setCommList([...res.data])
            } else {
                message.error(res.msg)
            }
        }

    }
    
    //编辑
    const handleOK = () => {
        if (isEdit) {
            const formItem = form.getFieldsValue();
            const Item = { ...listItem, ...formItem }
            list.map(item => {
                if (item.id === Item.id) {
                    item.rank = Item.rank;
                    item.province = Item.province;
                    item.city = Item.city;
                    item.town = Item.town
                }
            })
        }
        else {
            addItem()
        }

        setIsModalVisible(false)
    }
    const handleCancel = () => {
        setIsModalVisible(false)
    }
    //全选
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        onSelect: (record, selected, selectedRows) => {
            console.log(record, selected, selectedRows);
        },
        onSelectAll: (selected, selectedRows, changeRows) => {
            console.log(selected, selectedRows, changeRows);
        },
    };
    //添加
    const addItem = async () => {
        const formItem = form.getFieldValue()
        formItem.key = Math.random().toString().slice(2, 8)
        formItem.range = list.length + 1;
        list.unshift(formItem)
        console.log(list)
        setList([...list])
    }
    //模糊搜索
    const changeVals = (e) => {
        setVals(e.target.value, vals);
    }
    //搜索
    const Search = () => {
        console.log("xioahua");
        const res = list.filter(item => item.province.includes(vals) || item.province.includes(vals))
        setList(res);
        console.log(res)
    }
    return <div className="rentStore">
        <div className="header">
            <Card>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/home">首页</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/rentStore">商业</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/rentStore">商铺</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/rentStore">出租管理</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
                <h3>出租管理</h3>
            </Card>
        </div>

        <div className="main">
            <div className="level">
                <div className="diqu">
                    {/* <Card> */}
                    省份:
                        <Col className="gutter-row" span={10}>
                        <Select type={city} selectValue={selectValue}></Select>
                    </Col>
                    市区:
                        <Col className="gutter-row" span={10}>
                        <Select type={area} selectValue={selectValue}></Select>
                    </Col>
                    县城:
                        <Col className="gutter-row" span={10}>
                        <Input placeholder="搜索小区名称" allowClear={true} onChange={changeVals} value={vals} onClick={Search} style={{ width: '100%', margin: '10px 0' }} />
                    </Col>
                    {/* </Card> */}

                </div>
                <div className="btn">
                    {/* <button>重置</button>
                        <button>查询</button> */}
                </div>
            </div>

            <div className="shike">
                <div className="left">
                    <span>时刻表</span>
                </div>
                <div className="right">
                    {/* <Card> */}
                    <Button type="primary" style={{ width: "100%", margin: '10px 0' }} onClick={() => {
                        form.setFieldsValue({ name: '', address: '', time: '' });
                        setIsModalVisible(true)
                        setIsEdit(false)
                    }}>新建</Button>
                    {/* </Card> */}

                </div>
            </div>


            <Table rowSelection={{ ...rowSelection }} columns={columns} dataSource={list} columns={columns} pagination={{ defaultPageSize: 3 }}>
            </Table>
            <Modal title='编辑' visible={isModalVisible} onOk={handleOK} onCancel={handleCancel}>
                <Form
                    form={form}
                >
                    <Form.Item label="排序" name="rank">
                        <Input placeholder="input placeholder" />
                    </Form.Item>
                    <Form.Item label="省份" name="province">
                        <Input placeholder="input placeholder" />
                    </Form.Item>
                    <Form.Item label="市区" name="city">
                        <Input placeholder="input placeholder" />
                    </Form.Item>
                    <Form.Item label="县城" name="town">
                        <Input placeholder="input placeholder" />
                    </Form.Item>
                </Form>
            </Modal>

        </div>
    </div>

}

export default Rentstore