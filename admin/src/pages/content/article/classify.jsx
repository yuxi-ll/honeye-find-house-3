import '../collect/collect.css'
import React, { useState,useEffect } from 'react';
import { Table,Space,Popconfirm, message, Modal,Card  } from 'antd';
import axios from '../../../util/http'
import { Form, Input, Button } from 'antd';
import { Breadcrumb } from 'antd';



const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};    
const tailLayout = {
  wrapperCol: {
    offset: 8,
    span: 16,
  }
};

const Collect = () => {
        const { Column,ColumnGroup  } = Table; //ui库表格组件
        const [selectionType] = useState('checkbox');//多选框类型
        const [tabData,setTabData]=useState([])  //表格总数据
        const [isModalVisible, setIsModalVisible] = useState(false); //弹框开启关闭状态
        const [updataMsg,setUpdataMsg]=useState({key:'',id:'',num:'',firm:'',grade:''})  //表格总数据
        const [form] = Form.useForm()
        const [btnFlag,setBtnFlag]=useState('') //弹框控制字段
        const [searchOne,setSearchOne]=useState('')  //搜索id
        const [searchTwo,setSearchTwo]=useState('')  //搜索id
        const [searchThree,setSearchThree]=useState('')  //搜索id

        //弹框提交按钮
        const onFinish = (values) => {
          console.log('Success:', values);
          setIsModalVisible(false);
          console.log(btnFlag)
          if(btnFlag==='update'){
              const arr=tabData.map((item)=>{
                return item.key==values.key?values:item
              })
              setTabData(arr)
              form.setFieldsValue({key:'',id:'',num:'',firm:'',grade:''})
          }else if(btnFlag==="add"){
            tabData.push(values)
            setTabData([...tabData])
          }
        };
        
        //生命周期
        useEffect(()=>{
          getTabData()
        },[])

        //请求数据
        const getTabData=()=>{
          axios.get('/api/gettypelist').then(res=>{
            setTabData(res.data.typelist)
          })
        }

        //多选框事件
        const rowSelection = {
          // onChange: (selectedRowKeys, selectedRows) => {
          //   console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
          // },
        };

        //排序
        function onChange(pagination, filters, sorter, extra) {
          // console.log('params', pagination, filters, sorter, extra);
        }

        //删除确认
        function confirm(record) {
          console.log(record)
          setTabData(tabData.filter((item)=>{
            return item.key!==record.key
          }))
          message.success('删除成功');
        }

        //编辑按钮
        const showModal = (record) => {
          setIsModalVisible(true);
          form.setFieldsValue(record)
          setBtnFlag('update')
        };
      
        //对话框叉号关闭
        const handleCancel = () => {
          setIsModalVisible(false);
          form.setFieldsValue()
        };

        //添加按钮
        const addTabDataBtn=()=>{
          setIsModalVisible(true);
          setBtnFlag('add')
          form.setFieldsValue()
        }
        
        //搜索id绑定
        const inpChangeOne=(e)=>{
          setSearchOne(e.target.value)
        }
        //搜索企业绑定
        const inpChangeTwo=(e)=>{
          setSearchTwo(e.target.value)
        }
        //搜索优先级绑定
        const inpChangeThere=(e)=>{
          setSearchThree(e.target.value)
        }

        //搜索重置按钮
        const searchResetBtn=()=>{
          setSearchTwo('')
          setSearchOne('') 
          setSearchThree('')
          getTabData()
        }
        //搜索查询按钮
        const searchSubmitBtn=()=>{
          console.log(searchOne)
          const arr=tabData.filter((item)=>{
            return item.num.includes(searchOne)&&item.firm.includes(searchTwo)&&item.grade.toString().includes(searchThree)
          })
          setTabData([...arr])
        }

    return <div className="collectdiv">
        <Card>
        <Breadcrumb>
        <Breadcrumb.Item>
          <a href="/home">首页</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>
          <a href="/home/collect">内容</a>
        </Breadcrumb.Item>
        <Breadcrumb.Item>文章分类</Breadcrumb.Item>
      </Breadcrumb>
        </Card>
      <h3>文章分类</h3>
            <div className="searchp">
            <Space direction="vertical">
              <p>
                序号 : 
                <Input
                value={searchOne}
                    id="searone"
                    placeholder="请输入"
                    allowClear={true}
                    size="large"
                    onChange={inpChangeOne}
                  />
                </p>
                <p>
                  企业 : 
                  <Input
                  value={searchTwo}
                    placeholder="请输入"
                    allowClear
                    size="large"
                    onChange={inpChangeTwo}
                  />
                </p>
                <p>
                  推荐优先度 : 
                  <Input
                  value={searchThree}
                    placeholder="请输入"
                    allowClear
                    size="large"
                    onChange={inpChangeThere}
                  />
                </p>
              </Space>
              <div className="btns">
                <Button onClick={searchResetBtn}>重置</Button>
                <Button className="searchsumit" onClick={searchSubmitBtn}>查询</Button>
              </div>
            </div>
            <Button onClick={addTabDataBtn}>添加</Button>
              <Table
                rowSelection={{
                  type: selectionType,
                  ...rowSelection,
                }}
                dataSource={tabData}
                pagination={{
                  defaultPageSize:5,
                  total:tabData.length
                }}
                onChange={onChange}
                showSorterTooltip={false}
              >
                <ColumnGroup title="编号" className='activetr' dataIndex="num" key="num">
                </ColumnGroup>
                <Column title="企业" dataIndex="firm" key="firm" />
                <Column title="推荐优先级" dataIndex="grade" key="grade" sorter={(a, b) => a.grade - b.grade} />
                <Column
                  title="Action"
                  key="action"
                  render={(text, record) => (
                    <Space size="middle"  className="columnbtn">
                      <button  onClick={()=>{showModal(record)}}>编辑</button>
                      <Modal title="编辑" visible={isModalVisible} footer={null} onCancel={handleCancel} className="modal">
                      <Form
                        {...layout}
                        name="basic"
                        form={form}
                        initialValues={{
                          remember: true,
                          key:updataMsg.key,
                          num:updataMsg.num,
                          firm:updataMsg.firm,
                          grade:updataMsg.grade
                        }}
                        onFinish={onFinish}
                      >
                        <Form.Item
                          label="num"
                          name="num"
                        >
                          <Input />
                        </Form.Item>
                        
                        <Form.Item
                          label="key"
                          name="key"
                        >
                          <Input />
                        </Form.Item>

                        <Form.Item
                          label="企业"
                          name="firm"
                        >
                          <Input />
                        </Form.Item>

                        <Form.Item
                          label="优先级"
                          name="grade"
                        >
                          <Input />
                        </Form.Item>

                        <Form.Item {...tailLayout}>
                          <Button type="primary" htmlType="submit">
                            Submit
                          </Button>
                        </Form.Item>
                      </Form>
                      </Modal>
                      <Popconfirm
                          title="确定要删除吗"
                          onConfirm={()=>{confirm(record)}}
                          okText="确认"
                          cancelText="取消"
                        >
                          <button >删除</button>
                        </Popconfirm>
                    </Space>
                  )}
                />
              </Table>
    </div>
}

export default Collect