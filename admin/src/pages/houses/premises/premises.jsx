import React from 'react'
import { useState, useEffect } from "react"
import http from "../../../util/http"
import { Input } from 'antd';
import "../../system/modules/advertising/advertising.css"
import { Table, Space } from 'antd';
import { Modal, } from 'antd';
import { Form, } from 'antd';
import { Button } from 'antd';
import { SearchOutlined } from '@ant-design/icons'
import { Breadcrumb, Card } from 'antd';
import { Image } from 'antd';
import {
    PlusOutlined,
    SyncOutlined,
    SettingOutlined
} from '@ant-design/icons';

const Premises = () => {

    const [isModalVisible, setIsModalVisible] = useState(false);
    const [selectionType, setSelectionType] = useState('checkbox');


    const handleOk = () => {
        setIsModalVisible(false);
        // form.submit()

    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    const [data, setdata] = useState([])
    const [record, setrecord] = useState(undefined)
    const [value, setvalue] = useState(String)
    const [value1, setvalue1] = useState(String)
    const [flag, setflag] = useState(Boolean)
    const [isModalVisibles, setIsModalVisibles] = useState(false);
    const [img1, setimg1] = useState('')
    const [imgFlag, setimgFlag] = useState('')
    useEffect(() => {
        getlist();
        form.setFieldsValue(record)

    }, [])
    // 列表
    const columns = [{
        title: '楼盘名称',
        dataIndex: "name"


    },
    {
        title: '所属区域',
        dataIndex: "area"

    },
    {
        title: '价格',
        dataIndex: 'price',


    },
    {
        title: '排序',
        dataIndex: "sorts",
        sorter: {
            compare: (a, b) => a.sorts - b.sorts,
            multiple: 3,
        },

    },

    {
        title: "操作",

        render: (text, record) => (
            <Space size="middle">
                <a type="primary" onClick={() => showModals(record.img1)}>户型</a>


                <a type="primary" onClick={() => showModals(record.img2)}>相册</a>

                <a type="primary" onClick={() => showModals(record.img3)}>动态</a>

                <a type="primary" onClick={() => showModals(record.img4)}>沙盘</a>
                <a type="primary" onClick={() => showModal(record)}>编辑</a>
                <a onClick={() => handleDelete(record)}>删除</a>
            </Space>
        )
    }
    ]
    // 点击删除
    const handleDelete = (record) => {
        http.post('/api/premisesDelete', { id: record.key }).then(res => {
            // console.log(res.data.data)
            setdata([...res.data.data2])
        })
    }

    // 点击编辑
    const showModal = (record) => {
        setIsModalVisible(true);
        setrecord(record);
        console.log(record)
        setflag(true)
    };
    // 渲染列表
    const getlist = () => {
        http.get('/api/advertising').then(res => {
            setdata([...res.data.list2])
            // console.log(res.data.list)
        })
    }
    // 点击添加
    const handleAdd = (record) => {
        setIsModalVisible(true);
        setflag(false)
    }

    // 模糊搜索
    const handleSearch = (e) => {
        setvalue(e.target.value)
        // console.log(value)
    }
    // 模糊搜索
    const handleSearchs = (e) => {
        setvalue1(e.target.value)
        // console.log(value)
    }

    // 搜索序号
    const handleSearch1 = () => {
        http.post('/api/premisessearch1', { value: value, value1: value1 }).then(res => {
            setdata([...res.data.data2])
        })
    }
    // 重置
    const handleReset = () => {
        setvalue('')
        setvalue1('')
        http.get('/api/advertising').then(res => {
            setdata([...res.data.list2])
            console.log(res.data)
        })
    }


    // 弹出框

    // console.log(data)
    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 16,
        },
    };
    const tailLayout = {
        wrapperCol: {
            offset: 8,
            span: 16,
        },
    };



    const onFinish = (values) => {
        console.log('Success:', values);
        setIsModalVisible(false);
        if (flag) {
            http.post('/api/premisesedit', {
                id: record.key, name: values.name, area: values.area, price: values.price, sorts: values.sorts, state: values.state

            }).then(res => {
                // console.log(res.data.data)

                // console.log(res.data.data)
                setdata([...res.data.data2])
            })
        } else {
            http.post('/api/premisesadd', {
                key: Math.random().toString().slice(2, 100), name: values.name, area: values.area, price: values.price, sorts: values.sorts, state: values.state

            }).then(res => {
                // console.log(res.data.data)

                // console.log(res.data.data)
                setdata([...res.data.data2])
            })
        }
        // console.log(record)
    };
    // 弹出户型，相册，动态，沙盘
    const showModals = (record) => {
        setIsModalVisibles(true);
        console.log(record)
        setimg1(record)

    };
    const handleOks = () => {
        setIsModalVisibles(false);
    };

    const handleCancels = () => {
        setIsModalVisibles(false);
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    const onOk = () => {
        form.submit()

        console.log(data)

    }

    const [form] = Form.useForm();
    useEffect(() => {
        form.setFieldsValue(record)
    }, [isModalVisible])
    // 复选
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        getCheckboxProps: (record) => ({
            disabled: record.name === 'Disabled User',
            // Column configuration not to be checked
            name: record.name,
        }),
    };
    console.log(img1)
    return <div>
        <h3>
            <Card>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/home">首页</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/premises">楼盘</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/premises">楼盘管理</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/premises">楼盘列表</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
            </Card>
        </h3>
        {/* 模糊搜索 */}
        <div className="search">
            <div className="search1">
                <span>楼盘名称：</span>
                <Input placeholder="请输入" value={value} onChange={handleSearch} />


            </div>
            <div className="search2">
                <span>所属区域：</span>
                <Input placeholder="请输入" value={value1} onChange={handleSearchs} />

            </div>
            <div className="search2">


                <Button type="primary" onClick={handleSearch1}>
                    查询
                </Button>
                <Button onClick={handleReset}>重置</Button>
            </div>

        </div>
        {/* 广告列表 */}
        <div className="advertisingList">
            <div className="function">
                <h4>广告位管理</h4>
                <div className="function1">
                    <Button type="primary" icon={<PlusOutlined />} onClick={handleAdd}>
                        新建
    </Button>
                    <SyncOutlined spin />
                    <SettingOutlined />
                </div>
            </div>
            <Table rowSelection={{
                type: selectionType,
                ...rowSelection,
            }} dataSource={data} columns={columns} pagination={{ defaultPageSize: 5 }} />

        </div>
        {/* 弹出框 */}


        <div className="dialog">
            <Modal title="Basic Modal" visible={isModalVisible} onOk={onOk} onCancel={handleCancel}
                forceRender
            >
                <Form
                    form={form}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    {/* {JSON.stringify(record)} */}
                    <Form.Item
                        label="楼盘名称"
                        name="name"
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="所属区域"
                        name="area"

                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="价格"
                        name="price"

                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="排序"
                        name="sorts"

                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="状态"
                        name="state"

                    >
                        <Input />
                    </Form.Item>
                </Form>
            </Modal>
        </div>
        {/* 弹出框 */}
        <Modal title="Basic Modal" visible={isModalVisibles} onOk={handleOks} onCancel={handleCancels}>
            <Image
                width={200}
                src={img1}
            />
        </Modal>

    </div>
}

export default Premises