import React from 'react'
import { Upload } from 'antd';
import ImgCrop from 'antd-img-crop';
import { useState, useEffect } from "react"
import http from "../../../util/http"
import { Input } from 'antd';
import "../../system/modules/advertising/advertising.css"
import { Table, Space } from 'antd';
import { Modal, } from 'antd';
import { Form, } from 'antd';
import { Button } from 'antd';
import { SearchOutlined } from '@ant-design/icons'
import { Breadcrumb, Card } from 'antd';
import {
    PlusOutlined,
    SyncOutlined,
    SettingOutlined
} from '@ant-design/icons';

const Group = () => {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [selectionType, setSelectionType] = useState('checkbox');


    const handleOk = () => {
        setIsModalVisible(false);
        // form.submit()

    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };
    const [data, setdata] = useState([])
    const [record, setrecord] = useState(undefined)
    const [value, setvalue] = useState(String)
    const [value1, setvalue1] = useState(String)
    const [flag, setflag] = useState(Boolean)

    useEffect(() => {
        getlist();
        form.setFieldsValue(record)

    }, [])
    // 列表
    const columns = [{
        title: '楼盘名称',
        dataIndex: "name"


    },
    {
        title: '图片',
        render: (record) => (
            <Space size="middle">

                <img src={record.img} alt="" />
            </Space>
        )

    },
    {
        title: '排序',
        dataIndex: 'sorts',

        sorter: {
            compare: (a, b) => a.sorts - b.sorts,
            multiple: 3,
        },
    },
    {
        title: '位置',
        dataIndex: "area"

    },
    {
        title: '类型',
        dataIndex: "types"

    },
    {
        title: '售价(元/平方米)',
        dataIndex: "price"

    },



    {
        title: "操作",

        render: (text, record) => (
            <Space size="middle">

                <a type="primary" onClick={() => showModal(record)}>编辑</a>
                <a onClick={() => handleDelete(record)}>删除</a>
            </Space>
        )
    }
    ]
    // 点击删除
    const handleDelete = (record) => {
        http.post('/api/groupDelete', { id: record.key }).then(res => {
            // console.log(res.data.data)
            setdata([...res.data.data4])
        })

    }

    // 点击编辑
    const showModal = (record) => {
        setIsModalVisible(true);
        setrecord(record);
        setflag(true)
        console.log(flag)

    };
    // 渲染列表
    const getlist = () => {
        http.get('/api/advertising').then(res => {
            setdata([...res.data.list4])
            // console.log(res.data.list)
        })
    }
    // 点击添加
    const handleAdd = (record) => {

        setIsModalVisible(true);

        setflag(false)
    }

    // 模糊搜索
    const handleSearch = (e) => {
        setvalue(e.target.value)
        // console.log(value)
    }
    // 模糊搜索
    const handleSearchs = (e) => {
        setvalue1(e.target.value)
        console.log(value1)
    }

    // 搜索序号
    const handleSearch1 = () => {
        http.post('/api/groupsearch1', { value: value, value1: value1 }).then(res => {
            setdata([...res.data.data4])
        })
        console.log(value1)
    }
    // 重置
    const handleReset = () => {
        setvalue('')
        setvalue1('')
        http.get('/api/advertising').then(res => {
            setdata([...res.data.list2])
            console.log(res.data)
        })
    }


    // 弹出框

    // console.log(data)
    const layout = {
        labelCol: {
            span: 8,
        },
        wrapperCol: {
            span: 16,
        },
    };
    const tailLayout = {
        wrapperCol: {
            offset: 8,
            span: 16,
        },
    };



    const onFinish = (values) => {
        console.log('Success:', values);
        // console.log(fileList[0]);
        setIsModalVisible(false);
        console.log(flag)
        // console.log(record)
        if (flag) {
            http.post('/api/groupedit', {
                id: record.key, name: values.name, img: fileList[0].thumbUrl, area: values.area, price: values.price, sorts: values.sorts, types: values.types

            }).then(res => {
                // console.log(res.data.data)

                // console.log(res.data.data)
                setdata([...res.data.data4])
            })
        } else {
            http.post('/api/groupadd', {
                key: Math.random().toString().slice(2, 10), name: values.name, img: fileList[0].thumbUrl, area: values.area, price: values.price, sorts: values.sorts, types: values.types

            }).then(res => {
                // console.log(res.data.data)

                // console.log(res.data.data)
                setdata([...res.data.data4])
            })
        }
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    const onOk = () => {
        form.submit()
        // console.log(record.key)
        console.log(data)

    }

    const [form] = Form.useForm();
    useEffect(() => {
        form.setFieldsValue(record)
    }, [isModalVisible])
    // 复选
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        getCheckboxProps: (record) => ({
            disabled: record.name === 'Disabled User',
            // Column configuration not to be checked
            name: record.name,
        }),
    };
    // 上传图片
    const [fileList, setFileList] = useState([

    ]);

    const onChange = ({ fileList: newFileList }) => {
        setFileList(newFileList);
    };

    const onPreview = async file => {
        let src = file.url;
        if (!src) {
            src = await new Promise(resolve => {
                const reader = new FileReader();
                reader.readAsDataURL(file.originFileObj);
                reader.onload = () => resolve(reader.result);
            });
        }
        const image = new Image();
        image.src = src;
        const imgWindow = window.open(src);
        imgWindow.document.write(image.outerHTML);
    };
    console.log(flag)
    return <div>
        <h3>
            <Card>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/home">首页</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/group">楼盘</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/group">团购管理</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/group">团购列表</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
            </Card>
        </h3>
        {/* 模糊搜索 */}
        <div className="search">
            <div className="search1">
                <span>楼盘名称：</span>
                <Input placeholder="请输入" value={value} onChange={handleSearch} />


            </div>
            <div className="search2">
                <span>位置：</span>
                <Input placeholder="请输入" value={value1} onChange={handleSearchs} />

            </div>
            <div className="search2">


                <Button type="primary" onClick={handleSearch1}>
                    查询
                </Button>
                <Button onClick={handleReset}>重置</Button>
            </div>

        </div>
        {/* 广告列表 */}
        <div className="advertisingList">
            <div className="function">
                <h4>广告位管理</h4>
                <div className="function1">
                    <Button type="primary" icon={<PlusOutlined />} onClick={handleAdd}>
                        新建
    </Button>
                    <SyncOutlined spin />
                    <SettingOutlined />
                </div>
            </div>
            <Table rowSelection={{
                type: selectionType,
                ...rowSelection,
            }} dataSource={data} columns={columns} pagination={{ defaultPageSize: 5 }} />

        </div>
        {/* 弹出框 */}


        <div className="dialog">
            <Modal title="时刻表" visible={isModalVisible} onOk={onOk} onCancel={handleCancel}
                forceRender
            >
                <Form
                    form={form}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                >
                    {/* {JSON.stringify(record)} */}
                    <Form.Item
                        label="楼盘名称"
                        name="name"
                    >
                        <Input />
                    </Form.Item>


                    <Form.Item
                        label="图片"
                        name="img"
                    >
                        <ImgCrop rotate>
                            <Upload
                                action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
                                listType="picture-card"
                                fileList={fileList}
                                onChange={onChange}
                                onPreview={onPreview}
                            >
                                {fileList.length < 5 && '+ Upload'}
                            </Upload>
                        </ImgCrop>
                    </Form.Item>
                    <Form.Item
                        label="排序"
                        name="sorts"

                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="位置"
                        name="area"

                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="类型"
                        name="types"

                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="售价"
                        name="price"

                    >
                        <Input />
                    </Form.Item>

                </Form>
            </Modal>
        </div>
    </div>
}

export default Group