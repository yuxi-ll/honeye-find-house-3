import React, { useState, useEffect } from 'react'
import { Breadcrumb, Card, Table, Button, Col, Row, Input, Switch, Modal, Form, Popconfirm, message } from 'antd'
import Select from '../../../component/Select'
import { deleteSeleSucc, getCity, getArea, searchValue, selectVal, getSuccess, deleteSucc, addSucc, searchValueSucc, selectValueSucc, updateSucc } from '../../../api/index'
import moment from 'moment'
import './community.css'
const layout = {
    labelCol: { span: 6 },
    wrapperCol: { span: 16 },
};
const Success = () => {
    const [successList, setSuccessList] = useState([])
    const [city, setCity] = useState([])
    const [area, setArea] = useState([])
    const [selectionType, setSelectionType] = useState('checkbox');
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [isEdit, setIsEdit] = useState(false)
    const [form] = Form.useForm();
    const [record, setRecord] = useState({})
    const [seaValue, setSeaValue] = useState('')
    const [selection, setSelection] = useState([])

    useEffect(() => {
        getCityList()
        getAreaList()
        getSuccessList()
    }, [])
    const columns = [
        {
            title: '成交户型',
            dataIndex: 'name',
        },
        {
            title: '所属区域',
            dataIndex: 'address',
        },
        {
            title: '成交时间',
            dataIndex: 'last',
            defaultSortOrder: 'descend',
            sorter: (a, b) => new Date(a.last).getTime() - new Date(b.last).getTime(),
        },
        {
            title: '占地面积',
            dataIndex: 'acreage',
        },
        {
            title: '价格',
            dataIndex: 'price',
        }, {
            title: '操作',
            dataIndex: 'action',
            render: (text, record) => {
                return <div>
                    <Button type='text' onClick={() => edit(record)}>编辑</Button>
                    <Popconfirm
                        title="确定删除吗"
                        onConfirm={() => confirm(record)}
                        onCancel={cancel}
                        okText="Yes"
                        cancelText="No"
                    >
                        <Button type='text'>删除</Button>
                    </Popconfirm>
                </div>
            }
        }
    ];
    //复选框
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelection([...selectedRowKeys])
        },
    };
    //批量删除
    const confirmSelect = async () => {
        const { data: res } = await deleteSeleSucc({ selection })
        if (res.code === 0) {
            message.success(res.msg)
            console.log(res.data)
            setSuccessList([...res.data])
        }
    }
    //获取城市列表
    const getCityList = async () => {
        const { data: res } = await getCity()
        let city = res.body.map(item => item.label)
        city.unshift('请选择')
        setCity([...city])
    }
    //获取区列表
    const getAreaList = async () => {
        const { data: res } = await getArea()
        let area = res.body.map(item => item.label)
        area.unshift('请选择')
        setArea([...area])
    }
    //获取成交列表
    const getSuccessList = async () => {
        const { data: res } = await getSuccess()
        if (res.code === 0) {
            message.success(res.msg)
            setSuccessList([...res.data])
        }
    }
    //编辑
    const edit = async (record) => {
        form.setFieldsValue(record)
        setIsModalVisible(true)
        setIsEdit(true)
        setRecord({ ...record })
    }
    //点击确认
    const handleOk = () => {
        if (isEdit) {
            editItem()
        } else {
            addItem()
        }
        setIsModalVisible(false);
    };
    //编辑
    const editItem = async () => {
        const formItem = { ...record, ...form.getFieldValue() }
        const { data: res } = await updateSucc({ ...formItem })
        if (res.code === 0) {
            message.success(res.msg)
            setSuccessList([...res.data])
        }
    }
    //添加
    const addItem = async () => {
        const formItem = form.getFieldValue()
        formItem.key = Math.random().toString().slice(2, 8)
        formItem.range = successList.length + 1
        formItem.last = moment(new Date()).format('YYYY-MM-DD HH:MM:SS')
        const { data: res } = await addSucc({ ...formItem })
        if (res.code === 0) {
            message.success(res.msg)
            setSuccessList([...res.data])
        }
    }
    //点击取消
    const handleCancel = () => {
        setIsModalVisible(false);
    };
    //确认删除
    const confirm = (record) => {
        deleteItem(record)
        message.success('删除成功');
    }
    //取消删除
    const cancel = (e) => {
        message.error('取消成功');
    }
    //删除
    const deleteItem = async (record) => {
        const { data: res } = await deleteSucc({ id: record.key })
        setSuccessList([...res.data])
    }
    //模糊搜索
    const onChange = (e) => {
        setSeaValue(e.target.value)
    }
    //回车按下
    const pressEnter = async () => {
        if (seaValue === '') {
            getSuccessList()
        } else {
            const { data: res } = await searchValueSucc(seaValue)
            if (res.code === 0) {
                message.success(res.msg)
                setSuccessList([...res.data])
            } else {
                message.error(res.msg)
            }
        }
    }
    //选择器
    const selectValue = async (value) => {
        console.log(value)
        if (value === '请选择') {
            getSuccess()
        } else {
            const { data: res } = await selectValueSucc({ value })
            if (res.code === 0) {
                message.success(res.msg)
                setSuccessList([...res.data])
            } else {
                message.error(res.msg)
            }
        }
    }
    return <div className="success">
        <Card>
            <Breadcrumb>
                <Breadcrumb.Item>
                    <a href="/home">首页</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/success">二手/出租</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/success">小区管理</a>
                </Breadcrumb.Item>
                <Breadcrumb.Item>
                    <a href="/home/success">成交列表</a>
                </Breadcrumb.Item>
            </Breadcrumb>
        </Card>
        <Row gutter={16}>
            <Col className="gutter-row" span={4}>
                <Select type={city} selectValue={selectValue}></Select>
            </Col>
            <Col className="gutter-row" span={4}>
                <Select type={area} selectValue={selectValue}></Select>
            </Col>
            <Col className="gutter-row" span={4}>
                <Input placeholder="搜索开发商名称" allowClear={true} value={seaValue} onChange={onChange} onPressEnter={pressEnter} style={{ width: '100%', margin: '10px 0' }} />
            </Col>
            <Col className="gutter-row" span={4}>
                <Button type='primary' style={{ width: '100%', margin: '10px 0' }} onClick={() => { setSeaValue(''); getSuccessList(); }} >重置</Button>
            </Col>
            <Col className="gutter-row" span={4}>
                <Button type='primary' style={{ width: '100%', margin: '10px 0' }} onClick={() => {
                    form.setFieldsValue({ name: '', address: '', last: '' });
                    setIsModalVisible(true);
                    setIsEdit(false);
                }}>新建</Button>
            </Col>
            <Col className="gutter-row" span={4}>
                <Popconfirm
                    title="确定删除吗"
                    onConfirm={confirmSelect}
                    onCancel={cancel}
                    okText="Yes"
                    cancelText="No"
                >
                    <Button type='primary' style={{ width: '100%', margin: '10px 0' }}>删除</Button>
                </Popconfirm>
            </Col>
        </Row>
        <Card style={{ width: '100%', height: '100%' }}>
            <Table
                rowSelection={{
                    type: selectionType,
                    ...rowSelection,
                }}
                columns={columns}
                dataSource={successList}
                pagination={{
                    defaultPageSize: 5,
                    defaultCurrent: 1
                }}
            />
        </Card>
        <Modal title={isEdit ? "编辑内容" : "添加信息"} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
            <Form
                {...layout}
                form={form}
                name="basic"
                initialValues={{ remember: true }}
            >
                <Form.Item
                    label="小区名称"
                    name="name"
                    rules={[{ required: true, message: '请输入小区名称' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="所在地区"
                    name="address"
                    rules={[{ required: true, message: '请输入地区' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="更新时间"
                    name="last"
                    rules={[{ required: true, message: '请输入时间' }]}
                >
                    <Input />
                </Form.Item>
            </Form>
        </Modal>
    </div>
}
export default Success
