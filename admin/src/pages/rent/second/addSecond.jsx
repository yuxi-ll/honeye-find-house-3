import React from 'react'
import { Breadcrumb } from 'antd';
const AddSecond = () => {
    return <div>
        添加二手房源
        <Breadcrumb>
            <Breadcrumb.Item>
                <a href="/home/second">首页</a>
            </Breadcrumb.Item>
            <Breadcrumb.Item>
                <a href="/home/addSecond">二手房</a>
            </Breadcrumb.Item>
        </Breadcrumb>
    </div>
}

export default AddSecond