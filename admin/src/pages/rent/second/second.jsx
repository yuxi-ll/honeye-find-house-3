import React from 'react'
import { Breadcrumb } from 'antd';
import "./second.css"
import axios from "axios"
import { useState, useEffect } from "react"
import { Table, Space, Modal, Form, Input, Card, Button, Popconfirm,message } from 'antd';



const Second = (props) => {
    const [list, setList] = useState([]);  //渲染数据
    const [isModalVisible, setIsModalVisible] = useState(false);//编辑
    const [form] = Form.useForm();//编辑
    const [listItem, setlistItem] = useState({})
    const [vals, setVals] = useState('');//搜索
    const [isEdit, setIsEdit] = useState(false)
    //代替声明周期 
    useEffect(() => {
        handleUserInfo()
    }, [])
    const handleUserInfo = async () => {
        const aaa = await axios.get("/api/second")
        setList(list => aaa.data.second)
        console.log(list)
    }
    const columns = [
        {
            title: '序号',
            dataIndex: 'id',
            render: text => <a>{text}</a>
        },
        {
            title: '企业类型',
            dataIndex: 'lastName',
        },
        {
            title: '投放时间',
            dataIndex: 'age',
            sorter: {
                compare: (a, b) => a.age - b.age,
                multiple: 3,
            },
        },
        {
            title: '广告内容',
            dataIndex: 'address',
        },
        {
            title: '操作',
            dataIndex: 'action',
            render: (text, record) => (
                <Space size="middle">
                    <a onClick={() => {
                        setlistItem({ ...record })
                        form.setFieldsValue({ ...record }) //record表示from表单中得值
                        setIsModalVisible(true)
                        setIsEdit(true)
                    }}>编辑</a>
                    <Popconfirm
                        title="你确定要删除么"
                        onConfirm={()=>confirm({...record})}
                        onCancel={cancel}
                        okText="确定"
                        cancelText="取消"
                    >
                        <a>删除</a>
                    </Popconfirm>
                </Space>
            ),
        }
    ];
    //删除弹框
    function confirm(e) {
        console.log(e);
        message.success('Click on Yes');
        handleDel(e)//调用删除函数
    }

    function cancel(e) {
        console.log(e);
        message.error('Click on No');
    }
    //删除
    const handleDel = (row) => {
        console.log(row, 'jaskfafhsgk')
        const ind = list.findIndex(item => item.id === row.id)//每行下标
        console.log(ind)
        list.splice(ind, 1);//1就是删除掉一个
        setList(list => [...list])
    }
    //编辑中ok按钮
    const handleOk = () => {
        if (isEdit) {
            const formItem = form.getFieldsValue(); //获取到三个文本框的值
            const Item = { ...listItem, ...formItem } //合并三个文本框的值
            list.map(item => {
                if (item.id === Item.id) {
                    item.lastName = Item.lastName;
                    item.age = Item.age;
                    item.address = Item.address
                }
            })
        } else {
            addItem()
        }

        setIsModalVisible(false)
    }
    //编辑中取消按钮
    const handleCancel = () => {
        setIsModalVisible(false)
    }
    //全选
    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
        },
        onSelect: (record, selected, selectedRows) => {
            console.log(record, selected, selectedRows);
        },
        onSelectAll: (selected, selectedRows, changeRows) => {
            console.log(selected, selectedRows, changeRows);
        },
    };
    //模糊搜索
    const changeVals = (e) => {
        setVals(e.target.value, vals);
    }
    //搜索
    const Search = () => {
        console.log("xioahua");
        const res = list.filter(item => item.lastName.includes(vals) || item.lastName.includes(vals))
        setList(res);
        console.log(res)
    }
    //添加
    const addItem = async () => {
        const formItem = form.getFieldValue()
        formItem.key = Math.random().toString().slice(2, 8)
        formItem.range = list.length + 1
        list.unshift(formItem)
        console.log(list)
        setList([...list])
    }
    return <div className="second">
        {/* 二手房源管理 */}
        <div className="hand">
            <Card>
                <Breadcrumb>
                    <Breadcrumb.Item>
                        <a href="/home">首页</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/second">二手/出租</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/second">二手房</a>
                    </Breadcrumb.Item>
                    <Breadcrumb.Item>
                        <a href="/home/second">房源管理</a>
                    </Breadcrumb.Item>
                </Breadcrumb>
            </Card>

            <b>房源管理</b>
        </div>
        <div className="propetry">

            <div className="left">
                序号:<input type="text" placeholder="请输入" />
            </div>
            <div className="right">
                <Card>
                    <div className="find">
                        企业类型:<input type="text" value={vals} onChange={changeVals} />
                        <button type="primary" onClick={Search}>搜索</button>
                    </div>
                </Card>
            </div>
        </div>

        <div className="import">
            <div className="advert">
                <div className="left">
                    <span>广告位管理</span>
                </div>
                <div className="right">
                    <Card>
                        <Button type='primary' style={{ width: '100%', margin: '10px 0' }} onClick={() => {
                            form.setFieldsValue({ name: '', address: '', time: '' });
                            setIsModalVisible(true);
                            setIsEdit(false);
                        }}>新建</Button>
                    </Card>

                </div>
            </div>
            <Table rowSelection={{ ...rowSelection }} dataSource={list} columns={columns} pagination={{ defaultPageSize: 3 }} >
            </Table>
            <Modal title='编辑' visible={isModalVisible} onOk={handleOk} onCancel={handleCancel}>
                <Form
                    form={form}
                >
                    <Form.Item label="姓名" name="lastName">
                        <Input placeholder="input placeholder" />
                    </Form.Item>
                    <Form.Item label="年龄" name="age">
                        <Input placeholder="input placeholder" />
                    </Form.Item>
                    <Form.Item label="住址" name="address">
                        <Input placeholder="input placeholder" />
                    </Form.Item>
                </Form>
            </Modal>


        </div>
    </div>
}

export default Second