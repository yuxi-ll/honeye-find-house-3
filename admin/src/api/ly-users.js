/*
 * @Author: your name
 * @Date: 2021-05-19 08:43:08
 * @LastEditTime: 2021-05-30 10:15:29
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\honeye-find-house-3\admin\src\api\login.js
 */
import axios from '../util/http'

//登录接口
export const login = ({
    username,
    password
}) => {
    return axios.post('/api/login', {
        username,
        password
    })
}

//注册接口
export const register = ({
    username,
    password,
    phone
}) => {
    return axios.post('/api/register', {
        username,
        password,
        phone
    })
}

//获取小区列表数据
export const getCommunity = () => {
    return axios.get('/api/community')
}

//获取城市列表
export const getCity = () => {
    return axios.get('http://157.122.54.189:9060/area/city?level=1')
}

//获取市区列表
export const getArea = () => {
    return axios.get('http://157.122.54.189:9060/area/city?level=2')
}

//删除小区列表数据
export const deleteComm = ({id}) => {
    return axios.post('/api/deleteComm', {id})
}

//批量删除
export const delectSele = ({selection}) => {
    return axios.post('/api/deleteSele', {selection})
}

//编辑小区列表数据
export const updateComm = ({...record}) => {
    return axios.post('/api/updateComm', {...record})
}

//添加数据
export const addComm = ({...record}) => {
    return axios.post('/api/addComm', {...record})
}

//模糊搜索
export const searchValue = (value) => {
    return axios.post('/api/searchValue', value)
}

//条件搜索
export const selectVal = ({value}) => {
    return axios.post('/api/selectValue', {value})
}

//获取成交列表数据
export const getSuccess = () => {
    return axios.get('/api/success')
}

//删除成交列表数据
export const deleteSucc = ({id}) => {
    return axios.post('/api/deleteSucc', {id})
}

//编辑成交列表数据
export const updateSucc = ({...record}) => {
    return axios.post('/api/updateSucc', {...record})
}

//添加数据
export const addSucc = ({...record}) => {
    return axios.post('/api/addSucc', {...record})
}

//模糊搜索
export const searchValueSucc = (value) => {
    return axios.post('/api/searchValueSucc', value)
}

//条件搜索
export const selectValueSucc = ({value}) => {
    return axios.post('/api/selectValueSucc', {value})
}

//更改状态
export const changeSwitch = ({state, record}) => {
    return axios.post('/api/changeSwitch', {state, record})
}

//批量删除
export const deleteSeleSucc = ({selection}) => {
    return axios.post('/api/deleteSeleSucc', {selection})
}

//获取人员列表数据
export const getpeople = () => {
    return axios.get('/api/people')
}

//删除人员列表数据
export const deletepeople = ({id}) => {
    return axios.post('/api/deletepeople', {id})
}

//编辑人员列表数据
export const updatepeople = ({...record}) => {
    return axios.post('/api/updatepeople', {...record})
}

//添加数据
export const addpeople = ({...record}) => {
    return axios.post('/api/addpeople', {...record})
}

//模糊搜索
export const searchValuepeople = (value) => {
    return axios.post('/api/searchValuepeople', value)
}
//更改状态
export const changeSwitchpeople = ({state, record}) => {
    return axios.post('/api/changeSwitchpeople', {state, record})
}

//获取角色列表数据
export const getrole = () => {
    return axios.get('/api/role')
}

//删除角色列表数据
export const deleterole = ({id}) => {
    return axios.post('/api/deleterole', {id})
}

//编辑角色列表数据
export const updaterole = ({...record}) => {
    return axios.post('/api/updaterole', {...record})
}

//添加数据
export const addrole = ({...record}) => {
    return axios.post('/api/addrole', {...record})
}

//模糊搜索
export const searchValuerole = (value) => {
    return axios.post('/api/searchValuerole', value)
}
//更改状态
export const changeSwitchrole = ({state, record}) => {
    return axios.post('/api/changeSwitchrole', {state, record})
}

//获取写字楼出售列表数据
export const getOffice = () => {
    return axios.get('/api/office')
}

//删除写字楼出售列表数据
export const deleteOffice = ({id}) => {
    return axios.post('/api/deleteOffice', {id})
}

//批量删除
export const delectSeleOffice = ({selection}) => {
    return axios.post('/api/deleteSeleOffice', {selection})
}

//编辑写字楼出售列表数据
export const updateOffice = ({...record}) => {
    return axios.post('/api/updateOffice', {...record})
}

//添加数据
export const addOffice = ({...record}) => {
    return axios.post('/api/addOffice', {...record})
}

//模糊搜索
export const searchValueOffice = (value) => {
    return axios.post('/api/searchValueOffice', value)
}

//条件搜索
export const selectValOffice = ({value}) => {
    return axios.post('/api/selectValueOffice', {value})
}

//更改状态
export const changeSwitchOffice = ({state, record}) => {
    return axios.post('/api/changeSwitchoffice', {state, record})
}



//获取用户分类列表数据
export const getUserClassify = () => {
    return axios.get('/api/getUserClassify')
}

//删除用户分类列表数据
export const deleteUserClassify = ({id}) => {
    return axios.post('/api/deleteUsersClassify', {id})
}

//批量删除
export const delectSeleUserClassify = ({selection}) => {
    return axios.post('/api/deleteSeleUsersClassify', {selection})
}

//编辑用户分类列表数据
export const updateUserClassify = ({...record}) => {
    return axios.post('/api/updateUsersClassify', {...record})
}

//添加数据
export const addUserClassify = ({...record}) => {
    return axios.post('/api/addUsersClassify', {...record})
}
//更改状态
export const changeSwitchUsers = ({state, record}) => {
    return axios.post('/api/changeSwitchUsersClassify', {state, record})
}


//获取小区列表数据
export const getMember = () => {
    return axios.get('/api/member')
}
//删除小区列表数据
export const deletemember = ({id}) => {
    return axios.post('/api/deletemember', {id})
}

//批量删除
export const deleteSelemember = ({selection}) => {
    return axios.post('/api/deleteSelemember', {selection})
}

//编辑成员列表数据
export const updatemember = ({...record}) => {
    return axios.post('/api/updatemember', {...record})
}

//添加数据
export const addmember = ({...record}) => {
    return axios.post('/api/addmember', {...record})
}

//模糊搜索
export const searchValuemember = (value) => {
    return axios.post('/api/searchValuemember', value)
}

//条件搜索
export const selectValmember = ({value}) => {
    return axios.post('/api/selectValuemember', {value})
}


//获取问答列表数据
export const getquestion = () => {
    return axios.get('/api/question')
}
//删除问答列表数据
export const deletequestion = ({id}) => {
    return axios.post('/api/deletequestion', {id})
}

//批量删除
export const deleteSelequestion = ({selection}) => {
    return axios.post('/api/deleteSelequestion', {selection})
}

//条件搜索
export const selectValquestion = ({value}) => {
    return axios.post('/api/selectValuequestion', {value})
}
//更改状态
export const changeSwitchquestion = ({state, record}) => {
    return axios.post('/api/changeSwitchquestion', {state, record})
}


//获取经纪人列表数据
export const getagent = () => {
    return axios.get('/api/getagent')
}
//删除经纪人列表数据
export const deleteagent = ({id}) => {
    return axios.post('/api/deleteagent', {id})
}

//批量删除
export const deleteSeleagent = ({selection}) => {
    return axios.post('/api/deleteSeleagent', {selection})
}

//条件搜索
export const selectValagent = ({value}) => {
    return axios.post('/api/selectValueagent', {value})
}
//更改状态
export const changeSwitchagent = ({state, record}) => {
    return axios.post('/api/changeSwitchagent', {state, record})
}


//获取预约列表数据
export const getappointment = () => {
    return axios.get('/api/appointment')
}
//删除预约列表数据
export const deleteapp = ({id}) => {
    return axios.post('/api/deleteapp', {id})
}

//批量删除
export const deleteSeleapp = ({selection}) => {
    return axios.post('/api/deleteSeleapp', {selection})
}

//条件搜索
export const selectValapp = ({value}) => {
    return axios.post('/api/selectValueapp', {value})
}
//更改状态
export const changeSwitchapp = ({state, record}) => {
    return axios.post('/api/changeSwitchapp', {state, record})
}
//编辑预约列表数据
export const updateapp = ({...record}) => {
    return axios.post('/api/updateapp', {...record})
}
//编辑预约列表数据
export const addapp = ({...record}) => {
    return axios.post('/api/addapp', {...record})
}


//获取楼盘点评列表数据
export const gethousereview = () => {
    return axios.get('/api/housereview')
}
//删除楼盘点评列表数据
export const deletehousereview = ({id}) => {
    return axios.post('/api/deletehousereview', {id})
}

//批量删除
export const deleteSelehousereview = ({selection}) => {
    return axios.post('/api/deleteSelehousereview', {selection})
}

//条件搜索
export const selectValhousereview = ({value}) => {
    return axios.post('/api/selectValuehousereview', {value})
}
//更改状态
export const changeSwitchhousereview = ({state, record}) => {
    return axios.post('/api/changeSwitchhousereview', {state, record})
}