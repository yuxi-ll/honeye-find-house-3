/*
 * @Author: your name
 * @Date: 2021-05-17 19:10:54
 * @LastEditTime: 2021-05-25 09:52:50
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \react-admin\admin\src\routes\route.js
 */
import Home from '../pages/home/home'
import Area from '../pages/system/modules/area/area'
import City from '../pages/system/modules/area/city'
import School from '../pages/system/modules/school/school'
import AddSchool from '../pages/system/modules/school/addSchool'
import Subway from '../pages/system/modules/subway/subway'
import Station from '../pages/system/modules/subway/station'
import Advertising from '../pages/system/modules/advertising/advertising'
import Role from '../pages/system/authority/role/role'
import Recommend from '../pages/system/modules/recommend/recommend'
import People from '../pages/system/authority/people/people'
import Premises from '../pages/houses/premises/premises'
import AddHouses from '../pages/houses/premises/addHouses'
import Developers from '../pages/houses/developers/developers'
import AddDevelopers from '../pages/houses/developers/addDevelorers'
import Group from '../pages/houses/group/group'
import addGroup from '../pages/houses/group/addGroup'
import Community from '../pages/rent/community/community'
import Success from '../pages/rent/community/success'
import Second from '../pages/rent/second/second'
import AddSecond from '../pages/rent/second/addSecond'
import RentHouse from '../pages/rent/rentHouses/rentHouses'
import AddRentHouse from '../pages/rent/rentHouses/addRentHouses'
import OfficeRent from '../pages/business/office/rentOut'
import SellOffice from '../pages/business/office/sell'
import StoreRent from '../pages/business/store/rentStore'
import SellStore from '../pages/business/store/sellStore'
import collect from '../pages/content/collect/collect'
import AddCollect from '../pages/content/collect/addCollection'
import Article from '../pages/content/article/classify'
import Single from '../pages/content/single/single'
import AddSingle from '../pages/content/single/addSingle'
import Login from '../pages/login/login'
import Register from '../pages/register/register'
import ArticleList from '../pages/content/article/articleList'
import AddArticle from '../pages/content/article/addArticle'
import Recovery from '../pages/content/article/recovery'
import UsersClassify from '../pages/users/users/userClassify'
import Member from '../pages/users/users/member/member'
import Agent from '../pages/users/review/agent'
import HouseReview from '../pages/users/review/houseReview'
import Appointment from '../pages/users/appointment/appointment'
import Question from '../pages/users/question/question'

const route = [{
    path: '/login',
    component: Login,
    meta: {
        title: 'login'
    }
}, {
    path: '/register',
    component: Register,
    meta: {
        title: 'Register'
    }
}, {
    path: '/home',
    component: Home,
    meta: {
        title: '主页'
    },
    children: [{
        path: '/home/area',
        component: Area,
        meta: {
            title: '地区管理'
        },
    }, {
        path: '/home/city',
        component: City,
        meta: {
            title: '城市列表'
        },
    }, {
        path: '/home/school',
        component: School,
        meta: {
            title: '学校管理'
        },
    }, {
        path: '/home/addschool',
        component: AddSchool,
        meta: {
            title: '添加学校'
        },
    }, {
        path: '/home/subway',
        component: Subway,
        meta: {
            title: '地铁管理'
        },
    }, {
        path: '/home/station',
        component: Station,
        meta: {
            title: '地铁站管理'
        },
    }, {
        path: '/home/advertising',
        component: Advertising,
        meta: {
            title: '广告位管理'
        },
    }, {
        path: '/home/recommend',
        component: Recommend,
        meta: {
            title: '推荐位管理'
        },
    }, {
        path: '/home/people',
        component: People,
        meta: {
            title: '人员管理'
        }
    }, {
        path: '/home/role',
        component: Role,
        meta: {
            title: '角色管理'
        }
    }, {
        path: '/home/premises',
        component: Premises,
        meta: {
            title: '楼盘列表'
        }
    }, {
        path: '/home/addHouses',
        component: AddHouses,
        meta: {
            title: '添加楼盘'
        }
    }, {
        path: '/home/developers',
        component: Developers,
        meta: {
            title: '开发商列表'
        }
    }, {
        path: '/home/addDevelopers',
        component: AddDevelopers,
        meta: {
            title: '添加开发商'
        }
    }, {
        path: '/home/group',
        component: Group,
        meta: {
            title: '团购列表'
        }
    }, {
        path: '/home/addGroup',
        component: addGroup,
        meta: {
            title: '添加团购'
        }
    }, {
        path: '/home/community',
        component: Community,
        meta: {
            title: '小区列表'
        }
    }, {
        path: '/home/success',
        component: Success,
        meta: {
            title: '成交记录'
        }
    }, {
        path: '/home/second',
        component: Second,
        meta: {
            title: '二手房源管理'
        }
    }, {
        path: '/home/addSecond',
        component: AddSecond,
        meta: {
            title: '添加二手房源管理'
        }
    }, {
        path: '/home/rentHouse',
        component: RentHouse,
        meta: {
            title: '出租房源管理'
        }
    }, {
        path: '/home/addRentHouse',
        component: AddRentHouse,
        meta: {
            title: '添加出租房源'
        }
    }, {
        path: '/home/officeRent',
        component: OfficeRent,
        meta: {
            title: '出租管理'
        }
    }, {
        path: '/home/sellOffice',
        component: SellOffice,
        meta: {
            title: '出售管理'
        }
    }, {
        path: '/home/storeRent',
        component: StoreRent,
        meta: {
            title: '出租管理'
        }
    }, {
        path: '/home/sellStore',
        component: SellStore,
        meta: {
            title: '出售管理'
        }
    }, {
        path: '/home/collect',
        component: collect,
        meta: {
            title: '采集管理'
        }
    }, {
        path: '/home/addCollect',
        component: AddCollect,
        meta: {
            title: '添加'
        }
    }, {
        path: '/home/article',
        component: Article,
        meta: {
            title: '文章分类'
        }
    }, {
        path: '/home/single',
        component: Single,
        meta: {
            title: '单页面列表'
        }
    }, {
        path: '/home/addSingle',
        component: AddSingle,
        meta: {
            title: '新增'
        }
    }, {
        path: '/home/articleList',
        component: ArticleList,
        meta: {
            title: '文章列表'
        }
    }, {
        path: '/home/addArticle',
        component: AddArticle,
        meta: {
            title: '增加文章'
        }
    }, {
        path: '/home/recovery',
        component: Recovery,
        meta: {
            title: '回收站'
        }
    },{
        path: '/home/usersClassify',
        component: UsersClassify,
        meta: {
            title: '用户分类'
        }
    }, {
        path: '/home/member',
        component: Member,
        meta: {
            title: '会员管理'
        }
    }, {
        path: '/home/agent',
        component: Agent,
        meta: {
            title: '经纪人点评'
        }
    }, {
        path: '/home/houseReview',
        component: HouseReview,
        meta: {
            title: '楼盘点评'
        }
    }, {
        path: '/home/appointment',
        component: Appointment,
        meta: {
            title: '预约管理'
        }
    }, {
        path: '/home/question',
        component: Question,
        meta: {
            title: '问答列表'
        }
    }, {
        from: '/home',
        to: '/home/area'
    }]
}, {
    from: '/',
    to: '/home/area'
}]

export default route