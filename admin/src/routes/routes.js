/*
 * @Author: your name
 * @Date: 2021-05-17 08:07:33
 * @LastEditTime: 2021-05-25 09:53:03
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \react-admin\admin\src\routes\routes.js
 */
const routes = [{
    title: '系统',
    name: 'system',
    children: [{
        title: '模块管理',
        name: 'modulesl',
        children: [{
            title: '地区管理',
            name: 'area',
            path: '/home/area',
        }, {
            title: '学校管理',
            name: 'school',
            path: '/home/school',
        }, {
            title: '地铁管理',
            name: 'subway',
            path: '/home/subway',
        }, {
            title: '广告位管理',
            name: 'advertising',
            path: '/home/advertising',
        }, {
            title: '推荐位管理',
            name: 'recommend',
            path: '/home/recommend',
        }]
    }, {
        title: '权限管理',
        name: 'authority',
        children: [{
            title: '人员管理',
            name: 'people',
            path: '/home/people',
        }, {
            title: '角色管理',
            name: 'role',
            path: '/home/role',
        }]
    }]
}, {
    title: '楼盘',
    name: 'houses',
    children: [{
        title: '楼盘管理',
        name: 'housesMan',
        children: [{
            title: '楼盘列表',
            name: 'premises',
            path: '/home/premises',
        }]
    }, {
        title: '开发商',
        name: 'developer',
        children: [{
            title: '开发商列表',
            name: 'developers',
            path: '/home/developers',
        }]
    }, {
        title: '团购管理',
        name: 'groups',
        children: [{
            title: '团购列表',
            name: 'group',
            path: '/home/group',
        }]
    }]
}, {
    title: '二手/出租',
    name: 'rent',
    children: [{
        title: '小区管理',
        name: 'communityl',
        children: [{
            title: '小区列表',
            name: 'community',
            path: '/home/community',
        }, {
            title: '成交列表',
            name: 'success',
            path: '/home/success',
        }]
    }, {
        title: '二手房',
        name: 'secondHouses',
        children: [{
            title: '房源管理',
            name: 'second',
            path: '/home/second',
        }]
    }, {
        title: '出租房',
        name: 'rentHouses',
        children: [{
            title: '房源管理',
            name: 'rentHouse',
            path: '/home/rentHouse',
        }]
    }]
}, {
    title: '商业',
    name: 'bussiness',
    children: [{
        title: '写字楼',
        name: 'office',
        children: [{
            title: '出售管理',
            name: 'sellOffice',
            path: '/home/sellOffice',
        }, {
            title: '出租管理',
            name: 'officeRent',
            path: '/home/officeRent',
        }]
    }, {
        title: '商铺',
        name: 'store',
        children: [{
            title: '出售管理',
            name: 'sellStore',
            path: '/home/sellStore',
        }, {
            title: '出租管理',
            name: 'storeRent',
            path: '/home/storeRent',
        }]
    }]
}, {
    title: '内容',
    name: 'content',
    children: [{
        title: '采集管理',
        name: 'collects',
        children: [{
            title: '采集管理',
            name: 'collect',
            path: '/home/collect',
        }]
    }, {
        title: '文章管理',
        name: 'articles',
        children: [{
            title: '文章分类',
            name: 'article',
            path: '/home/article',
        }, {
            title: '文章列表',
            name: 'articleList',
            path: '/home/articleList'
        }, {
            title: '增加文章',
            name: 'addArticle',
            path: '/home/addArticle'
        }, {
            title: '回收站',
            name: 'recovery',
            path: '/home/recovery'
        }]
    }, {
        title: '单页面管理',
        name: 'singlel',
        children: [{
            title: '单页面列表',
            name: 'single',
            path: '/home/single',
        }]
    }]
}, {
    title: '用户',
    name: 'userlist',
    children: [{
        title: '用户管理',
        name: 'userman',
        children: [{
            title: '用户分类',
            name: 'usersClassify',
            path: '/home/usersClassify',
        }, {
            title: '会员管理',
            name: 'member',
            path: '/home/member',
        }]
    }, {
        title: '评论管理',
        name: 'review',
        children: [{
            title: '经纪人点评',
            name: 'agent',
            path: '/home/agent',
        }, {
            title: '楼盘点评',
            name: 'houseReview',
            path: '/home/houseReview',
        }]
    }, {
        title: '预约管理',
        name: 'appointmentList',
        children: [{
            title: '预约列表',
            name: 'appointment',
            path: '/home/appointment',
        }]
    }, {
        title: '问答管理',
        name: 'questionList',
        children: [{
            title: '问答列表',
            name: 'question',
            path: '/home/question',
        }]
    }]
}]

export default routes