/*
 * @Author: your name
 * @Date: 2021-05-18 08:28:50
 * @LastEditTime: 2021-06-03 20:21:50
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \宏烨找房\admin\src\routes\index.js
 */
import React from 'react'
import { Switch, Redirect, Route } from 'react-router-dom'
//登录拦截
const beforeEach = (item) => {
    if(item.path!=='/login'&&item.path!=='/register'&&!sessionStorage.getItem('token')) {
        return <Redirect key="33" to="/login" />
    }
    return
}
const Routeview = (props) => {
    const { routes } = props
    return <Switch>
        {
            routes.map((item, index) => item.path?<Route 
                key={index}
                path={item.path}
                render={(History) => beforeEach(item)||<item.component {...History}>
                    <Routeview routes={item.children?item.children:[]}>{item.children?item.children:''}</Routeview>
                </item.component>}>
            </Route>:<Redirect key={index} {...item} />)
        }
    </Switch>
}


export default Routeview